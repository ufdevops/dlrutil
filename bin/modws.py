#!/usr/bin/python3
# Modify your workspace

import argparse
import json
import sys
import os
import requests

import dlrutil.basic as basic
import dlrutil.logger as logger

def parse_arguments():
    parser = argparse.ArgumentParser()

    curdir = basic.get_script_path()
    default_config_file = curdir + "/config/scm.json"

    parser.add_argument('-f', '--config_file',
                        help='The JSON configuration file (default: %s)' % default_config_file,
                        required=False,
                        default=default_config_file)
    parser.add_argument('-s', '--scm_tag',
                        help='The SCM tag to use in the configuration file',
                        required=True)
    parser.add_argument('-b', '--branch',
                        help='The branch to switch to',
                        required=True)
    parser.add_argument('-a', '--action',
                        help='What to do',
                        required=True,
                        choices=['update', 'switch', 'status', 'branch'],
                        default="update")

    return parser.parse_args()

def cloneRepo(name,project,clone_url):
    # make the project directory if it doesn't exist
    basic.create_dir(project)

    cmd = "cd %s && git clone %s" % (project, clone_url)
    logger.info(cmd)
    return os.system(cmd)

def updateRepo(repo_dir):
    cmd = "cd %s && git pull" % repo_dir
    logger.info(cmd)
    return os.system(cmd)

def getRepoStatus(repo_dir):
    cmd = "cd %s && git status -s" % repo_dir
    logger.info(cmd)
    return os.system(cmd)

def getCurrentRepoBranch(repo_dir):
    cmd = "cd %s && git branch" % repo_dir
    logger.info(cmd)
    return os.system(cmd)

def switchRepoBranch(repo_dir,branch):
    cmd = "cd %s && git checkout %s" % (repo_dir, branch)
    logger.info(cmd)
    return os.system(cmd)

def modify_bitbucket2_workspace(action,data_dict,branch):
    rc = 1
    protocol = data_dict["protocol"]
    scm_host = data_dict["scm_host"]
    version  = data_dict["version"]
    team     = data_dict["team"]

    url = "https://" + scm_host + "/" + version + "/teams/" + team + "/repositories?limit=10000"
    ret = requests.get(url)
    repo_array = ret.json()['values']

    for r in repo_array:
        repo_name = r['name']
        project = r['project']['name']
        repo_dir = "%s/%s" % (project, repo_name)
        
        if action == "update":
            if not basic.does_dir_exist(repo_dir):
                # clone the repo
                clone_array = r['links']['clone']
                clone_url = None
                for c in clone_array:
                    if c['name'] == 'ssh':
                        clone_url = c['href']
                        
                if clone_url != None:
                    rc = cloneRepo(repo_name,project,clone_url)
                else:
                    logger.error ("Cannot find clone href for repo: %s" % repo_dir)
                    rc = 1
            else:
                # update the repo
                rc = updateRepo(repo_dir) & \
                     switchRepoBranch(repo_dir,branch) & \
                     getCurrentRepoBranch(repo_dir) & \
                     getRepoStatus(repo_dir)
        elif action == "status":
            rc = getRepoStatus(repo_dir)
        elif action == "branch":
            rc = getCurrentRepoBranch(repo_dir)
        elif action == "switch":
            rc = updateRepo(repo_dir) & \
                 switchRepoBranch(repo_dir,branch) & \
                 getCurrentRepoBranch(repo_dir) & \
                 updateRepo(repo_dir)
    return rc

def main():
    rc = 1

    args = parse_arguments()
    config_file = args.config_file
    scm_tag = args.scm_tag
    branch = args.branch
    action = args.action

    # get the data for the command line specified scm tag
    fh = open(config_file)
    scm_dict = json.load(fh)
    fh.close()
    
    if scm_tag in scm_dict:
        data_dict = scm_dict[scm_tag]

        # call the right method based on the type of the scm tag
        scm_type = data_dict["type"]
        method_name = "modify_" + scm_type + "_workspace"
        method = getattr(sys.modules[__name__], method_name)
        rc = method(action,data_dict,branch)
    else:
        logger.error("The scm_tag, %s, does not exist in the configuration file, %s" % (scm_tag, config_file))

    return rc
     
exit(main())

