#!/usr/bin/python
################################################################################
# Copyright 2016 Kronos Incorporated. All rights reserved.
#
# Manage a cluster of Falcon shared services.
# TODO: Support schedules and stop times
################################################################################

from cat import Application, Template
from concurrent.futures import ThreadPoolExecutor
from cutils import CountdownTimer
from project import set_project_id
from rsc.api.ss import SelfService
from rscutils.logger import error
from rscutils.logger import info
from rscutils.logger import debug
from service import Service
from subprocess import CalledProcessError
import time
import yaml

MAX_THREADS = 8  # RightScale throws HTTP 503 "server overloaded" errors if we push it too hard


class Cluster:
    """Falcon shared services cluster.
       Read cluster data from YAML-formatted cluster config file.
       The cluster data contains the specification of each service in this cluster,
       including the service's CAT name, input parameters and values, and deployment dependencies.
       Each distinct cluster has it's own UUID which is used to name the services
       that belong to that cluster.
    """

    def __init__(self, config_file, published):
        """Instantiate each service in the config file and stick it in a map"""
        info('Cluster configuration file: ' + config_file)
        with open(config_file) as yaml_data:
            config = yaml.load(yaml_data)
        if 'general' not in config:
            raise Exception('Bad config file format: missing general section: ' + config_file)
        if 'variables' not in config['general']:
            raise Exception('Bad config file format: missing variables section: ' + config_file)
        if 'cluster_uuid' not in config['general']:
            raise Exception('Bad config file format: missing cluster_uuid: ' + config_file)
        if 'rightscale_project_id' not in config['general']:
            raise Exception('Bad config file format: missing rightscale_project_id: ' + config_file)
        self._variables = config['general']['variables']
        self._cluster_uuid = config['general']['cluster_uuid']
        description = config['general']['description']
        set_project_id(config['general']['rightscale_project_id'])
        self._service_map = {}
        if 'services' not in config:
            raise Exception('Bad config file format: missing services section: ' + config_file)
        if 'types' not in config:
            raise Exception('Bad config file format: missing types section: ' + config_file)
        parameter_types = config['types']
        for service_id, service_config in config['services'].items():
            cat_name = service_config['cat_name']
            cat = self.get_cat(cat_name, published)
            service_config['description'] = description
            service_config['execution_name'] = cat_name + ' [' + self.get_cluster_uuid() + ']'
            service_config['id'] = service_id
            service_config['parameter_types'] = parameter_types
            self._service_map[service_id] = Service(service_config, cat, self)

    def get_cluster_uuid(self):
        return self._cluster_uuid

    def get_cat(self, cat_name, published):
        """Look up the specified CAT, either published (in the Self-Service catalog)
           or not (just uploaded to the Designer).
        """
        if published:
            return Application(cat_name)
        else:
            return Template(cat_name)

    def get_services(self):
        return self._service_map.values()

    def get_service_by_id(self, sid):
        if sid in self._service_map:
            return self._service_map[sid]
        raise Exception('No service with id: ' + sid)

    def get_service_by_name(self, name):
        for service in self.get_services():
            if service.get_cat().get_name() == name:
                return service
        raise Exception('No service with name: ' + name)

    @staticmethod
    def _process_finished_futures(deploying, running):
        """Process any services that are up and running, or that have failed.
           Move finished deployments out of the 'deploying' list into 'running'.
           :param deploying: list of services currently deploying, as concurrent.futures.Future
           :param running: list of running services as service id string
           :return updated 'deploying' and 'running' lists
           :raises Exception if one or more services fail to deploy
        """
        running.extend([d.result() for d in deploying if d.done() and not d.exception()])
        exceptions = [d.exception() for d in deploying if d.done() and d.exception()]
        deploying = [d for d in deploying if not d.done()]
        if exceptions:
            for ex in exceptions:
                Cluster._print_error_message(ex)
            raise Exception('Some services failed to deploy')
        return deploying, running

    @staticmethod
    def _deploy_one_service(svc):
        """Deploy the specified service and wait for it to reach the running state.
           :param svc: service as cluster.Service instance
           :return: service id (string)
           :raises various exceptions if service fails to deploy
        """
        if not svc.is_deployed():
            svc.deploy()
        svc.wait_for_running()
        return svc.get_id()

    @staticmethod
    def _print_error_message(ex):
        if ex is CalledProcessError:
            error(str(ex.cmd))
            error(ex.output)
        else:
            error(str(ex))  # TODO: Is there a better way to get meaning messages out of an exception?

    def deploy_all_services(self, timeout_minutes=30):
        info('Deploying all services')
        waiting = sorted(self._service_map.keys())
        deploying = []
        running = []
        executor = ThreadPoolExecutor(MAX_THREADS)
        timer = CountdownTimer(minutes=timeout_minutes)
        try:
            while waiting or deploying:
                for service_id in waiting:
                    service = self.get_service_by_id(service_id)
                    if service_is_ready_to_deploy(service, running):
                        waiting.remove(service_id)
                        deploying.append(executor.submit(Cluster._deploy_one_service, service))
                # Process any services that have finished deploying, or have failed.
                deploying, running = Cluster._process_finished_futures(deploying, running)
                timer.check()
                debug('Waiting for services to deploy')
                time.sleep(10)
        finally:
            # Cancel any unfinished tasks and shut down executor
            [dep.cancel() for dep in deploying if not dep.done()]
            executor.shutdown(wait=True)  # should return immediately; all remaining futures should be cancelled


def service_is_ready_to_deploy(service, running):
    """Are all this service's dependencies running?"""
    # TODO: Handle the case where one of the service's dependencies isn't even in the cluster config.
    #       The service will never be 'ready' and we'll just loop until we time out.
    not_running = [dep for dep in service.get_depends_on() if dep not in running]
    if not_running:
        debug(service.get_name() + ' is waiting on ' + str(not_running))
        return False
    else:
        debug(service.get_name() + ' is ready to deploy')
        return True
