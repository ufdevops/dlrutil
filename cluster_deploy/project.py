#!/usr/bin/python
################################################################################
# Copyright 2016 Kronos Incorporated. All rights reserved.
################################################################################
# Convenience class where we stash our shared SelfService instance and project id
# TODO: I'm sure there's a better way to do this...
################################################################################

from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15


def set_project_id(id_val):
    global _project_id
    _project_id = id_val


def get_project_id():
    assert '_project_id' in globals()
    return _project_id


def get_self_service_api():
    """RightScale self-service API singleton. Instantiating this API is expensive, so only do it once."""
    if '_self_service_api' not in globals():
        global _self_service_api
        _self_service_api = SelfService()
    return _self_service_api


def get_cloud_management_api():
    """RightScale Cloud Management v1.5 API singleton. Instantiating this API is expensive, so only do it once."""
    if '_cloud_management_api' not in globals():
        global _cloud_management_api
        _cloud_management_api = CM15()
    return _cloud_management_api
