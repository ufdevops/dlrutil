#!/usr/bin/python
################################################################################
# Copyright 2017 Kronos Incorporated. All rights reserved.
################################################################################

from create_db import create_or_restore_ppas_database
from create_db import CREATE
from rscutils.logger import error
import argparse

# NOTE: These database names must match the names in the shared service config file
DATABASES = {
    'ppas_wfm_clustered':      'ppas_falcon_customer_multi.dump',
    'ppas_wfm_team':           'ppas_falcon_customer_multi.dump',
    'ppas_wfm_user':           'ppas_falcon_customer_multi.dump',
    # Note: This database must be created with the same dump as the WFM database above.
    # It is used to populate OpenAM with the required OpenAM seed users. Ask Charanpreet Singh for details.
    'ppas_post_openam_config': 'ppas_falcon_customer_multi.dump',
    'ppas_workflow_admin':     'ppas_empty.dump',
    'ppas_workflow_app':       'ppas_falcon_bpm_app.dump',
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--db_server',
                        help='The full host name of the PPAS server.',
                        required=True)
    args = parser.parse_args()
    db_server = args.db_server
    for db_name, db_dump in DATABASES.items():
        job_params = {
            'operation': CREATE,
            'db_server': db_server,
            'db_name': db_name,
            'db_dump': db_dump,
        }
        try:
            create_or_restore_ppas_database(job_params)
        except Exception as ex:
            error('Error creating/restoring PPAS database: ' + str(ex))
