#!/usr/bin/python
################################################################################
# Copyright 2017 Kronos Incorporated. All rights reserved.
################################################################################

import sys
sys.path.append('../rsc')   # for rsc.rscutils, rsc.api, etc.

from cutils import CountdownTimer
from pprint import PrettyPrinter
from rscutils.logger import info
import argparse
import jenkins
import time

JENKINS_URL = 'http://dcs-ss-buildserver1.int.kronos.com:8080'
JENKINS_JOB = 'UDM_RDatabase_Auto_Deploy'
JENKINS_JOB_TOKEN = 'remote'
DEFAULT_DB_NAME = 'ppas_udm'

jenkins_server = jenkins.Jenkins(JENKINS_URL)
pp = PrettyPrinter()

def create_udm_database(job_params):
    info("Jenkins job parameters: " + pp.pformat(job_params))
    for value in job_params.values():
        assert value

    jenkins_server.build_job(JENKINS_JOB, job_params, token=JENKINS_JOB_TOKEN)
    job_info = jenkins_server.get_job_info(JENKINS_JOB)
    build_number = job_info['nextBuildNumber']  # TODO: Is this always our job number?
    build_info = None

    # Wait for build job to start
    timer = CountdownTimer(minutes=5)
    while not build_info:
        try:
            build_info = jenkins_server.get_build_info(JENKINS_JOB, build_number)
        except jenkins.NotFoundException:
            timer.check()
            info('Waiting for Jenkins job {0} to start executing'.format(build_number))
            time.sleep(10)
    build_name = build_info['fullDisplayName']

    # Wait for build job to complete
    timer = CountdownTimer(minutes=20)
    while build_info['building']:
        timer.check()
        info('Waiting for Jenkins job {0} to finish'.format(build_name))
        time.sleep(20)
        build_info = jenkins_server.get_build_info(JENKINS_JOB, build_number)
    if build_info['result'] == 'SUCCESS':
        info('Build succeeded: ' + build_name)
    else:
        raise Exception('Jenkins build failed: ' + build_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--db_server',
                        help='The full host name of the PPAS server.',
                        required=True)
    parser.add_argument('--db_name',
                        help='The name of the UDM database on the PPAS server.',
                        default=DEFAULT_DB_NAME)

    args = parser.parse_args()
    job_params = {
        'serverip': 'Other',
        'serverip_Other': args.db_server,
        'username': 'james.lennon@kronos.com',
        'udmdbname': args.db_name,
    }

    create_udm_database(job_params)
