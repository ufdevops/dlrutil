#!/usr/bin/python
################################################################################
# Copyright 2017 Kronos Incorporated. All rights reserved.
################################################################################

import sys
sys.path.append('../rsc')   # for rsc.rscutils, rsc.api, etc.

from cutils import CountdownTimer
from pprint import PrettyPrinter
from rscutils.logger import info
import argparse
import jenkins
import time

JENKINS_URL = 'http://10.3.48.45:8080'
CREATE = 'create'
REBUILD = 'rebuild'
JENKINS_JOB_MAP = {
    CREATE: 'create db 95',
    REBUILD: 'rebuild db 95',
}
NODES = ['master', 'Asia']
DEFAULT_NODE = NODES[0]

jenkins_server = jenkins.Jenkins(JENKINS_URL)
pp = PrettyPrinter()

def create_or_restore_ppas_database(job_params):
    for name, value in job_params.items():
        info(name + ': ' + str(value))
    for value in job_params.values():
        assert value
    # Pull the operation out of the parameter list.
    # Everything else gets sent to the Jenkins job
    operation = job_params['operation']
    del job_params['operation']
    jenkins_job = JENKINS_JOB_MAP[operation]

    jenkins_server.build_job(jenkins_job, job_params)
    job_info = jenkins_server.get_job_info(jenkins_job)
    build_number = job_info['nextBuildNumber']  # TODO: Is this always our job number?
    build_info = None

    # Wait for build job to start
    timer = CountdownTimer(minutes=5)
    while not build_info:
        try:
            build_info = jenkins_server.get_build_info(jenkins_job, build_number)
        except jenkins.NotFoundException:
            timer.check()
            info('Waiting for Jenkins job {0} to start executing'.format(build_number))
            time.sleep(10)
    build_name = build_info['fullDisplayName']

    # Wait for build job to complete
    timer = CountdownTimer(minutes=20)
    while build_info['building']:
        timer.check()
        info('Waiting for Jenkins job {0} to finish'.format(build_name))
        time.sleep(20)
        build_info = jenkins_server.get_build_info(jenkins_job, build_number)
    if build_info['result'] == 'SUCCESS':
        info('Build succeeded: ' + build_name)
    else:
        raise Exception('Jenkins build failed: ' + build_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--operation',
                        choices=JENKINS_JOB_MAP.keys(),
                        required=True)
    parser.add_argument('--db_server',
                        help='The full host name of the PPAS server.',
                        required=True)
    parser.add_argument('--db_name',
                        help='The name of the database on the PPAS server.',
                        required=True)
    parser.add_argument('--db_dump',
                        help='The dump file for initializing database.',
                        required=True)
    parser.add_argument('--db_username',
                        help='The user name for the database.',
                        default='tkcsowner')
    parser.add_argument('--db_password',
                        help='The password for the database.',
                        default='tkcsowner')
    parser.add_argument('--Node_to_Run_On',
                        help='The dump file for initializing database.',
                        default=DEFAULT_NODE)

    job_params = vars(parser.parse_args())

    create_or_restore_ppas_database(job_params)
