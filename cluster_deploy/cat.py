#!/usr/bin/python
################################################################################
# Copyright 2016 Kronos Incorporated. All rights reserved.
#
# Support for RightScale CATs.
################################################################################

from project import get_project_id, get_self_service_api
from rsc.api.ss import SelfService
from rscutils.logger import debug
from rscutils.logger import set_log_level
from rscutils.logger import LEVELS


class Cat:

    def __init__(self, name):
        self.project_id = get_project_id()
        self.ss_api = get_self_service_api()
        self._name = name
        self._input_map = None

    def get_name(self):
        return self._name

    def get_info(self):
        pass # subclasses must override

    def is_application(self):
        return False # subclass may override

    def is_template(self):
        return False # subclass may override

    def get_input_map(self):
        """Get the input parameters needed to deploy an instance of this CAT, keyed by parameter name."""
        if self._input_map is None:
            params = self.get_info()['parameters']
            self._input_map = {}
            for param in params:
                self._input_map[param['name']] = param['default']
        return self._input_map

    def generate_config_template(self):
        return {
            'cat_name': self.get_name(),
            'inputs': self.get_input_map()
        }

    def __str__(self):
        return self.get_name()


class Application(Cat):
    """A published CAT - one that appears in the Self-Service 'Catalog' view"""

    def __init__(self, name):
        Cat.__init__(self, name)
        self._application = None

    def is_application(self):
        return True

    def get_info(self):
        if self._application is None:
            debug('Getting info for application: ' + str(self))
            application = None
            for app in self.ss_api.index_application(catalog_id=self.project_id):
                if self.get_name() == app.get('name'):
                    application = app
                    break
            if application is None:
                raise Exception('No application for ' + str(self))
            self._application = self.ss_api.show_application(
                catalog_id=self.project_id,
                id=application['id'],
                view='expanded'
            )
        return self._application


class Template(Cat):
    """An unpublished CAT - one that appears in the Self-Service 'Designer' view"""

    def __init__(self, name):
        Cat.__init__(self, name)
        self._template = None

    def is_template(self):
        return True

    def get_info(self):
        if self._template is None:
            debug('Getting info for template: ' + str(self))
            template = None
            for t in self.ss_api.index_template(collection_id=self.project_id):
                if self.get_name() == t.get('name'):
                    template = t
                    break
            if template is None :
                raise Exception('No template for ' + str(self))
            self._template = self.ss_api.show_template(
                collection_id=self.project_id,
                id=template['id'],
                view='expanded'
            )
        return self._template
