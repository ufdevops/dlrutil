#!/usr/bin/env python
################################################################################
# Copyright 2016, 2017 Kronos Incorporated. All rights reserved.
################################################################################

import sys
sys.path.append('../rsc')                       # for rsc.rscutils, rsc.api, etc.
sys.path.append('../../global-config/scripts')  # for util.config.ConfigJSON, etc.

from cluster import Cluster
from concurrent.futures import ThreadPoolExecutor
from project import set_project_id
from rscutils.logger import info
from rscutils.logger import set_log_level
from rscutils.logger import warning
from service import Service
from textwrap import dedent
from util.config.ConfigJSON import ConfigJSON
import argparse
import os
import sys
import uuid
import yaml

MAX_THREADS = 8  # RightScale throws HTTP 503 "server overloaded" errors if we push it too hard
CMD_PREFIX = 'cmd_'  # CLI-invokable methods start with this
THIS_FILE = os.path.basename(__file__)
default_global_config_root_dir = '../../global-config'
default_project = 'gce_engineering'
global_config = None


class ClusterCmd(object):
    """Command line processor for cluster-related commands.
       Any method in this class whose name starts with 'cmd_' can be invoked from the command line.
    """

    def __init__(self):
        description = dedent("""
        Manage clusters of Falcon shared services in RightScale:
        o Automatically deploy a cluster of shared services driven by a service
          configuration file.
        o Terminate and delete those services and track their status.
        o Create and update a shared service configuration file from a template
          using CAT data - input & output parameters, etc. - from 'global-config'.

        See this Confluence page for more info:
            https://engconf.int.kronos.com/display/CIA/Automated+Deployment+of+Shared+Service+Clusters
        """)
        parser = argparse.ArgumentParser(
            description=description,
            usage=self.get_usage(),
            formatter_class=argparse.RawTextHelpFormatter  # preserves newlines & other formatting of help text
        )
        parser.add_argument('command', help='Command to run')
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail
        args = parser.parse_args(sys.argv[1:2])
        command = CMD_PREFIX + args.command
        if not hasattr(self, command):
            print('Unrecognized command: ' + args.command)
            parser.print_help()
            exit(1)
        # use dispatch pattern to invoke method with same name
        getattr(self, command)()

    def get_usage(self):
        # Find all methods in this class that start with 'cmd_'
        cmds = [cmd[len(CMD_PREFIX):] for cmd in dir(self) if cmd.startswith(CMD_PREFIX)]
        return 'Valid commands:\n\t{0}\nType "{1} --help" for an overview or "{1} <command> --help" for help with a specific command'\
            .format('\n\t'.join(cmds), THIS_FILE, THIS_FILE)

    def get_subcommand_arg_parser(self, **kwargs):
        kwargs['formatter_class'] = argparse.RawTextHelpFormatter  # preserves newlines & other formatting of help text
        parser = argparse.ArgumentParser(**kwargs)

        # Add common args
        parser.add_argument('--quiet',
                            help='Suppress most output.',
                            action='count',
                            required=False)
        parser.add_argument('--debug',
                            help='Debugging output.',
                            action='count',
                            required=False)
        return parser

    def get_subcommand_args(self, parser):
        """Process arguments for a subcommand. Ignore the first 2 argvs, i.e., the command and the subcommand"""
        args = parser.parse_args(sys.argv[2:])
        # Process common args
        if args.debug:
            set_log_level('DEBUG')
        elif args.quiet:
            set_log_level('WARNING')
        return args

    def cmd_deploy_all(self):
        description = dedent('''
        Automatically deploy the cluster of shared services listed in the specified
        configuration file. The config file specifies which CAT and CAT input
        parameters will be used to deploy each service, as well as the dependencies
        between services. Services are deployed in dependency order - service 'X' is
        not deployed until all its dependencies are successfully launched and running.

        The config file contains a uuid that uniquely identifies the shared service
        deployments in this cluster. This uuid is part of each deployment's name, e.g.:
            Kronos PPAS (cia) [2fbc5236-7012-4b55-8e71-04c4af9b38b6]
        and is used by other commands to locate this cluster's deployments.

        This command can be run multiple times on the same cluster. Each time it's run,
        it will determine the status of each service in the cluster and deploy any that
        aren't running, or wait for services that are in the process of launching.

        This command will terminate if any service fails to deploy, or if it finds an
        already-deployed service in a failed or terminated state. To relaunch a failed
        service deployment, use the 'terminate_one' and 'delete_one' commands to remove
        the failed deployment from RightScale Self-Service, then re-run this command.

        The '--published' flag specifies which CAT version to use for deployment - the
        official published version in the Self-Service Catalog view or the test version
        that's been uploaded to the Self-Service Designer.
        ''')
        parser = self.get_subcommand_arg_parser(description=description)
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        parser.add_argument('--published',
                            help='Specify which CAT version to deploy from: published in the Catalog vs. just uploaded to the Designer.',
                            choices=['true', 'false'],
                            default='false',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, args.published == 'true')
        cluster.deploy_all_services()

    def cmd_deploy_one(self):
        description = dedent('''
        Automatically deploy the specified service using the CAT and CAT input
        parameters specified in the config file.

        This command assumes that all other services on which this service depends
        are already deployed and running.

        The '--published' flag specifies which CAT version to use for deployment - the
        official published version in the Self-Service Catalog view or the test version
        that's been uploaded to the Self-Service Designer.
        ''')

        parser = self.get_subcommand_arg_parser(description=description)
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        parser.add_argument('--service_id',
                            help='The ID of the service to deploy.',
                            required=True)
        parser.add_argument('--published',
                            help='Specify which CAT version to deploy from: published in the Catalog vs. just uploaded to the Designer.',
                            choices=['true', 'false'],
                            default='false',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, args.published == 'true')
        service = cluster.get_service_by_id(args.service_id)
        futures = []

        # Make sure service's dependencies, if any, are running
        def assert_is_running(dep):
            status = dep.get_status()
            if status != Service.RUNNING:
                raise Exception('Service dependency must be "running", but is "{0}": {1}'.format(status, dep.get_name()))

        with ThreadPoolExecutor(MAX_THREADS) as executor:  # will automatically shut down executor
            for dep_id in service.get_depends_on():
                dependency = cluster.get_service_by_id(dep_id)
                futures.append(executor.submit(assert_is_running, dependency))
        for f in futures:
            f.result()  # Throws exception if dependency is not running

        # Deploy service and wait for it to reach running state
        Cluster._deploy_one_service(service)

    def cmd_create_config_file(self, update_existing=False):
        from datetime import datetime
        from config import ClusterConfig
        if update_existing:
            description = 'Update an existing shared service configuration file preserving its cluster uuid.'
        else:
            description = 'Create a new shared service configuration file.'
        description += dedent('''
        Basic algorithm for each service listed in the specified template file:
        1) Get service's CAT definition from .../global-config/data/catalogs.json
        2) For each input parameter in from CAT definition, determine the parameter value:
           a) Use service-specific parameter override value, if any, from template file
           b) Else use global parameter override value, if any, from template file
           c) Else use location default from 'global-config', if any
           d) Else assign value of 'TODO' and print warning
        3) Generate a unique name for deployments of the service: '<CAT name> [<cluster_uuid>]'
           that will be used by other commands to find these deployments in RightScale
        4) Record the service's dependencies - the other services that must be deployed
           before this one.
        Save these service configurations to the config file, plus the cluster uuid,
        the parameters used to generate the file, current date/time, etc.
        ''')
        parser = self.get_subcommand_arg_parser(description=description)
        parser.add_argument('--project',
                            help='The RightScale project containing the deployment.',
                            default=default_project)
        parser.add_argument('--release',
                            help='The CIA release for the deployment.',
                            required=True)
        parser.add_argument('--global_config_root_dir',
                            help='Root directory of global config utilities',
                            default=default_global_config_root_dir,
                            required=False)
        parser.add_argument('--template_file',
                            help='The YAML-formatted template file for this cluster',
                            required=True)
        parser.add_argument('--config_file',
                            help='Where to write the config file',
                            required=True)
        parser.add_argument('--force',
                            help='Overwrite existing config file',
                            choices=['true', 'false'],
                            default='false')
        args = self.get_subcommand_args(parser)
        global global_config
        global_config = ConfigJSON(args.global_config_root_dir, args.project, args.release)
        global_config.read ()
        set_project_id(global_config.get('project_id'))

        cluster_uuid = str(uuid.uuid4())
        if update_existing:
            if os.path.exists(args.config_file):
                # We're updating an existing config file, so reuse its cluster uuid, etc.
                cluster = Cluster(args.config_file, False)
                cluster_uuid = cluster.get_cluster_uuid()
                info('Updating existing config file: {0} [{1}]'.format(args.config_file, cluster_uuid))
            else:
                warning('No such config file; creating new one: ' + args.config_file)
        else:
            # We're create a new config file, so generate a new cluster uuid, etc.
            if os.path.exists(args.config_file):
                if args.force == 'false':
                    raise Exception("Config file already exists; use '--force' option to overwrite or 'update_config_file' command to update: " + args.config_file)
                else:
                    info('Overwriting existing config file: ' + args.config_file)

        cluster_config = ClusterConfig(args.template_file, global_config)
        # Named parameters to 'generate()' will be written out to new config file
        cluster_config.generate(
            cluster_uuid=cluster_uuid,
            generated_date_time=str(datetime.now()),
            generated_from_template_file=args.template_file,
            generated_command_line=' '.join(sys.argv),
            global_config_root_dir=args.global_config_root_dir,
            google_project=args.project,
            cia_release=args.release,
        )

        header_comment = "# TODO: Assign appropriate values to any parameters marked 'TODO'\n\n"
        cluster_config.write(args.config_file, header_comment)

    def cmd_update_config_file(self):
        """Regenerate existing config file preserving its cluster uuid, etc."""
        self.cmd_create_config_file(update_existing=True)

    def cmd_show_status(self):
        parser = self.get_subcommand_arg_parser(description='Show shared services status')
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, False)
        futures = []

        def get_one_status(svc):
            return '{0} status: {1}'.format(svc.get_name(), svc.get_status())

        with ThreadPoolExecutor(MAX_THREADS) as executor:  # will automatically shut down executor
            for service in cluster.get_services():
                futures.append(executor.submit(get_one_status, service))
        for f in futures:
            print(f.result())

    def cmd_terminate_all(self):
        description = dedent('''
        Terminate all service deployments listed in the specified configuration file
        that are not already in a terminated state. This command makes sure each
        deployment is unlocked before terminating it; otherwise the operation will fail
        and leave the deployment in a bad state.
        ''')
        parser = self.get_subcommand_arg_parser(description=description)
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, False)
        futures = []

        with ThreadPoolExecutor(MAX_THREADS) as executor:  # will automatically shut down executor
            for service in cluster.get_services():
                futures.append(executor.submit(Service.start_terminate, service))
        terminating = filter(None, [f.result() for f in futures])  # list of terminating services w/Nones removed

        with ThreadPoolExecutor(MAX_THREADS) as executor:  # will automatically shut down executor
            for service in terminating:
                futures.append(executor.submit(Service.wait_for_terminate_to_finish, service, 10))
        [f.result() for f in futures]  # give threads a chance to throw exceptions, if any

    def cmd_terminate_one(self):
        parser = self.get_subcommand_arg_parser(description='Terminate the specified shared service deployment')
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        parser.add_argument('--service_id',
                            help='The ID of the service to terminate.',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, False)
        service = cluster.get_service_by_id(args.service_id)
        service.start_terminate()
        service.wait_for_terminate_to_finish(10)

    def cmd_delete_all(self):
        description = dedent('''
        Delete all service deployments listed in the specified configuration file
        that are already in a terminated state. To terminate deployments, use the
        'terminate_all' or 'terminate_one' command.
        ''')
        parser = self.get_subcommand_arg_parser(description=description)
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, False)
        futures = []

        with ThreadPoolExecutor(MAX_THREADS) as executor:  # will automatically shut down executor
            for service in cluster.get_services():
                futures.append(executor.submit(Service.start_delete, service))
        deleting = filter(None, [f.result() for f in futures])  # list of deleting services w/Nones removed

        with ThreadPoolExecutor(MAX_THREADS) as executor:  # will automatically shut down executor
            for service in deleting:
                futures.append(executor.submit(Service.wait_for_delete_to_finish, service, 10))
        [f.result() for f in futures]  # give threads a chance to throw exceptions, if any

    def cmd_delete_one(self):
        parser = self.get_subcommand_arg_parser(description='Delete the specified shared service deployment')
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        parser.add_argument('--service_id',
                            help='The ID of the service to delete.',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, False)
        service = cluster.get_service_by_id(args.service_id)
        service.start_delete()
        service.wait_for_delete_to_finish(10)

    def cmd_dump_all(self):
        parser = self.get_subcommand_arg_parser(description='Dump all deployed shared services')
        parser.add_argument('--config_file',
                            help='The YAML file containing the cluster configuration.',
                            required=True)
        parser.add_argument('--dump_file',
                            help='Where to dump the results',
                            required=True)
        args = self.get_subcommand_args(parser)
        cluster = Cluster(args.config_file, False)
        dump_file = args.dump_file

        dump = {}
        futures = []

        def dump_one_service(service):
            return service, service.dump()

        with ThreadPoolExecutor(MAX_THREADS) as executor:  # will automatically shut down executor
            for service in cluster.get_services():
                futures.append(executor.submit(dump_one_service, service))
        for f in futures:
            service, service_dump = f.result()
            dump[service.get_name()] = service_dump

        info('Writing configuration dump to: ' + dump_file)
        with open(dump_file, 'w') as out:
            yaml.safe_dump(dump, out, default_flow_style=False)


if __name__ == '__main__':
    ClusterCmd()
