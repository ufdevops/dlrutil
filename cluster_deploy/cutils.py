#!/usr/bin/python
################################################################################
# Copyright 2016 Kronos Incorporated. All rights reserved.
################################################################################


def datetime_to_rightscale_format(dt):
    """Print datetime in RightScale format, e.g., '2016-12-19T15:44:51-05:00'"""
    from datetime import datetime
    from math import fabs
    import dateutil.tz

    local_tz = dateutil.tz.tzlocal()
    local_offset = local_tz.utcoffset(datetime.now(local_tz))
    offset_seconds = int(local_offset.total_seconds())
    offset_hours = int(fabs(offset_seconds / 3600))  # absolute value
    offset_minutes = int(offset_seconds / 60) % 60
    sign = '-' if offset_seconds < 0 else '+'
    tz_suffix = '{0}{1:0>2}:{2:0>2}'.format(sign, offset_hours, offset_minutes)
    return dt.strftime('%Y-%m-%dT%H:%M:%S') + tz_suffix


def midnight_tomorrow():
    from datetime import date
    from datetime import timedelta
    from datetime import time
    from datetime import datetime

    now = time()
    tomorrow = date.today() + timedelta(days=1)
    nearly_midnight = time(hour=23, minute=59)
    tomorrow_midnight = datetime.combine(tomorrow, nearly_midnight)
    return datetime_to_rightscale_format(tomorrow_midnight)


class CountdownTimer(object):

    def __init__(self, **kwargs):
        from time import time
        self.timeout_seconds = 0
        for arg in kwargs:
            if arg == 'seconds':
                self.timeout_seconds += int(kwargs[arg])
            elif arg == 'minutes':
                self.timeout_seconds += int(kwargs[arg] * 60)
            elif arg == 'hours':
                self.timeout_seconds += int(kwargs[arg] * 60 * 60)
            else:
                raise Exception('Invalid CountdownTimer keyword: ' + arg)
        self.start_seconds = time()
        self.cancelled = False

    def check(self):
        if not self.cancelled:
            from time import time
            if time() - self.start_seconds > self.timeout_seconds:
                # TODO: In python 3 we can raise TimeoutError()
                raise Exception('Timeout of {0} seconds exceeded'.format(self.timeout_seconds))

    def cancel(self):
        self.cancelled = True


if __name__ == '__main__':
    from time import sleep
    timer = CountdownTimer(minutes=1, seconds=10)
    timer.check() # shouldn't throw exception
    sleep(60)
    timer.check() # shouldn't throw exception
    sleep(15)
    try:
        timer.check()
        assert False  # shouldn't get here
    except TimeoutError as expected:
        print('Got expected timeout error')
