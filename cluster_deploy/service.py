#!/usr/bin/python
################################################################################
# Copyright 2016 Kronos Incorporated. All rights reserved.
################################################################################

from execution import Execution
from execution import get_execution_by_name
from project import get_cloud_management_api
from project import get_project_id
from project import get_self_service_api
from rscutils.logger import debug
from rscutils.logger import info
from time import sleep
from cutils import CountdownTimer
import re

MAX_WAIT_MINUTES = 30  # "OpenAM Post Deploy Config" can take > 20 minutes


class Service:
    """A Falcon shared service as defined by the specified JSON configuration.
       The configuration defines the service's id, CAT name, application name,
       and input parameters. The service's CAT may be published (the RSC API calls this
       an 'application') or merely uploaded to the Self-Service Designer page (a 'template').
       The service can be deployed if it is not already, in which case the input parameters
       and their values are passed to the deployment. Parameter values may contain
       embedded variables, which get evaluated at deployment time. Those variables
       can refer to output parameters of other service deployments. For instance,
       the OpenAM service deployment uses OpenDJ's FQDN as an input parameter value.
    """

    # Possible service execution status values (incomplete)
    FAILED = 'failed'
    LAUNCHING = 'launching'
    RUNNING = 'running'
    TERMINATED = 'terminated'
    TERMINATING = 'terminating'
    NOT_DEPLOYED = 'not deployed'  # Placeholder for undeployed service

    VARIABLE_REGEX = re.compile('\$\{([^\}]*)\}')

    def __init__(self, service_config, cat, cluster):
        """Instantiate a Falcon shared service.
        :param service_config: JSON description of this service, including required input parameters
        :param cat: the RightScale CAT used to deploy this service
        :param cluster: shared service cluster this service belongs to
        """
        self._cat = cat
        self._cluster = cluster
        self._execution = None
        self.project_id = get_project_id()
        self.ss_api = get_self_service_api()
        self.cm15 = get_cloud_management_api()
        self._id = service_config['id']
        self._name = service_config['execution_name']
        self._depends_on = service_config['depends_on']
        self._description = service_config['description']
        self._parameter_types = service_config['parameter_types'] # map of parameter name to parameter type
        self._parameter_values = service_config['input_parameters'] # map of parameter name to parameter value
        for name, value in self._parameter_values.items():
            # Make sure each value is a string; ints, booleans, etc. cause problems below
            self._parameter_values[name] = str(value)

    def get_name(self):
        return self._name

    def get_cat(self):
        return self._cat

    def get_id(self):
        return self._id

    def get_depends_on(self):
        """List of services (service ids) that this service depends on"""
        return self._depends_on

    def is_deployed(self):
        return self.get_status() != Service.NOT_DEPLOYED

    def get_execution(self, must_exist=True, use_cached=True):
        if self._execution is None or not use_cached:
            self._execution = get_execution_by_name(self.get_name())
        if self._execution is None and must_exist:
            raise Exception('No execution for ' + str(self))
        return self._execution

    def get_status(self):
        execution = self.get_execution(must_exist=False, use_cached=False)
        status = execution.get_status() if execution else Service.NOT_DEPLOYED
        debug('Status: ' + status)
        return status

    def get_output_parameter_value(self, parameter_name):
        # TODO: Need to wait until service is running before fetching outputs
        execution = self.get_execution()
        outputs = execution.get_outputs()
        for output in outputs:
            if parameter_name == output['name']:
                # Output value is in output['value']['value']
                # No, this is not a typo
                if 'value' not in output or 'value' not in output['value']:
                    # Maybe the execution is not in a running state?
                    raise Exception('Execution "{0}" is missing value for output parameter: {1}'.format(str(execution), parameter_name))
                return output['value']['value']
        raise Exception('Execution "{0}" is missing output parameter: {1}'.format(str(execution), parameter_name))

    def get_inputs_with_values_formatted(self):
        """Get the input parameters needed to deploy an instance of this service.
           We consult the service's CAT for the definitive list of parameter
           names and their types, then we get actual parameter value from the config file.
        """
        formatted = []
        # We encode each parameter as a 3-element list of strings:
        #   ['options[][name]=<param-name>', 'options[][type]=<param-type>', 'options[][value]=<param-value>']
        # concatenated into one big list.
        for param_name, param_value in self._parameter_values.iteritems():
            param_value = self.resolve_embedded_variables(param_value)
            param_type = self._parameter_types[param_name]
            debug('input parameter: {0}={1} ({2})'.format(param_name, param_value, param_type))
            formatted.append('options[][name]=' + param_name)
            formatted.append('options[][type]=' + param_type)
            formatted.append('options[][value]=' + param_value)
        return formatted

    def _find_variable_value(self, variable_name):
        cluster = self._cluster
        if ':' not in variable_name:
            return cluster.get_variable_value_cluster_zzz(variable_name)
        else:
            segments = variable_name.split(':')
            root = segments[0]
            if root == 'service':  # e.g., 'service:openam:output_fqdn'
                service_id = segments[1]
                variable_name = segments[2]
                service = self if service_id == 'this' else cluster.get_service_by_id(service_id)
                if variable_name == 'id':
                    return self.get_id()
                elif variable_name.startswith('output_'):
                    return service.get_output_parameter_value(variable_name)
        raise Exception('Unsupported variable type: ' + variable_name)

    def resolve_embedded_variables(self, string_with_embedded):
        def closure(match):  # Closure for resolving a single variable
            variable_name = match.group(1)
            return self._find_variable_value(variable_name)
        return Service.VARIABLE_REGEX.sub(closure, string_with_embedded)

    def wait_for_running(self):
        timer = CountdownTimer(minutes=MAX_WAIT_MINUTES)
        status = self.get_status()
        while status == Service.LAUNCHING:
            info('Waiting for service to finish launching: ' + str(self))
            timer.check()  # throws exception if we're taking too long
            sleep(10)
            status = self.get_status()
        if status == Service.RUNNING:
            info('Service is running: ' + str(self))
        else:
            raise Exception('Error deploying {0}: status is "{1}"'.format(str(self), status))

    def deploy(self):
        info('Deploying ' + str(self))
        template_href = None
        application_href = None
        cat = self.get_cat()
        if cat.is_template():
            template_href = cat.get_info()['href']
        elif cat.is_application():
            application_href = cat.get_info()['href']
        else:
            raise Exception('Unexpected CAT type: ' + str(cat))
        self.ss_api.create_execution(  # Seems like this should return an 'execution', but it doesn't
            project_id=self.project_id,
            application_href=application_href,
            template_href=template_href,
            name=self.get_name(),
            description=self._description,
            # TODO: re-enable:  ends_at=midnight_tomorrow(),  # TODO: Allow caller to override
            # current_schedule=config.get('default_schedule_name'),
            InviSiblE01=self.get_inputs_with_values_formatted()
        )

    def start_terminate(self):
        """Initiate termination of this service unless it's not running or already terminat(ed/ing)
           :param svc: the service to terminate
           :returns self if the service is being terminated so caller can wait for it to finish, else None
        """
        if self.get_status() in [Service.NOT_DEPLOYED, Service.TERMINATED, Service.TERMINATING]:
            return None  # not deployed or already terminating/terminated
        self.unlock()  # Can't terminate a locked deployment
        info('Terminating ' + str(self))
        self.ss_api.terminate_execution(
            project_id=self.project_id,
            id=self.get_execution().get_id()
        )
        return self

    def wait_for_terminate_to_finish(self, timeout_minutes):
        timer = CountdownTimer(minutes=timeout_minutes)
        while self.get_status() not in [Service.NOT_DEPLOYED, Service.TERMINATED]:
            info('Waiting for service to finish terminating: ' + str(self))
            timer.check()
            sleep(10)

    def start_delete(self):
        """Initiate deletion of this service, but only if it's in a terminated state; otherwise deleting could strand resources
           :param svc: the service to delete
           :returns self if the service is being deleted so caller can wait for it to finish, else None
        """
        status = self.get_status()
        if status == Service.TERMINATED:
            info('Deleting ' + str(self))
            self.ss_api.delete_execution(
                project_id=self.project_id,
                id=self.get_execution().get_id()
            )
            return self
        else:
            if status != Service.NOT_DEPLOYED:
                info('Not deleting {0} because its status "{1}" is not "{2}"'.format(self, status, Service.TERMINATED))
            return None

    def wait_for_delete_to_finish(self, timeout_minutes):
        timer = CountdownTimer(minutes=timeout_minutes)
        while self.get_status() != Service.NOT_DEPLOYED:
            info('Waiting for service to finish deleting: ' + str(self))
            timer.check()
            sleep(10)

    def lock(self):
        """Lock this service.
           :param svc: the service to lock
           :returns self
        """
        info('Locking ' + str(self))
        self.cm15.unlock_deployment(id=self.get_execution().get_deployment_id())
        return self

    def unlock(self):
        """Unlock this service.
           :param svc: the service to unlock
           :returns self
        """
        info('Unlocking ' + str(self))
        self.cm15.unlock_deployment(id=self.get_execution().get_deployment_id())
        return self

    def dump(self):
        info('Dumping ' + str(self))
        execution = self.get_execution(must_exist=False, use_cached=False)
        if not execution:
            return {}
        exinfo = execution.get_info_expanded()
        deployment_name = exinfo['name']
        inputs = {}
        outputs = {}
        for input in exinfo['configuration_options']:
            inputs[input['name']] = input['value']
        for output in exinfo['outputs']:
            value = output['value']['value'] if output['value'] else None
            outputs[output['name']] = str(value) + '###' + output['description']
        deployment_href = exinfo['deployment']
        tags = self.cm15.by_resource_tag(resource_hrefs=[deployment_href])[0]["tags"]
        cat_name = '<no CAT name>'
        on_behalf_of = '<missing tag: kronos:on_behalf_of>'
        for tag in tags:
            name, value = tag['name'].split('=')
            if name == 'selfservice:launched_from':
                cat_name = value
            elif name == 'kronos:on_behalf_of':
                on_behalf_of = value
        return {
            'catalog_name': cat_name,
            'deployed_on_behalf_of': on_behalf_of,
            'deployment_name': deployment_name,
            'input_parameters': inputs,
            'outputs': outputs,
        }

    def __str__(self):
        return self.get_name()
