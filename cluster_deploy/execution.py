#!/usr/bin/python
################################################################################
# Copyright 2016 Kronos Incorporated. All rights reserved.
################################################################################

from project import get_project_id, get_self_service_api
from rsc.api.ss import SelfService
from rscutils.logger import debug


class Execution:
    """A deployed RightScale service.
    """

    def __init__(self, exinfo):
        self._execution_info_basic = exinfo
        self._execution_info_expanded = None
        self.project_id = get_project_id()
        self.ss_api = get_self_service_api()

    def get_status(self):
        return self._execution_info_basic['status'].lower()

    def get_id(self):
        return self._execution_info_basic['id']

    def get_deployment_href(self):
        return self._execution_info_basic['deployment']

    def get_deployment_id(self):
        """Get the deployment id which is the last segment of the href"""
        return self.get_deployment_href().split('/')[-1]

    def get_name(self):
        return self._execution_info_basic['name']

    def get_outputs(self):
        info = self.get_info_expanded()
        if 'outputs' not in info:
            raise Exception('Execution is missing outputs: ' + str(self))
        return info['outputs']

    def get_info_expanded(self):
        if self._execution_info_expanded is None:
            debug('Getting execution (expanded) for ' + str(self))
            self._execution_info_expanded = self.ss_api.show_execution(
                project_id=self.project_id,
                id=self.get_id(),
                view='expanded'
            )
        return self._execution_info_expanded

    def __str__(self):
        return self.get_name()


def get_execution_by_name(name):
    project_id = get_project_id()
    ss_api = get_self_service_api()

    debug('Getting execution for ' + name)
    for exinfo in ss_api.index_execution(project_id=project_id):
        if name == exinfo.get('name'): # Be careful - not all executions have names
            debug('Found execution for ' + name)
            return Execution(exinfo)
    debug('No execution found for ' + name)
    return None
