#!/usr/bin/env python
################################################################################
# Copyright 2016 Kronos Incorporated. All rights reserved.
################################################################################

from rscutils.logger import debug
from rscutils.logger import info
from rscutils.logger import warning
from service import Service
import yaml


class ClusterConfig:
    """Read and write shared service cluster config files"""

    def __init__(self, config_template_file, global_config):
        """
        :param config_template_file: YAML file containing list of shared service CATs, parameter overrides, etc.
        :param global_config: contains list of input params for each CAT, default param values, param types, etc.
        """
        with open(config_template_file, 'r') as stream:
            self.config_template = yaml.load(stream)

        self.description = self.config_template['general'].get('description', '')
        self.global_overrides = self.config_template['general'].get('parameter_overrides', {})
        self.service_templates = self.config_template['services']

        self.global_cat_dict = global_config.substitute(global_config.get_catalog_dict())
        self.global_param_dict = global_config.substitute(global_config.get_parameter_dict())
        self.project_id = global_config.get('project_id')

        self._config = {}
        self._todo_params = None  # scratch variable used to avoid redundant warning messages

    def get_service_ids(self):
        """Get a list of the internal ids of all the services in the template"""
        return self.service_templates.keys()

    def get_output_params_for_service(self, service_id):
        """Get the list of output parameter names for the specified service"""
        cat_id = self.service_templates[service_id]['catalog_id']
        return self.global_cat_dict[cat_id]['outputs']

    def get_parameter_info(self, param_names, param_dict, param_overrides):
        type_map = {}
        value_map = {}
        for param in param_names:
            if param in param_overrides:
                param_value = param_overrides[param]
            else:
                param_value = self.get_default_parameter_value(param, param_dict)
            value_map[param] = param_value
            type_map[param] = param_dict[param]['type']
        return type_map, value_map

    def get_default_parameter_value(self, param, param_dict):
        param_value = param_dict[param].get('default')
        if param_value == 'Location Default' or param_value == '' or param_value is None:
            param_value = 'TODO'
            self._todo_params[param] = ''
        return param_value

    def validate_parameter_overrides(self, overrides, params):
        for override in overrides:
            if override not in params:
                warning('Override parameter "{0} is not a valid parameter name'.format(override))

    def validate_embedded_variables(self, overrides, global_cat_dict):
        for value in overrides.values():
            if not isinstance(value, str):
                continue # YAML loader converts some values to ints, bools, etc. Skip 'em
            match = Service.VARIABLE_REGEX.match(value)
            if not match:
                continue  # value doesn't contain embedded variables
            variable = match.group(1)
            segments = variable.split(':')
            if len(segments) != 3:
                continue
            if segments[0] != 'service':
                raise Exception('Unsupported variable format: ' + variable)
            service_id = segments[1]
            output_param = segments[2]
            if service_id not in self.get_service_ids():
                raise Exception('Invalid service id "{0}" in variable {1}'.format(service_id, value))
            if output_param not in self.get_output_params_for_service(service_id):
                raise Exception('Invalid output parameter "{0}" in variable {1}'.format(output_param, value))

    def validate_service_dependencies(self, services):
        for service_id, service in services.iteritems():
            for dependency in service['depends_on']:
                if dependency not in services:
                    raise Exception('Invalid service id "{0}" in dependency list for {1}'.format(dependency, service_id))

    def generate(self, **kwargs):
        """Generate a shared service cluster configuration"""

        self.validate_parameter_overrides(self.global_overrides, self.global_param_dict)
        self.validate_embedded_variables(self.global_overrides, self.global_cat_dict)
        services = {}
        all_param_types = {}
        self._todo_params = {}
        for service_id, service_template in self.service_templates.items():
            cat_id = service_template['catalog_id']
            cat_name = self.global_cat_dict[cat_id]['value']
            param_names = self.global_cat_dict[cat_id]['parameters']
            # Replace global overrides with service-specific ones, if any
            overrides = service_template.get('parameter_overrides', {})
            self.validate_parameter_overrides(overrides, param_names)
            local_overrides = self.global_overrides.copy()
            local_overrides.update(overrides)
            depends_on = service_template.get('depends_on', [])
            param_types, param_values = self.get_parameter_info(param_names, self.global_param_dict, local_overrides)
            all_param_types.update(param_types)
            service_config = {
                'cat_name': cat_name,
                'depends_on': depends_on,
                'input_parameters': param_values
            }
            services[service_id] = service_config

        self.validate_service_dependencies(services)

        general = {
            'description': self.description,
            'rightscale_project_id': self.project_id,
            'variables': {}
        }
        for name, value in kwargs.items():  # Add values passed in by caller
            general[name] = value
        self._config = {
            'general': general,
            'services': services,
            'types': all_param_types
        }
        for param in sorted(self._todo_params):
            warning('No default value or override for input parameter "{0}"; value set to "TODO"'.format(param))
        self._todo_params = None
        return self

    def write(self, output_file, header_comment):
        info('Writing cluster configuration to file: ' + output_file)
        with open(output_file, 'w') as out:
            out.write(header_comment)
            yaml.safe_dump(self._config, out, default_flow_style=False)
