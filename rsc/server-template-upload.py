#!/bin/python
###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  Uploads all Server Templates to the specified RightScale project.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# collections imports
from collections import OrderedDict
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
# multiprocessing imports
import multiprocessing
# operator imports
from operator import itemgetter
# os imports
import os
# sys imports
import sys
# subprocess imports
import subprocess
from subprocess import CalledProcessError
# re imports
import re
# rscutils imports
import rscutils
import rscutils.logger as logger
# rsc.api imports
from rsc.api.cm15 import CM15
from rscutils import get_all_templates

###############################################################################
# Global variables
###############################################################################

#Current Server Template to Upload
gCurrentServerTemplates = []

#List of Server Templates that have been uploaded
gUploaderServerTemplates = []

# Create a thread pool.
gThreadPool = ThreadPoolExecutor(multiprocessing.cpu_count())

#Future Results
gFutureResults = []

#Global local ST names
gServerTemplate = []

#Make it Easy
cm_api = CM15()

###############################################################################
# Input arguments
###############################################################################

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--st-dir',
                        help='Path to preprocessed Server Template file directory',
                        required=True)
    args = parser.parse_args()

    return args

###############################################################################
# Helper Functions
###############################################################################

def thread_upload_st():
    server_template_names = get_server_templates()
    #print (server_template_name)
    for server_template_name in server_template_names:
        gThreadPool.submit(upload_server_template_new, server_template_name)


def get_server_templates():
#We need to get the server name/file path to be uploaded
    server_templates = []
    server_template_file_path = []
    #preprocesses_st_dir = os.path.join(root,'preprocessed/server_templates')

    for root, dirs, files in os.walk("%s/preprocessed/server_templates" % args.st_dir):
        for file in files:
            if file.endswith(".yml"):
               # print(os.path.join(root, file))
                #print (file)
                server_template_file_path.append(os.path.join(root, file))
                server_templates.append(file)
    #print (server_templates)
#    gServerTemplate = server_templates

    return server_templates

def check_existing():
    print ("Checking for existing Server Templates")
    existing_server_templates = cm_api.index_servertemplate()


    for server_template in existing_server_templates:
        gCurrentServerTemplates.append(server_template["name" ] + ".yml")


def upload_server_template_new(server_template_name):
    print ("Uploading Server Template with name %s" % server_template_name)
    os.system("right_st st upload %s/preprocessed/server_templates/%s" % (args.st_dir, server_template_name))

args = parse_arguments()
#check_existing()
#get_server_templates()
thread_upload_st()
