#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Perform an action on a RightScale deployment
###############################################################################
###############################################################################

import os
import argparse
import subprocess

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--output_dir',
                        help='Output directory for the CATs',
                        required=True)
    parser.add_argument('--verbose',
                        help='Verbose output',
                        required=False)
    return parser.parse_args()

def main():
    # assume it will fail
    return_code = 1

    args = parse_arguments()
    verbose = args.verbose
    output_dir = args.output_dir

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')

    ss_api = SelfService()

    try:
        applications=ss_api.index_application(
            catalog_id=project_id
        )

        for a in sorted(applications):
            cat_name = a["name"]
            filename = cat_name
            
            if verbose == "True":
                info = ss_api.show_application(
                    catalog_id=project_id,
                    id=a["id"]
                )
                print info

            contents = ss_api.download_application(
                catalog_id=project_id,
                id=a["id"],
                api_version=config.get("rs_ss_api_ver")
            )
            fullpath = output_dir + "/" + filename
            print "Getting " + cat_name + " into " + fullpath
            target = open(fullpath, 'w')
            target.write(contents)
            target.close()
            
        return_code = 0
    except subprocess.CalledProcessError as s:
        logger.error (s.output)

    return return_code

exit(main())
