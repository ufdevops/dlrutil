#!/bin/bash
# This is just a test script

source common.sh

# get designer CATs
python rsc-get-cats-des.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --output_dir="cats_designer"

exit $?
