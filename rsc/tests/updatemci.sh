#!/bin/bash
# This is just a test script

source common.sh

DESCRIPTION="Test MCI"
IMAGE_NAME="Name of the image"
INSTANCE_SIZE="instance_size"
CLOUD="google"

python rsc-update-mci.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --mci_name "$MCI_NAME" --image-name "$IMAGE_NAME" --instance-size "$INSTANCE_SIZE" --cloud="$CLOUD" --description "$DESCRIPTION"

exit $?
