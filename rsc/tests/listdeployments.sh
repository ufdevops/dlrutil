#!/bin/bash
# This is just a test script

source common.sh

echo "Listing deployments"

# Test case #1 by statuses
statuses="failed,stopped,terminated,running"
name_filter=wfm
python rsc-list-deployments.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --deployment_status="$statuses" --name_filter "$name_filter" --cat_filter "wfm"

# Test case #2 by who deployed
#created_by="Service Account"
#python rsc-list-deployments.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --created_by="$created_by"

exit $?
