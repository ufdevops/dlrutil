#!/bin/bash
# This is just a test script

source common.sh

if [[ -z $NUM_DAYS_TO_RUN ]]; then
   echo "You must set NUM_DAYS_TO_RUN"
   exit 1
fi

echo "Canceling $NUM_DAYS_TO_RUN stops for the deployment"

python rsc-cancel-stops.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --num_days_to_run "$NUM_DAYS_TO_RUN"

exit $?
