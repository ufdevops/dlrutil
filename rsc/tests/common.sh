#!/bin/bash -x
# This is included in all test scripts to provide setup

# get the directory the script was started in
bindir=$(dirname "$(readlink -f "$0")")

PROJECT=gce_engineering
RELEASE=phasedemo
GLOBAL_CONFIG_DIR=../../../global-config

echo "Working in $PROJECT/$RELEASE"

make -C $GLOBAL_CONFIG_DIR -f Makefile RELEASE=$RELEASE default

# You need to set this to the deployment name on which you want to operate
EXECUTION_NAME="IntegrationTesting2"

# needed by Python to find global_config
export PYTHONPATH=$GLOBAL_CONFIG_DIR/scripts:.

if [[ $EXECUTION_NAME == "" ]]; then
  echo "You need to set EXECUTION_NAME in common.sh before proceeding"
  exit 1
else
  echo "EXECUTION_NAME=$EXECUTION_NAME"
fi

TERMINATE_DATE="2016-09-26"
TERMINATE_TIME="18:30"
NUM_DAYS_TO_RUN=10
TIMEOUT=2400
NOTIFY=domenic.larosa@kronos.com

source "$GLOBAL_CONFIG_DIR/generated/shell/global_config.sh"
