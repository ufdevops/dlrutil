#!/bin/bash
# This is just a test script

source common.sh

# get published CATs
python onebutton.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --test_name "test_cluster"

exit $?
