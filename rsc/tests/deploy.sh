#!/bin/bash
# This is just a test script

source common.sh

APPLICATION_NAME="$RELEASE_APPLICATION_NAME"
team_location="$RELEASE_TEAM_LOCATION"
team_name="$RELEASE_TEAM_NAME"
team_slot="$RELEASE_TEAM_SLOT"
build_level_backend="$RELEASE_BUILD_LEVEL_BACKEND"
build_level_frontend="$RELEASE_BUILD_LEVEL_FRONTEND"
deployment_datacenter="$RELEASE_DEPLOYMENT_DATACENTER"
db_name="$RELEASE_DBNAME"
db_server="$RELEASE_DBSERVER"
db_username="$RELEASE_DB_USERNAME"
db_password="$RELEASE_DB_PASSWORD"
rabbit_server="$RELEASE_RABBIT_SERVER"
redis_hosts="$RELEASE_REDIS_HOSTS"
redis_master="$RELEASE_REDIS_MASTER"
auth_url="$RELEASE_AUTH_URL"
owner="$RELEASE_OWNER"

use_artifactory=0
if [[ $use_artifactory == 1 ]]; then
		echo "Using properties file"

		# user selcted a deploy configuration, get the properties file from artifactory and
		# use the values in it for the deployment input parameters
		properties_filename=Scalability1.properties
		OPTIONS=$(<"$properties_filename")
else
		OPTIONS="'param_team_location'|'list'|'$team_location'|
         'param_team_name'|'list'|'$team_name'|
         'param_team_slot'|'list'|'$team_slot'|
         'param_build_level_backend'|'list'|'$build_level_backend'|
         'param_build_level_frontend'|'list'|'$build_level_frontend'|
         'param_deployment_datacenter'|'list'|'$deployment_datacenter'|
         'param_dbname'|'string'|'$db_name'|
         'param_dbserver'|'string'|'$db_server'|
         'param_db_username'|'string'|'$db_username'|
         'param_db_password'|'string'|'$db_password'|
         'param_rabbit_host'|'string'|'$rabbit_server'|
         'param_redis_hosts'|'string'|'$redis_server'|
         'param_auth_url'|'string'|'$auth_url'
        "
fi

DESCRIPTION="A test deployment for $PROJECT/$RELEASE ($owner)"

echo "Deploying the deployment"

python ../rsc-deploy.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --application_name "$APPLICATION_NAME" --options "$OPTIONS" --description "$DESCRIPTION"

exit $?
