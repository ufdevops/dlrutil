#!/bin/bash
# This is just a test script

source common.sh

if [[ -z $TERMINATE_DATE ]]; then
   echo "You must set TERMINATE_DATE"
   exit 1
fi

if [[ -z $TERMINATE_TIME ]]; then
   echo "You must set TERMINATE_TIME"
   exit 1
fi

echo "Setting terminate date and time for the deployment"

python rsc-set-terminate-date.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --terminate_date "$TERMINATE_DATE" --terminate_time "$TERMINATE_TIME"

exit $?
