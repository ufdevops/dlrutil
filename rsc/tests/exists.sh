#!/bin/bash
# This is just a test script

source common.sh

echo "Checking if the deployment exists"

python rsc-exists.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME"

exit $?
