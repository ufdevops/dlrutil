#!/bin/bash
# This is just a test script

source common.sh

echo "Getting WFM backend IP from deployment"

# this script outputs two important lines
# WFM_BACKEND_IP=<wfm_backend_ip>
# WFM_FRONTEND_IP=<wfm_frontend_ip>
# make a shell command to set the correct IP
cmd=`python rsc-get-ips.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" | grep WFM_BACKEND_IP`

# evaluate the command to turn the output command into a shell variable
eval $cmd
echo "backend_ip = $WFM_BACKEND_IP"

# now run check to see if wfc is up and running
if [ -z "${WFM_BACKEND_IP}" ]; then
  echo "No WFM backend IP"
	exit 1
fi

if [ -z "${TIMEOUT}" ]; then
  echo "No timeout, using default of 600 seconds"
  TIMEOUT=600
fi

cat > getstatus.sh<<EOF
#!/bin/bash -x

/usr/local/kronos/wfc/bin/wfc status

# Check if wfc is running in offline mode which means there is something wrong with the code
# and it's never going to start correctly.
/usr/local/kronos/wfc/bin/wfc status 2>&1 | egrep 'WFC Server has been started in Offline Mode'
isOffline=\$?
if [[ \$isOffline = 0 ]]; then
   # indicate that the wfc is offline
   exit 99
fi

# Not offline, check to see if all three services (nginx, Tomcat & wfc) are running
numRunning=\`/usr/local/kronos/wfc/bin/wfc status 2>&1 | egrep 'Nginx Web Server is running|Tomcat App Server is running|WFC Server has been started in Online Mode'|wc -l\`

if [[ "\$numRunning" != 3 ]]; then
   echo "PENDING: Some services still need to start"
   rc=1
else
   echo "SUCCESS: All services are running on the WFM backend"
   rc=0
fi

exit "\$rc"
EOF

startTime=`date`
isStillStarting=1
elapsed=0
sleepTime=30
while [[ $isStillStarting == 1 && $elapsed -lt $TIMEOUT ]]; do
	 # execute the getstatus remotely on the backend
	 cat getstatus.sh | sshpass -pkronites ssh -o StrictHostKeyChecking=no root@$WFM_BACKEND_IP
	 isStillStarting=$?
    
	 echo isStillStarting = $isStillStarting

	 # if this comes back as 99, then the wfc is running in offline mode and
	 # can get never get right.  There is something wrong with the code,
	 # so we will fail this script
	 if [[ $isStillStarting == 99 ]]; then
		  echo "The wfc service is offline"
	 elif [[ $isStillStarting == 1 ]]; then
		  # if the backend is still coming up, sleep for 10 seconds and try again
		  sleep $sleepTime
		  let elapsed=elapsed+sleepTime
	 fi
	 
	 echo "Elapsed time: $elapsed"
done
endTime=`date`

echo "Start time: $startTime"
echo "  End time: $endTime"

# Need this or the exit status of this shell will always be failure 
# because the last line in the loop above actually fails when
# everything is started.  If no explicit exit, the status of a shell
# is the status of the last command executed.

# This should be 0 once everything is running.  If not there is
# something drastically wrong
exit $isStillStarting
