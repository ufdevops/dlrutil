#!/bin/bash

export dbserver=keng-dbserver.int.dev.mykronos.com
export environment=Operations
export auth_host=keng01-dev01-ath41-oam41.int.dev.mykronos.com:8080
export extra_inputs='{"tms_url":"Location Default",}'
export APPLICATION_NAME="WFM Services - Team (eng)"
export NOTIFY=domenic.larosa@kronos.com
export DESCRIPTION="Deployed from US KATE server for master test ($NOTIFY)"
export PROJECT=gce_engineering
export RELEASE=master
export EXECUTION_NAME="domenic test deployment job"

export GLOBAL_CONFIG_DIR=global-config
export PYTHONPATH=$GLOBAL_CONFIG_DIR/scripts:.

output_file=params.log
python scripts/rsc/convert_params.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" | tee $output_file
option_string=`grep "OPTIONS=" $output_file`
eval $option_string
echo "The deployment OPTIONS:"
echo "------------------"
echo $OPTIONS
echo "------------------"

python scripts/rsc/rsc-deploy.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --application_name "$APPLICATION_NAME" --options "$OPTIONS" --description "$DESCRIPTION"

exit $?
