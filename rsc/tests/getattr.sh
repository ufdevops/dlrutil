#!/bin/bash
# This is just a test script

source common.sh

output_fn_base=set_deployment_parameters
output_fn_base=$1
output_fn=$output_fn_base.sh

echo "Getting JSON for the deployment"

rm -f $output_fn

python rsc-get-attributes.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --show_all --output_fn_base "$output_fn_base"
rc=$?
if [[ $rc == 0 ]]; then
		cat $output_fn; 
		source $output_fn
		echo "output_url_back = $output_url_back"
		echo "output_url_front = $output_url_front"
fi
exit $rc

python rsc-get-attributes.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --outputs "output_url_back, output_url_front" --output_fn_base "$output_fn_base"
rc=$?
if [[ $rc == 0 ]]; then	cat $output_fn_base; fi

python rsc-get-attributes.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --outputs "output_datacenter, output_redis_hosts" --output_fn_base "$output_fn_base"
rc=$?
if [[ $rc == 0 ]]; then	cat $output_fn_base; fi

python rsc-get-attributes.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --inputs "param_image_type_ilb,param_dbname" --output_fn_base "$output_fn_base"
rc=$?
if [[ $rc == 0 ]]; then	cat $output_fn_base; fi

python rsc-get-attributes.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --outputs "output_datacenter, output_redis_hosts"  --inputs "param_image_type_ilb,param_dbname" --output_fn_base $output_fn_base
rc=$?
if [[ $rc == 0 ]]; then	cat $output_fn_base; fi

python rsc-get-attributes.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --show_all --output_fn_base "$output_fn_base"
rc=$?
if [[ $rc == 0 ]]; then	cat $output_fn_base; fi
