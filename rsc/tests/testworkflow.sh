#!/bin/bash
# This is just a test script

source common.sh

make -C $GLOBAL_CONFIG_DIR PROJECT=$PROJECT RELEASE=$RELEASE

# Destroy the old testing environment
$bindir/destroy.sh

# Rebuild the database

# Deploy WFM Services
export STATE="running"
$bindir/deploy.sh
if [[ $? != 0 ]]; then
		echo "Deployment failed"
		exit 1
fi
$bindir/wait.sh
if [[ $? != 0 ]]; then
		echo "Wait for deployment failed"
		exit 1
fi
$bindir/show.sh

# Excercise some functions and get some info
$bindir/getbackendip.sh
$bindir/getfrontendip.sh
$bindir/setterminate.sh
    
# Set the number of days the deployment will run
$bindir/cancelStops.sh
$bindir/show.sh

# Check if wfc is online
$bindir/checkonline.sh

# run WFM tests
echo "TODO: create some WFM tests"

# Test stop
export STATE="stopped"
$bindir/stop.sh
if [[ $? != 0 ]]; then
		echo "Stop failed"
		exit 1
fi
$bindir/wait.sh
if [[ $? != 0 ]]; then
		echo "Wait for stop failed"
		exit 1
fi
$bindir/show.sh

# Restart deployment
export STATE="running"
$bindir/start.sh
if [[ $? != 0 ]]; then
		echo "Restart failed"
		exit 1
fi
$bindir/wait.sh
if [[ $? != 0 ]]; then
		echo "Wait for start failed"
		exit 1
fi
$bindir/show.sh

# Terminate deployment for good
export STATE="terminated"
$bindir/terminate.sh
if [[ $? != 0 ]]; then
		echo "Terminate failed"
		exit 1
fi
$bindir/wait.sh
if [[ $? != 0 ]]; then
		echo "Wait for terminate failed"
		exit 1
fi
$bindir/show.sh
