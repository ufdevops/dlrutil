#!/bin/bash
# This is just a test script

source common.sh

echo "Getting WFM IPs from deployment"

python rsc-get-ips.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME"

exit $?
