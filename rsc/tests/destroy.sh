#!/bin/bash
# This is just a test script

source "common.sh"

$bindir/exists.sh
# the logic is backwards but kept with standard of 0 is success (it exists)
# if 1 returned it does not exist
if [[ $? != 0 ]]; then
		echo "Deployment $EXECUTION_NAME does not exist.  No need to destroy."
		exit 0
fi		

# Terminate the deployment 
export STATE="terminated"
$bindir/terminate.sh
$bindir/wait.sh

# Delete the deployment 
export STATE="deleted"
$bindir/delete.sh
$bindir/wait.sh

# Remove front and backend hosts from DNS just in case it failed in RightScale
export SUBNAME=$EXECUTION_NAME
export MANAGED_ZONE=$RELEASE_MANAGED_ZONE
$bindir/../../../rightscale/scripts/dns/removedns2.sh
