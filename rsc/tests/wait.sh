#!/bin/bash
# This is just a test script

source common.sh

echo "Waiting for state, $STATE, for the deployment"

python rsc-wait-state.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --timeout "$TIMEOUT" --state "$STATE"

exit $?
