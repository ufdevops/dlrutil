#!/bin/bash
# This is just a test script

source common.sh

echo "Getting WFM frontend IP from deployment"

# this script outputs two important lines
# WFM_BACKEND_IP=<wfm_backend_ip>
# WFM_FRONTEND_IP=<wfm_frontend_ip>
# make a shell command to set the correct IP
cmd=`python rsc-get-ips.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" | grep WFM_FRONTEND_IP`

# evaluate the command to turn the output command into a shell variable
eval $cmd
return_code=$?
echo "frontend_ip = $WFM_FRONTEND_IP"

exit $return_code
