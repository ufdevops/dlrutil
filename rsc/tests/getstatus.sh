#!/bin/bash -x
# This is just a test script

/usr/local/kronos/wfc/bin/wfc status

# Check if wfc is running in offline mode which means there is something wrong with the code
# and it's never going to start correctly.
/usr/local/kronos/wfc/bin/wfc status 2>&1 | egrep 'WFC Server has been started in Offline Mode'
isOffline=$?
if [[ $isOffline = 0 ]]; then
   # indicate that the wfc is offline
   exit 99
fi

# Not offline, check to see if all three services (nginx, Tomcat & wfc) are running
numRunning=`/usr/local/kronos/wfc/bin/wfc status 2>&1 | egrep 'Nginx Web Server is running|Tomcat App Server is running|WFC Server has been started in Online Mode'|wc -l`

if [[ "$numRunning" != 3 ]]; then
   echo "PENDING: Some services still need to start"
   rc=1
else
   echo "SUCCESS: All services are running on the WFM backend"
   rc=0
fi

exit "$rc"
