#!/bin/bash
# This is just a test script

source common.sh

echo "Publishing CATs"

python rsc-publish-cats2.py --project "$PROJECT" --release "$RELEASE" \
  --config_root_dir "$GLOBAL_CONFIG_DIR"

exit $?
