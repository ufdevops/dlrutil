#!/bin/bash
# This is just a test script

source common.sh

echo "Launching the deployment"

python rsc-deployment.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" --name "$EXECUTION_NAME" --action "launch"

exit $?
