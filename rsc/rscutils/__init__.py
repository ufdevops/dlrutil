#!/usr/bin/env python
###############################################################################
# (C) 2016 - Kronos Inc.
###############################################################################

import os
import json
import time
import rscutils.logger as logger

class NoDeploymentError(Exception):
    """Exception raised when a deployment does not exist."""
    pass

class NoScheduleError(Exception):
    """Exception raised when a schedule does not exist."""
    pass

def check_env_vars(evars):
    missing = False
    for e in evars:
        if os.getenv(e) == None:
            print('You must set ' + e)
            missing = True
    if missing:
        print("Exiting because you are missing some environment variables.")
        exit (1)
    return

def get_all_deployments(ss_api,project_id,view="default"):
    # get all the deployments
    deployments=ss_api.index_execution(
        project_id=project_id,
        view=view
    )
    return deployments

def get_deployment_by_id(ss_api,project_id,deployment_id):
    deployment=ss_api.show_execution(
        project_id=project_id,
        id=deployment_id,
        view="expanded"
    )

    if deployment == None:
        raise NoDeploymentError("No deployment with id " + deployment_id)

    return deployment

def get_deployment_internal(ss_api, project_id, name):
    deployment = None
    deployments = get_all_deployments (ss_api, project_id)
    # search for the correct deployment and grab the appropriate key if found
    for a in deployments:
        if a['name'] == name:
            deployment = a
            break

    if deployment == None:
        raise NoDeploymentError("No deployment named " + name)

    return deployment
    
def get_deployment_id(ss_api, project_id, name):
    deployment_id = cache_get(name)
    if deployment_id == None:
        deployment=get_deployment_internal(ss_api,project_id,name)
        if deployment != None:
            deployment_id = deployment['id']
            cache_store(name,deployment_id)

    return deployment_id

def wait_deployment(ss_api, project_id, name, state, timeout):

    # assume it will timeout
    return_code = 1

    # Look for an existing deployment with the right name
    deployment_id = get_deployment_id(ss_api, project_id, name)

    sleep_time=30
    elapsed=0
    status='unknown'
    while status != state and elapsed < timeout:
        json = ss_api.show_execution(project_id=project_id, id=deployment_id)
        status = json['status']
        if len(status) == 0:
            logger.warning("No deployment named, " + name)
            # special case for deleted, can't delete what it isn't there so we are good
            if state == "deleted":
                logger.info ("Looking for deleted but there is no deployment, so we are bailing")
                return_code = 0
            break
        
        # special case when looking for running, if it fails or terminates for whatever reason, bail
        if state == "running" and (status == "failed" or status == "terminated"):
            logger.info ("Looking for running but the deployment failed, so we are bailing")
            return_code = 1
            break

        logger.info ("Looking for status, " + state + ", got status for " + name + ", " + status + " (" + str(elapsed) + ")")
        
        if status == state:
            return_code=0
        else:
            time.sleep(sleep_time)
            elapsed = elapsed + sleep_time

    return return_code

def set_terminate_date(ss_api, project_id, name, terminate_date, terminate_time):
    return_code = 1
    logger.info("rscutils.set_terminate_date not implemented yet")

    return return_code

def get_deployment_status(ss_api,project_id,deployment_id):
    status='unknown'
    json = ss_api.show_execution(project_id=project_id, id=deployment_id)
    if "status" in json:
        status = json['status']

    return status

def launch_deployment(ss_api, project_id, deployment_id):
    # TODO: check all status and only try if you can
    status = get_deployment_status(ss_api,project_id,deployment_id)
    if status != "running":
        ss_api.launch_execution(
            project_id=project_id,
            id=deployment_id
        )
    else:
        logger.info ("Deployment is already running")
        
    return 0

def start_deployment(ss_api, project_id, deployment_id):
    # TODO: check all status and only try if you can
    status = get_deployment_status(ss_api,project_id,deployment_id)
    if status != "running":
        ss_api.start_execution(
            project_id=project_id,
            id=deployment_id
        )
    else:
        logger.info ("Deployment is already running")
    return 0

def stop_deployment(ss_api, project_id, deployment_id):
    # TODO: check all status and only try if you can
    status = get_deployment_status(ss_api,project_id,deployment_id)
    if status != "stopped":
        ss_api.stop_execution(
            project_id=project_id,
            id=deployment_id
        )
    else:
        logger.info ("Deployment is already stopped")
    return 0

def terminate_deployment(ss_api, project_id, deployment_id):
    # TODO: check all status and only try if you can
    status = get_deployment_status(ss_api,project_id,deployment_id)
    if status != "terminated":
        ss_api.terminate_execution(
            project_id=project_id,
            id=deployment_id
        )
    else:
        logger.info ("Deployment is already terminated")
    return 0

def delete_deployment(ss_api, project_id, deployment_id):
    # TODO: check all status and only try if you can
    ss_api.delete_execution(
        project_id=project_id,
        id=deployment_id
    )
    return 0

def has_tag(cm_api,deployment,exclusion_tag):
    deployment_href = deployment['deployment']
    deployment_tags = cm_api.by_resource_tag(resource_hrefs=[deployment_href])
    
    has_tag = False        
    if deployment_tags and len(deployment_tags) > 0:
        for tag in deployment_tags[0]['tags']:
            if exclusion_tag in tag['name']:
                has_tag = True
                break
                
    return has_tag

def show_deployment(ss_api, project_id, deployment_id):
    json = ss_api.show_execution(
        project_id=project_id,
        id=deployment_id,
        view='expanded'
    )

    created_by = json['created_by']
    timestamps = json['timestamps']
    cost = json['cost']
    if 'next_action' in json:
        next_action = json['next_action']['action'] + ' at ' + json['next_action']['next_occurrence']
    else:
        next_action = None

    print('           id: ', json['id'])
    print('         name: ', json['name'])
    print('         href: ', json['href'])
    print('  description: ', json.get('description', None))
    print('       status: ', json['status'])
    print('         cost: ', cost['unit'] + cost['value'])
    print('   created_by: ', created_by['name'] + ' (' + created_by['email'] + ')')
    print('   created_at: ', timestamps['created_at'])
    print('  launched_at: ', timestamps['launched_at'])
    print('terminated_at: ', timestamps['terminated_at'])
    print('    scheduled: ', json['scheduled'])
    print('  next_action: ', next_action)
    
    for co in json['configuration_options']:
        print(co['name'] + " = " + co['value'])

    for o in json['outputs']:
        print(o['name'] + " = " + o['value']['value'])

    return 0

rsc_cache = {}
def cache_store(key,value):
    rsc_cache[key] = value

def cache_get(key):
    if key in rsc_cache:
        return rsc_cache[key]
    else:
        return None

def get_deployment(ss_api, project_id, deployment_id):
    deployment = cache_get(deployment_id)
    if deployment == None:
        deployment = get_deployment_by_id(ss_api, project_id, deployment_id)
        cache_store(deployment_id,deployment)

    return deployment

def get_all_cats(ss_api,project_id,deployable):
    # get all the CATs names
    # if deployable true, get only deployable CATs (ie. CATs not in a package)
    # if deployable false, get all

    cat_names = []
    all_cats = get_all_templates(ss_api,project_id)
    for cat in sorted(all_cats):
        # if in package, it's not a deployable cat
        if not deployable or (deployable and not "package" in cat):
            cat_names.append(cat["name"])
                                 
    return cat_names

def get_all_applications(ss_api,project_id):
    # get all the applications
    applications=ss_api.index_application(
        catalog_id=project_id
    )
    return applications

def get_application(ss_api,project_id,name):
    application = None
    applications = get_all_applications (ss_api, project_id)
    # search for the correct application and grab the appropriate key if found
    for a in applications:
        if a['name'] == name:
            application = a
            break
    return application
    
def get_all_templates(ss_api,project_id):
    # get all the templates
    templates=ss_api.index_template(
        collection_id=project_id
    )
    return templates

def get_template(ss_api,project_id,name):
    template = None
    templates = get_all_templates (ss_api, project_id)
    # search for the correct template and grab the appropriate key if found
    for a in templates:
        if a['name'] == name:
            template = a
            break
    return template

def get_application_value(ss_api,project_id,name,key,application=None):
    value = None
    
    # check all published CATs (applications)
    if application == None:
        application=get_application(ss_api,project_id,name)

    # search for the correct application and grab the appropriate key if found
    if application != None and key in application:
        value = application[key]
        
    if value == None:
        raise Exception ("Error getting application key, " + key + ", for application, " +
                         name + ".  No such application exists.")

    return value

def publish_cat(ss_api, project_id, cat_name, application_name, schedule_id):
    logger.info("Publishing CAT file, " + cat_name)
    # try to find it in designer
    template_id = get_template_value (ss_api, project_id, cat_name, 'id')
    
    application_href_override = None
    application = get_application(ss_api,project_id,application_name)
    if application != None:
        application_id = application['id']
        application_href_override="/api/catalog/catalogs/" + project_id + "/applications/" + application_id

    # publish the CAT
    return_code = publish_template_by_id(ss_api, project_id, template_id, application_name,
                                         schedule_id, application_href_override)
    
    return return_code, template_id

def get_template_value(ss_api,project_id,name,key,template=None):
    value = None
    
    # check all published CATs (templates)
    if template == None:
        template=get_template(ss_api,project_id,name)

    # search for the correct template and grab the appropriate key if found
    if template != None and key in template:
        value = template[key]
        
    if value == None:
        raise Exception ("Error getting template key, " + key + ", for template, " +
                         name + ".  No such template exists.")

    return value

def publish_template_by_id(ss_api,project_id,template_id,application_name,schedule_id,application_href_override=None):
    if application_href_override == None:
        ss_api.publish_template(
            collection_id=project_id,
            id=template_id,
            name=application_name,
            schedules=[schedule_id]
        )
    else:
        ss_api.publish_template(
            collection_id=project_id,
            id=template_id,
            name=application_name,
            schedules=[schedule_id],
            overridden_application_href=application_href_override
        )
        
    return 0

def unpublish_template_by_id(ss_api,project_id,template_id):
    ss_api.unpublish_template(
        collection_id=project_id,
        id=template_id
    )
    return 0

def get_all_schedules(ss_api,project_id):
    schedules = ss_api.index_schedule(
        collection_id=project_id
    )
    return schedules

def get_schedule_by_name(ss_api,project_id,name):
    schedule = 0

    schedules = get_all_schedules(ss_api,project_id)
    for a in schedules:
        if a['name'] == name:
            schedule = a
            break

    if schedule == None:
        raise NoScheduleError("No schedule named " + name)

    return schedule

def get_schedule_id(ss_api,project_id,name):
    logger.info("Getting schedule")
    schedule = get_schedule_by_name(ss_api,project_id,name)
    schedule_id = schedule['id']
    logger.info("Schedule id: " + schedule_id)

    return schedule_id
