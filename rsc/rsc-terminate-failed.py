#!/usr/bin/env python
###############################################################################
# (C) 2016 - Kronos Inc.
###############################################################################
# This script will email both CIA and users of terminated rightscale
# deployments and let them know that their deployment failed and was
# terminated as a result.
###############################################################################

###############################################################################
# Imports
###############################################################################
# Python imports
import argparse
import datetime
import cgi
import os
import json
import sys
import time
# dateutil import
from dateutil.tz import tzlocal
from dateutil import parser
# email imports
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.MIMEBase import MIMEBase
from email import Encoders
# os imports
from os.path import basename
# rsc imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
from rsc.cmd import RscCommand
# smtplib imports
from smtplib import SMTP_SSL as SMTP

###############################################################################
# Globals
###############################################################################

# Self-service API.
ss_api = SelfService()

# Cloud management API.
cm_api = CM15()

###############################################################################
# Global
###############################################################################

# This is the list of people to notify when a deployment is terminated.
gEmailsToNotify = [
    'deniz.sezen@kronos.com', 
    'domenic.larosa@kronos.com',
    'bhrigu.malhotra@kronos.com',
    'james.lennon@kronos.com',
    'brian.prendergast@kronos.com',
    'daniel.enright@kronos.com',
    'austin.johnson@kronos.com',
    'Prashant.Rawat@kronos.com',
    'alan.tsao@kronos.com',
    'sean.spencer@kronos.com',
    'paul.saraceno@kronos.com'
]

# This dictionary will be filled with information about stranded or
# failed deployments
gDataDictionary = {}

###############################################################################
# Functions
###############################################################################

def parse_arguments():
    """Parses required arguments for this script."""
    parser = argparse.ArgumentParser(description='Terminate failed WFM deployments.')
    parser.add_argument('-pi','--project-id', type=str, help='Project ID (for rightscale)', action='store', dest='project_id')
    parser.add_argument('-u','--smtpuser', type=str, help='SMTP Username', action='store', dest='smtpuser')
    parser.add_argument('-p','--password', type=str, help='SMTP Password', action='store', dest='password')
    parser.add_argument('-a','--termination-age', type=str, help='Age (in days) after which to terminate a failed deployment.', action='store', dest='terminate_age')

    args = parser.parse_args()
    error = False

    # Check for valid arguments.
    if not args.smtpuser:
        print "SMTP Username required."
        error = True
    elif not args.password:
        print "SMTP password required."
        error = True
    elif not args.terminate_age:
        print "Termination age required."
        error = True
    elif not args.project_id:
        print("Project ID required.")
        error = True

    # Quit if we have a problem.
    if error:
        sys.exit(1)

    return args.smtpuser, args.password, int(args.terminate_age), args.project_id


def send_notification(smtpuser, password, deployment):
    """
    Sends an email notification via Amazon's SMTP when a deployment
    has been terminated due to it being stuck in failure mode.
    :param smtpuser: Username for SMTP service.
    :param password: Password for SMTP service.
    :param deployment: JSON data for the deployment.
    :return: None.
    """

    # Setup connection parameters for SMTP.
    smtp_server = "email-smtp.us-east-1.amazonaws.com"
    sender = "Service Account <svcCI@KRONOS.COM>"

    # Add the creator of the deployment as a recipient.
    deployment_recipients = gEmailsToNotify[:]
    deployment_recipients.insert(0, deployment['created_by']['email'])

    # Commented out for debug purposes.
    # deployment_recipients = ["deniz.sezen@kronos.com"]

    # Generate the self-service link for the deployment.
    deployment_ss_link = 'https://selfservice-4.rightscale.com/manager/exe/%s' % deployment['href'].split('/')[6]

    # Get the deployment creation time.
    deployment_create_date = parser.parse(deployment['timestamps']['created_at'])
    deployment_create_date = deployment_create_date.strftime('%d %B %Y')

    # Setup the message header for the user.
    msg = MIMEMultipart('alternative')
    msg['From'] = sender
    msg['To'] = ", ".join(deployment_recipients)
    msg['Subject'] = "RightScale: Failed Deployment: %s" % deployment

    body = \
        """
        Hi {0},<br>
        <br>
        We've detected that your deployment <b><a href={1}>{2}</a></b> is in an unrecoverable
        state. You created this deployment on <b>{3}</b>.<br>
        <br>
        We've automatically terminated this deployment for you.<br>
        <br>
        Thanks,<br>
        -The CIA<br>
        <br>
        <b>Debug information for the Cloud Integration Automation (CIA) team:</b><br>
        <b><a href={4}>This</a></b> is the link to the failed deployment in RightScale cloud management.<br>
        <br>
        """.format(
            deployment['created_by']['name'],
            deployment_ss_link,
            deployment['name'],
            deployment_create_date,
            deployment['deployment_url'])

    # Create the text portion of the message and attach it.
    text_portion = MIMEText(body, 'html')
    msg.attach(text_portion)

    # SSL SMTP Connection
    try:
        # Open connection.
        conn = SMTP(smtp_server)

        # Setup connection attributes.
        conn.set_debuglevel(False)

        # Login to the machine.
        conn.login(smtpuser, password)

        # Send the email.
        conn.sendmail(sender, deployment_recipients, msg.as_string())

        # Close the connection.
        conn.close()

    except Exception, exc:
        sys.exit( "mail failed; %s" % str(exc) ) # give a error message

    time.sleep(2) # Required for SES sending rates


###############################################################################
# Implementation
###############################################################################

# Process the command line arguments.
arg_smtp_user, arg_smtp_password, arg_terminate_days, arg_project_id = parse_arguments()

# First off, we need to make sure we've queried for all of the deployments.
if not os.path.isfile('./executions.dat'):
    print('Querying for all RightScale deployments..')

    # Perform the query.
    executions = ss_api.index_execution(
        project_id=arg_project_id,
        view='expanded',
        filter=['status==failed'])

    # Write the data to a file.
    with open('./executions.dat', 'w') as out_file:
        json.dump(executions, out_file)
else:
    print('Using cached executions.dat file..')
    with open('./executions.dat', 'r') as in_file:
        executions = json.load(in_file)

# Get the current date/time with the timezone.
current_time = datetime.datetime.now(tzlocal())

# List of failed executions to terminate.
executions_to_terminate = []

# Now begin iterating through each deployment.
for execution in executions:
    print('Processing execution %s.' % execution['name'])

    # First step: Ensure that the deployment is actually failed.
    # This is just an extra check for safety.
    if execution['status'] != 'failed':
        print('\tSkipping deployment in %s state.' % deployment_data['status'])
        continue

    # Get the HREF for the deployment.
    if "deployment" not in execution:
        print("\tSkipping execution without deployment.")
        continue

    # Get the deployment ID.
    deployment_href = execution["deployment"]
    deployment_id = deployment_href.split('/')[3]

    # Retrieve the full data for this deployment.
    ShowDeploymentCmd = RscCommand("cm16", ["show", deployment_href, "view=full"])
    try:
        deployment_data = json.loads(ShowDeploymentCmd.execute())
    except:
        print("\tCould not get deployment data for %s." % execution["name"])
        continue

    deployment_tags = deployment_data["tags"]
    if not deployment_tags or len(deployment_tags) == 0:
        print('\tSkipping deployment. No tags found..')
        continue

    # If we found the exclusion tag, skip this deployment.
    exclusion_tag = "kronos:do_not_delete=true"
    if exclusion_tag in deployment_tags:
        print("\tSkipping deployment -- excluded from deletion.")
        continue

    # If the deployment is locked, skip it.
    if deployment_data["locked"]:
        print("\tSkipping deployment -- locked.")
        continue

    # Iterate through each instance within the deployment.
    stranded_instances = []
    for instance in deployment_data["instances"]:
        # Skip instance data marked with is_next as it means nothing.
        if instance["is_next"]:
            print("\tSkipping next_instance data.")
            continue

        # Skip locked instances.
        if instance["locked"]:
            print("\tSkipping locked instance %s." % instance["name"])
            continue

        # Ensure that the server is failed.
        if instance["state"] != "stranded":
            print("\tSkipping non-stranded server %s in state %s." % (instance["name"], instance["state"]))
            continue

        # Now that we know it's stranded, get the stranded_at time.
        if "stranded_at" not in instance["timestamps"]:
            print("\tCan't find stranded timestamp for server %s." % instance["name"])
            continue

        # Convert to a datetime object.
        failure_date = parser.parse(instance["timestamps"]["stranded_at"])

        # Subtract the failure date from the current time. This yields a
        # time delta object.
        time_delta = current_time - failure_date

        # Debug message.
        print('\tInstance failure date: %s' % failure_date)
        print('\tDays since failure: %d' % time_delta.days)

        # Is the delta greater than the given time (in days)? If so, add the instance
        # to the stranded_instances list.
        if time_delta.days >= arg_terminate_days:
            stranded_instances.append(instance)

    # If we have at least one instance that is stranded, then we need to
    # kill the deployment.
    if len(stranded_instances) > 0:
        print("\tScheduling execution for termination (%d failed servers)." % len(stranded_instances))
        executions_to_terminate.append(execution)

# Debug message.
print('There are %d executions to terminate.' % len(executions_to_terminate))

# Iterate through each execution to terminate and kill it.
for execution in executions_to_terminate:
    try:
        # Kill the deployment.
        ss_api.terminate_execution(project_id=arg_project_id, id=execution['href'].split('/')[6])

        # Notify the execution creator that their execution was terminated.
        send_notification(arg_smtp_user, arg_smtp_password, execution)
    except Exception as e:
        print("An exception occurred while trying to terminate execution %s: %s" % (
            execution["name"],
            repr(e)
        ))
        continue
