###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This script will find all deployments in rightscale and terminate them
#  if they match the following criteria:
#    - Only launched by WFM Services - Team
#    - Only launched by WFM Services - User
#    - Only in the dev environment.
#    - Only if they point to the rabbitmq server:
#       * keng01-dev01-dmq02.dev.mykronos.internal
#       * keng01-dev01-dmq70.dev.mykronos.internal
###############################################################################
# Pre-requisites:
#  - This script assumes you are authenticated with the 'rsc' command locally
#    on the machine you are running the script on.
# -----------------------------------------------------------------------------
#  - This script also requires the 'ansicolors' package installed. pip install
#    it by doing `pip install ansicolors`.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
from argparse import ArgumentParser
# json imports
import json
# os imports
from os import path
# rsc imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
from rsc.cmd import RscCommand
# sys imports
from sys import exit
# colors imports
from colors import red, green, blue, yellow, white
# multiprocessing imports
from concurrent import futures

###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()
executor = futures.ThreadPoolExecutor(6)

###############################################################################
# Functions
###############################################################################

def parse_arguments():
    """
    Parses arguments required by this program.
    :return: Values of said arguments.
    """

    parser = ArgumentParser(description='Tags deployments in bulk.')

    parser.add_argument('-pi', '--project_id', type=str,
                        help='RightScale project ID to query against.',
                        action='store', dest='project_id',
                        required=True)

    args = parser.parse_args()

    return args.project_id

def get_output(deployment, output_name):
    # Ignore any deployments that aren't in dev.
    output_object = None
    for output in deployment["outputs"]:
        if output["name"] == output_name:
            output_object = output

    if output_object and output_object["value"] != None:
        return output_object["value"]["value"]

    return None

def get_input(resource_inputs, input_name):
    # Find the RABBIT_HOST input.
    input_object = None
    for input in resource_inputs:
        if input["name"] == input_name:
            input_object = input
            break

    if not input_object:
        return None

    # Get input value.
    return input_object["value"].split('text:')[1]


def check_deployment(deployment):
    print green("Processing deployment %s" % deployment["name"])

    # Ignore any deployments that weren't launched via our CATs.
    deployment_cat = deployment["launched_from_summary"]["value"]["name"]
    if deployment_cat not in ["WFM Services - User", "WFM Services - Team"]:
        print white("\tSkipping this deployment as it was launched by %s." %  deployment_cat)
        return

    # Ignore any deployments that are terminated.
    if deployment["status"] == "terminated":
        print white("\tSkipping this deployment as it is terminated.")
        return

    # Skip this deployment if we don't have a deployment href..
    if "deployment" not in deployment:
        print red("\tSkipping deployment: No deployment href!")
        return

    # Check to see if the do_not_delete tag exists on the deployment.
    deployment_href = deployment['deployment']
    deployment_tags = cm_api.by_resource_tag(resource_hrefs=[deployment_href])

    if not deployment_tags or len(deployment_tags) == 0:
        print red('\tSkipping deployment. No tags found..')
        busted_deployments.append(deployment)
        return

    # Read out all the tags the deployment has.
    tags = []
    for tag in deployment_tags[0]['tags']:
        tags.append(tag['name'])

    # If we found the exclusion tag, skip this deployment.
    exclusion_tag = "kronos:do_not_delete=true"
    if exclusion_tag in tags:
        print red("\tSkipping deployment. It's excluded!")
        return

    # Ignore any deployments that aren't in dev.
    subnet_output = get_output(deployment, "output_subnetwork")
    if not subnet_output:
        print red("\tSkipping deployment as it has no subnet output!")
        busted_deployments.append(deployment)
        return

    if "dev-net" not in subnet_output:
        print white("\tSkipping deployment as it's in the wrong subnet: %s" % subnet_output)
        return

    # Get backend fqdn.
    backend_fqdn = get_output(deployment, "output_url_front")
    frontend_fqdn = get_output(deployment, "output_url_back")

    print yellow("This deployment has %d resources.." % len(deployment["api_resources"]))

    # Iterate through every server in the deployment.
    for resource in deployment["api_resources"]:
        # Get resource details.
        resource_type = resource["type"]
        resource_details = resource["value"]["details"]

        # If there are no resource details, we need to skip this deployment.
        if not resource_details:
            print red("\tDeployment resource has invalid data.")
            busted_deployments.append(deployment)
            return

        # If the resource is failed, we're going to terminate it anyway.
        if resource_details["state"] != "operational":
            print white("Deployment will be terminated as resource %s is %s." % (
                resource_details["name"], resource_details["state"]))

            return {
                "deployment": deployment,
                "subnet": subnet_output,
                "backend_fqdn": backend_fqdn,
                "frontend_fqdn": frontend_fqdn
            }

        resource_name = resource_details["name"]
        print green("\tProcessing resource %s.." % resource_name)

        # Is the resource a server?
        if resource_type != "servers":
            print red("\tSkipping resource %s of type %s." % (resource_name, resource_type))
            continue

        # Get current instance details.
        current_instance = resource_details["current_instance"]

        # See if we can find the inputs link.
        inputs_href = None
        for link in current_instance["links"]:
            if link["rel"] == "inputs":
                inputs_href = link["href"]
                break

        if not inputs_href:
            print red("\tSkipping resource: No inputs link..")
            busted_deployments.append(deployment)
            continue

        # Construct a command to index the inputs for this resource.
        IndexInputsCmd = RscCommand("cm15", ["index", inputs_href])

        try:
            resource_inputs = json.loads(IndexInputsCmd.execute())
        except Exception as e:
            print red("\tSkipping this resource as we couldn't get its inputs.")
            continue

        # Find the RABBIT_HOST input.
        input_rabbit_host = get_input(resource_inputs, "RABBIT_HOST")

        if not input_rabbit_host:
            print red("\t\tSkipping resource: No RABBIT_HOST input!")
            busted_deployments.append(deployment)
            continue

        print blue("\t\tRabbitMQ host: %s" % input_rabbit_host)

        # Does this input fall under our two rabbitmq servers?
        if input_rabbit_host not in ["keng01-dev01-dmq02.dev.mykronos.internal", "keng01-dev01-dmq70.dev.mykronos.internal"]:
            print red("\t\t\tSkipping resource as it points to RabbitMQ host: %s" % input_rabbit_host)
            continue

        print blue("\t\t\tScheduling deployment for termination.")

        # Add this deployment to the list.
        return {
            "deployment": deployment,
            "subnet": subnet_output,
            "rabbit": input_rabbit_host,
            "frontend_fqdn": frontend_fqdn,
            "backend_fqdn": backend_fqdn,
            "tags": ",".join(tags)
        }

    return None


###############################################################################
# Implementation
###############################################################################

# Remove this line if required!
print("This script is intentionally aborting. Do not run this script unless " +
      "you know what you are doing!")
exit(0)

# Parse arguments to program.
arg_project_id = parse_arguments()

# First off, we need to make sure we've queried for all of the deployments.
if not path.isfile('./executions.dat'):
    print yellow('Querying for all RightScale deployments..')

    # Perform the query.
    deployments = ss_api.index_execution(
        project_id=arg_project_id,
        view="expanded")

    # Write the data to a file.
    with open('./executions.dat', 'w') as out_file:
        json.dumps(deployments, out_file)
else:
    print yellow('Using cached executions.dat file..')
    with open('./executions.dat', 'r') as in_file:
        deployments = json.load(in_file)
    print yellow("There are %d deployments to consider." % len(deployments))

# Stores deployments we're going to delete.
deployment_progress = 0

# Lists.
deployments_to_terminate = []
busted_deployments = []

for deployment in deployments:
    #result = check_deployment(deployment)
    #f result != None:
    #    deployments_to_terminate.append(result)

    future_result = executor.submit(check_deployment, deployment)
    deployments_to_terminate.append(future_result)

# Terminate the threadpool and wait until everything completes.
executor.shutdown(wait=True)

# Open up a handle to a csv file.
csv_data = open("./deployments_to_nuke.csv", "w")
csv_data.write("Deployment href,Deployment name,Deployment subnet,Deployment RabbitMQ,Frontend URL,Backend URL,Tags\n")

# Now iterate through each deployment.
for deployment_data in deployments_to_terminate:
    # Get the result from the futures object.
    deployment_data = deployment_data.result()

    if not deployment_data:
        continue

    # Get the data within the dictionary.
    if "deployment" in deployment_data:
        deployment = deployment_data["deployment"]
    else:
        deployment = None

    if "subnet" in deployment_data:
        deployment_subnet = deployment_data["subnet"]
    else:
        deployment_subnet = "None"

    if "rabbit" in deployment_data:
        deployment_rabbit = deployment_data["rabbit"]
    else:
        deployment_rabbit = "None"

    if "frontend_fqdn" in deployment_data:
        frontend_fqdn = deployment_data["frontend_fqdn"]
    else:
        frontend_fqdn = None

    if "backend_fqdn" in deployment_data:
        backend_fqdn = deployment_data["backend_fqdn"]
    else:
        backend_fqdn = None

    if "tags" in deployment_data:
        tags = deployment_data["tags"]
    else:
        tags = "None"

    csv_data.write("%s,%s,%s,%s,%s,%s,%s\n" % (
        deployment["href"], deployment["name"],
        deployment_subnet, deployment_rabbit,
        frontend_fqdn, backend_fqdn, tags))

csv_data.close()
#print green("We are going to terminate %d deployments." % len(deployments_to_terminate))



















