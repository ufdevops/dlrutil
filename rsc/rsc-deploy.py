###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  A utility to deploy/launch a CAT in RightScale
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################
# usage: rsc-deploy.py [-h] --project PROJECT --release RELEASE --config_root_dir CONFIG_ROOT_DIR
#                      --name NAME --application_name APPLICATION_NAME --options
#                      OPTIONS --description DESCRIPTION
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment on which to
#                         operate.
#   --application_name APPLICATION_NAME
#                         The name of the RightScale CAT to launch.
#   --options OPTIONS     The options to pass to the CAT.
#   --description DESCRIPTION
#                         The description of the deployment.
###############################################################################

import argparse
import json
import os

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

ss_api = SelfService()

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment on which to operate.',
                        required=True)
    parser.add_argument('--application_name',
                        help='The name of the RightScale CAT to launch.',
                        required=True)
    parser.add_argument('--options',
                        help='The options to pass to the CAT.',
                        required=True)
    parser.add_argument('--description',
                        help='The description of the deployment.',
                        required=True)

    return parser.parse_args()

def check_env_vars(evars):
    missing = False
    for e in evars:
        if os.getenv(e) == None:
            logger.error('You must set ' + e)
            missing = True
    if missing:
        logger.error ("Exiting because you are missing some environment variables.")
        exit (1)
    return

def main():
    check_env_vars (["PYTHONPATH"])

    return_code = 1

    args = parse_arguments()
    config_root_dir = args.config_root_dir
    execution_name = args.name
    application_name = args.application_name
    options = args.options
    description = args.description

    config = ConfigJSON(config_root_dir, args.project, args.release)
    
    project_id = config.get ('project_id')
    ignore_label = config.get ('ignore_label')

    logger.info ("application_name: " + application_name)

    # we'll assume that cat is not published and we'll need to unpublish after done deploying so
    # as not to litter the catalog
    need_to_unpublish = True
    try:
        application = rscutils.get_application (ss_api, project_id, application_name)
        application_id = rscutils.get_application_value (ss_api, project_id, application_name, 'id', application)

        # If we got here the above calls succeeded so we know it's published already and we just need
        # to republish.  Not going to unpublish afterwards.
        need_to_unpublish = False
    except Exception as e:
        logger.warning("Application, " + application_name + ", not published.  Publishing now.")

    # always publish so we get new CAT changes in case it already exists
    # get the default schedule
    schedule_name = config.get ('default_schedule_name')
    schedule_id = rscutils.get_schedule_id(ss_api, project_id, schedule_name)
    
    # publish the cat, publish with the CAT name
    return_code, template_id = rscutils.publish_cat(ss_api, project_id, application_name, application_name, schedule_id)
    
    application = rscutils.get_application (ss_api, project_id, application_name)
    application_id = rscutils.get_application_value (ss_api, project_id, application_name, 'id', application)
        
    # see if we can find an existing execution with the right name
    logger.info("Checking for existing deployment")
    try:
        execution_id = rscutils.get_deployment_id(ss_api, project_id, execution_name)
    except rscutils.NoDeploymentError as e:
        execution_id = None

    if execution_id == None:
        # launch a new deployment

        # convert the CAT options into a list
        execution_options=[]
        array_count=1
        options_list = options.split('|')
        add_option = False
        for option_param in options_list:
            op_val = option_param.strip().strip("'")
            if array_count == 1:
                op_label="name"
                name_str = "options[][" + op_label + "]=" + op_val
            elif array_count == 2:
                op_label="type"
                label_str = "options[][" + op_label + "]=" + op_val
            else:
                # if user has explicitly said this value is to be ignored, then don't add
                # the option to the list
                if op_val != ignore_label:
                    add_option = True
                    op_label="value"
                    value_str = "options[][" + op_label + "]=" + op_val
                else:
                    logger.info("Ignoring parameter: " + name_str)

                # reset array counter at 0 so it will reset to 1 below which starts the 3-tuple
                # parsing over
                array_count=0

            array_count=array_count+1

            # if we have an option that we are setting, add it to the list of options, otherwise ignore it
            if add_option:
                logger.info(name_str)
                logger.info(label_str)
                logger.info(value_str)
                logger.info("")
                
                execution_options.append(name_str)
                execution_options.append(label_str)
                execution_options.append(value_str)
                add_option = False

        logger.info("Setting " + str(len(execution_options)/3) + " options")

        try:
            # By specifying the name of a parameter as InviSiblE, it is ignored
            # by the command execute method.  The value must be a list.
            logger.info("Launching the application")
            launch=ss_api.launch_application(
                catalog_id=project_id,
                id=application_id,
                name=execution_name,
                description=description,
                schedule_name=config.get ('default_schedule_name'),
                InviSiblE01=execution_options
            )

            if need_to_unpublish:
                # unpublish the CAT
                logger.info("Unpublishing the template")
                rscutils.unpublish_template_by_id(ss_api,project_id,template_id)

            return_code = 0
        except Exception as e:
            return_code = 1
            logger.error (str(e))

    else:
        logger.error("A deployment with name, " + execution_name + ", already exists")

    return return_code
                             
exit(main())
