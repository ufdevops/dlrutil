###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  Terminates deployments given in a CSV file.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
# -----------------------------------------------------------------------------
#  - This script also requires the 'ansicolors' package installed. pip install
#    it by doing `pip install ansicolors`.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
from argparse import ArgumentParser
# json imports
import json
# os imports
from os import path
# rsc imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
from rsc.cmd import RscCommand
# sys imports
from sys import exit
# colors imports
from colors import red, green, blue, yellow, white
# multiprocessing imports
from concurrent import futures

###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()
executor = futures.ThreadPoolExecutor(12)

###############################################################################
# Functions
###############################################################################

def parse_arguments():
    """
    Parses arguments required by this program.
    :return: Values of said arguments.
    """

    parser = ArgumentParser(description='Tags deployments in bulk.')

    parser.add_argument('-pi', '--project_id', type=str,
                        help='RightScale project ID to query against.',
                        action='store', dest='project_id',
                        required=True)

    parser.add_argument('-csv', '--csv', type=str,
                        help='Path to CSV.',
                        action='store', dest='csv_path',
                        required=True)

    args = parser.parse_args()

    return args.project_id, args.csv_path

def terminate_deployment(deployment_data):

    deployment_name = deployment_data["name"]
    deployment_id = deployment_data["id"]

    try:
        # Terminate the deployment.
        #print("Terminating deployment %s.." % deployment_name)
        #ss_api.terminate_execution(project_id=arg_rs_project_id, id=deployment_id)

        # Delete the execution.
        print("Deleting deployment %s.." % deployment_name)
        ss_api.delete_execution(project_id=arg_rs_project_id, id=deployment_id)
    except:
        print("Ignoring exception!")


###############################################################################
# Imports
###############################################################################

# Remove this line if required!
print("This script is intentionally aborting. Do not run this script unless " +
      "you know what you are doing!")
exit(0)

# Get the project id argument.
arg_rs_project_id, arg_csv_path = parse_arguments()

# Deployments to nuke.
deployments_to_nuke = []

# True if we should skip the current line.
skip_line = True

# Open the handle to the csv file.
with open(arg_csv_path, 'r') as csv_file:
    for line in csv_file:
        # Skip the first line.
        if skip_line:
            skip_line = False
            continue

        # Now split the string by comma.
        deployment_data = line.split(',')

        # Get the deployment execution href.
        deployment_href = deployment_data[0]
        deployment_name = deployment_data[1]

        # Get the last part of the deployment href.
        deployment_ss_id = deployment_href.split('/')[6]

        # Package up the data and submit it.
        threaded_data = {
            "id" : deployment_ss_id,
            "name" : deployment_name
        }

        executor.submit(terminate_deployment, threaded_data)

















