###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This RightScale utility
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# datetime imports
from datetime import datetime
# dateutil imports
from dateutil import parser
# json imports
import json
# os imports
import os
# pytz imports
import pytz
# rsc.date imports
from rsc.date import get_rs_date_as_date
# rsc.api imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15

###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()

###############################################################################
# Helper methods
###############################################################################

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()

    parser.add_argument('--project-id',
        help="ID of the RightScale project to operate on.",
        required=True)

    parser.add_argument('--cutoff-date',
        help="Deployments older than this date will be deleted. MM/DD/YY format.",
        required=True)

    args = parser.parse_args()

    return args.project_id, args.cutoff_date

###############################################################################
# Implementation
###############################################################################

# Process the command line arguments.
arg_rs_project_id, arg_cutoff_date = parse_arguments()

# Get the current time as a date object.
# current_date = datetime.now(pytz.UTC)

# Convert the cutoff date to a datetime object.
cutoff_date = parser.parse('%s 12:00 AM EST' % arg_cutoff_date)

# First off, we need to make sure we've queried for all of the deployments.
if not os.path.isfile('./executions.dat'):
    print('Querying for all RightScale deployments..')

    # Perform the query.
    deployments = ss_api.index_execution(
        project_id=arg_rs_project_id,
        view='expanded',
        filter=['status==terminated'])

    # Write the data to a file.
    with open('./executions.dat', 'w') as out_file:
        json.dump(deployments, out_file)
else:
    print('Using cached executions.dat file..')
    with open('./executions.dat', 'r') as in_file:
        deployments = json.load(in_file)

# List of terminated deployments to delete.
deployments_to_delete = []

# Now begin iterating through each deployment.
for deployment in deployments:
    # First step: Ensure there are NO SERVERS in the deployment!
    server_count = 0
    for resource in deployment['api_resources']:
        if resource['type'] == 'servers' and resource['value']['href']:
            server_count += 1

    if server_count > 0:
        print('Deployment name: %s' % deployment['name'])
        print(json.dumps(deployment, indent=4, sort_keys=True))
        print('  Warning: This deployment contains %d servers! Ignoring..' % server_count)
        continue

    # Get the date the deployment was terminated.
    termination_date = deployment['timestamps']['terminated_at']

    # Sometimes rightscale is weird and doesn't timestamp the terminated
    # deployment. Instead, look at the creation date.
    if not termination_date:
        termination_date = deployment['timestamps']['created_at']

    # Convert the termination date into a datetime object.
    termination_date = get_rs_date_as_date(termination_date)

    # If the cutoff date is larger than the termination date,
    # then the deployment termination date is older. This means
    # we are free to delete the deployment.
    if cutoff_date > termination_date:
        print('Name: %s, Termination date: %s' % (deployment['name'], termination_date))
        deployments_to_delete.append(deployment)

# When ready: run the delete function here..
print('We intend to delete %d deployments.' % len(deployments_to_delete))