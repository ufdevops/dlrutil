###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  A utility to deploy/launch a full deployment in RightScale
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################
# usage: rsc-deploy.py [-h] --project PROJECT --release RELEASE --config_root_dir CONFIG_ROOT_DIR
#                      --name NAME --application_name APPLICATION_NAME --options
#                      OPTIONS --description DESCRIPTION
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --test_name NAME      The name of the test to run
###############################################################################

import threading
import random
import time
import argparse
import json
import os
from collections import OrderedDict

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

ss_api = SelfService()

class deployThread (threading.Thread):
    def __init__(self, config, template_config, ss_api, name, deployment, schedule_id):
        threading.Thread.__init__(self)
        self.config = config
        self.template_config = template_config
        self.name = name
        self.deployment = deployment
        self.ss_api = ss_api
        self.schedule_id = schedule_id
        self.project_id = self.config.get('project_id')

    def run(self):
        #logger.debug ("Starting " + self.name + ": " + time.ctime(time.time()))
        template_name = self.deployment['application_name']

        #logger.info ("Looking for template: " + template_name)
        #template = get_template(self.config, self.template_config, self.ss_api, template_name)

        #logger.info ("Publishing template: " + template_name)
        #rscutils.publish_cat(self.ss_api, self.project_id, template_name, template_name, self.schedule_id)

        logger.info ("Looking for application: " + template_name)
        application = rscutils.get_application(self.ss_api, self.project_id, template_name)
        #print application
        
        # get the required parameters and translate them to input parameters specified in config
        if application is not None:
            required_parameters = application["required_parameters"]
            if required_parameters is not None:
                for rp in required_parameters:
                    rel_param = "release_" + rp.replace("param_", "")
                    try:
                        rel_value = self.config.get(rel_param)
                        logger.debug (rp + " -> " + rel_param + " = " + rel_value)
                    except Exception as e:
                        logger.exception (e)
                        logger.error ("Need to add (" + rel_param + ") to the release in projects.json")
            else:
                logger.debug ("No required paramters for " + template_name)
        else:
            logger.debug ("No published application CAT for " + template_name)
            
        #deploy (self.name, self.deployment)
        #logger.debug ("Finishing " + self.name + ": " + time.ctime(time.time()))

        return

def read_templates(config_filename):
    template_config = {}
    logger.info ("config_filename:" + config_filename)
    with open(config_filename) as config_file:
        try:
            template_config = json.load(config_file, object_pairs_hook=OrderedDict)
            
        except Exception as e:
            logger.error ("Error loading the dictionary from " + config_filename)
            raise 
        
    return template_config

def get_template(config, template_config, ss_api, template_name):
    project_id = config.get ('project_id')

    try:
        #template = template_config['templates'][template_name]
        raise
    except Exception as e:
        logger.warning ("Cannot find " + template_name)
        template = rscutils.get_template(ss_api,project_id,template_name)
        print template

    return template

def deploy(deployment_name, deployment):
    return_code = 1

    logger.info ("Running deployment: " + deployment_name)
    logger.info ("Launching: " + deployment["application_name"])
    time.sleep(random.randint(1,2))

    return return_code

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--test_name',
                        help='The name of the test to run.',
                        required=True)

    return parser.parse_args()

def check_env_vars(evars):
    missing = False
    for e in evars:
        if os.getenv(e) == None:
            logger.error ('You must set ' + e.str())
            missing = True
    if missing:
        logger.error ("Exiting because you are missing some environment variables.")
        exit (1)
    return

def main():
    check_env_vars (["PYTHONPATH"])

    return_code = 1

    args = parse_arguments()
    config_root_dir = args.config_root_dir

    config = ConfigJSON(config_root_dir, args.project, args.release)
    config.read ()

    project_id = config.get ('project_id')
    #template_config = read_templates('templates.json')
    template_config = None
    
    test_name = args.test_name
    logger.info ("Running test: " + test_name)
    test_cluster = config.get (test_name)

    schedule_name = config.get ('default_schedule_name')
    schedule_id = rscutils.get_schedule_id(ss_api,project_id,schedule_name)

    for sequence in test_cluster:
        threads = []

        logger.info ("Running sequence: " + sequence)
        for deployment_name in test_cluster[sequence]["value"]:
            thread = deployThread(config, template_config, ss_api, deployment_name,
                                  test_cluster[sequence]["value"][deployment_name], schedule_id)
            thread.start()
            threads.append(thread)

        # Wait for all threads to complete
        isThreadStillAlive = True
        while isThreadStillAlive:
            isThreadStillAlive = False
            for t in threads:
                t.join(1)
                if t.isAlive():
                    #logInfo (t.getName() + " is still alive")
                    isThreadStillAlive = True

    return return_code
                             
exit(main())
