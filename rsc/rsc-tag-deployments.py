###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This RightScale utility tags deployments listed in a file (one deployment
#  self-service link per line) with a list of tags in another file
#  (one tag per line).
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
from argparse import ArgumentParser
# json imports
from json import dumps
# os imports
from os import path
# rsc imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
# sys imports
from sys import exit

###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()

###############################################################################
# Functions
###############################################################################

def parse_arguments():
    """
    Parses arguments required by this program.
    :return: Values of said arguments.
    """

    parser = ArgumentParser(description='Tags deployments in bulk.')

    parser.add_argument('-pi', '--project_id', type=str,
                        help='RightScale project ID to query against.',
                        action='store', dest='project_id',
                        required=True)

    parser.add_argument('-d','--deployments-file', type=str,
                        help='Path to deployments file.', action='store',
                        dest='deployments_file',
                        required=True)

    parser.add_argument('-t','--tags-file', type=str,
                        help='Path to tags file.', action='store',
                        dest='tags_file',
                        required=True)

    args = parser.parse_args()

    return args.project_id, args.deployments_file, args.tags_file

def tag_deployment(deployment_name, tag_list):
    """
    Given a deployment name, this function will find the deployment in
    rightscale and apply the tags listed in tag_list to it.
    :param deployment_name: Name of the deployment to tag.
    :param tag_list: List of tags to apply to the deployment.
    :return: True on success. False on failure.
    """

    # Query rightscale for the deployment.
    pass


###############################################################################
# Implementation
###############################################################################

# Parse arguments to program.
arg_project_id, arg_deployments_file, arg_tags_file = parse_arguments()

# Ensure both files exist.
if not path.exists(arg_deployments_file):
    print('Path to deployments file (%s) is not valid.' % arg_deployments_file)
    exit(1)

if not path.exists(arg_tags_file):
    print('Path to tags file (%s) is not valid.' % arg_tags_file)
    exit(1)

# Read in the list of deployments given to us.
deployment_ss_links = []
with open(arg_deployments_file, 'r') as f:
    deployment_ss_links = f.read().splitlines()

# Read in the list of tags to apply.
tags_to_apply = []
with open(arg_tags_file, 'r') as f:
    tags_to_apply = f.read().splitlines()

# Print out some debug information.
print('There are %d deployments to tag with %d tags.' % (
    len(deployment_ss_links), len(tags_to_apply)))

# Progress counter.
progress = 0
total = len(deployment_ss_links)

error_file = open('bad_deployments.txt', 'w')

# Begin iterating through each deployment self-service link.
for ss_link in deployment_ss_links:
    # Read out the id portion of the self-service link and
    # generate an execution href.
    execution_id = ss_link.split('/')[5]

    try:
        # Ask rightscale for the json data for this execution.
        deployment = ss_api.show_execution(
            project_id=arg_project_id, id=execution_id)
    except:
        # Log failed deployment. url.
        print('Failed to tag deployment %s.' % ss_link)
        error_file.write(ss_link + '\n')

        # Increment progress.
        progress += 1

        # Move on..
        continue

    # Get the href of the deployment.
    deployment_href = deployment['deployment']

    # Debug message.
    print('Tagging deployment %s: (%d/%d)' % (
        deployment['name'], progress + 1, total))

    # Apply the tag.
    cm_api.multi_add_tag(
        resource_hrefs=[deployment_href],
        tags=tags_to_apply)

    # Increment progress.
    progress += 1

# Close the error file as we're done.
error_file.close()
