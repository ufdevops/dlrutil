#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Perform an action on a RightScale deployment
###############################################################################
# Prerequisites: Assumes the RSC command from RightScale is installed and
# in the system search path.
###############################################################################
# usage: rsc-deployment.py [-h] --project PROJECT --release RELEASE --config_root_dir
#                          CONFIG_ROOT_DIR --name NAME --input "csv of inputs" --outputs "csv of outputs"
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment on which to
#                         operate.
#
# Exit status
#    0 if action succeeded
#    1 if action did not succeed
#    2 if deployment did not exist
###############################################################################

import sys
import json
import argparse
import subprocess
from collections import OrderedDict

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment on which to operate.',
                        required=True)
    parser.add_argument('--output_fn_base',
                        help='The name of the file to put parameters into.',
                        required=False)
    parser.add_argument('--inputs',
                        help='A comma separated list of inputs to get',
                        required=False)
    parser.add_argument('--outputs',
                        help='A comma separated list of outputs to get',
                        required=False)
    parser.add_argument('--show_all', action='store_true',
                        help='Show all the output and input parameters for the deployment',
                        required=False)
    
    args = parser.parse_args()

    if not args.show_all and args.inputs == None and args.outputs == None:
        logger.error ("You must specify either --show_all or --inputs or --outputs")
        exit (1)

    if args.show_all and (args.inputs != None or args.outputs != None):
        logger.error ("If --show_all used, you cannot use --inputs or --outputs")
        exit (1)

    return args

def get_input_parameters(deployment):
    ips = OrderedDict()
    if deployment['configuration_options'] != None:
        for i in deployment['configuration_options']:
            ips[i['name']] = i['value']

    return ips

def get_output_parameters(deployment):
    ops = OrderedDict()
    if deployment['outputs'] != None:
        for o in deployment['outputs']:
            if o['value'] == None:
                value = 'null'
            elif o['value']['value'] == None:
                value = 'null'
            else:
                value = o['value']['value']
                ops[o['name']] = value
                
    return ops

def dump_parameters(input_parameters, output_parameters, output_fn_base=None):
    fh = None
    if output_fn_base is not None:
        shell_output_filename = output_fn_base + ".sh"
        fh = open(shell_output_filename, 'w')
    else:
        fh = sys.stdout

    all_parameters = {}
    for i in sorted(input_parameters):
        fh.write(i + "='" + input_parameters[i] + "'\n")
        all_parameters[i] = input_parameters[i]
        
    for o in sorted(output_parameters):
        fh.write(o + "='" + output_parameters[o] + "'\n")
        all_parameters[o] = output_parameters[o]

    if output_fn_base is not None:
        json_output_filename = output_fn_base + ".json"
        with open(json_output_filename, "w") as json_output_file:
            json.dump(all_parameters, json_output_file)
            json_output_file.close ()
            
    return 0

def get_requested_parameters(inputs,outputs,input_parameters,output_parameters):

    # helps us easily output the missing parameter, if any
    si = None
    so = None
    try:
        requested_input_parameters = OrderedDict()
        for i in inputs:
            si = i.strip(' ')
            requested_input_parameters[si] = input_parameters[si]
            
        requested_output_parameters = OrderedDict()
        for o in outputs:
            so = o.strip(' ')
            requested_output_parameters[so] = output_parameters[so]
    except Exception as e:
        if so == None:
            problem = si
        else:
            problem = so
        logger.error ("Cannot find the parameter you requested: " + problem)
        raise
                
    return requested_input_parameters,requested_output_parameters

def main():
    # assume it will fail
    return_code = 1

    args = parse_arguments()
    
    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')

    ss_api = SelfService()

    deployment = None
    try:
        deployment_id = rscutils.get_deployment_id(ss_api, project_id, args.name)
        deployment = rscutils.get_deployment(ss_api, project_id, deployment_id)

    except subprocess.CalledProcessError as s:
        logger.error (s.output)
        
    except rscutils.NoDeploymentError as e:
        logger.error ("There is no deployment named " + args.name)

    if deployment != None:
        input_parameters = get_input_parameters (deployment)
        output_parameters = get_output_parameters (deployment)

        show_input_parameters = None
        show_output_parameters = None

        if args.show_all:
            # return them all
            show_input_parameters = input_parameters
            show_output_parameters = output_parameters
        else:
            # only return the ones requested on the command line
            inputs = []
            outputs = []
            if args.inputs is not None:
                inputs = args.inputs.split(',')
            if args.outputs is not None:
                outputs = args.outputs.split(',')
                    
            try:
                show_input_parameters, show_output_parameters = get_requested_parameters (inputs,outputs,
                                                                                          input_parameters,
                                                                                          output_parameters)
            except Exception as e:
                # do nothing except return an error
                pass
                
        if show_input_parameters != None and show_output_parameters != None:
            dump_parameters (show_input_parameters, show_output_parameters, args.output_fn_base)

            # if we got here, then we are good
            return_code = 0
                
    return return_code

exit(main())
