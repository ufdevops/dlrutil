###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  A utility to publish a single CAT in RightScale
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################
# usage: rsc-deploy.py [-h] --project PROJECT --release RELEASE --config_root_dir CONFIG_ROOT_DIR
#                      --name NAME
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --name NAME           The name of the CAT to publish
###############################################################################

import argparse
import rscutils
import rscutils.logger as logger
from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

ss_api = SelfService()

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the CAT to publish',
                        required=True)

    return parser.parse_args()

def main():
    rscutils.check_env_vars (["PYTHONPATH"])

    return_code = 0

    args = parse_arguments()
    config_root_dir = args.config_root_dir

    config = ConfigJSON(config_root_dir, args.project, args.release)
    config.read ()
    project_id = config.get ('project_id')
    
    # right now these are the same, but we might want to publish with the
    # release information stripped from the name
    application_name = cat_name
    
    try:
        # get the default schedule
        schedule_id = rscutils.get_schedule_id(ss_api, project_id, config.get ('default_schedule_name'))
        rc, template_id = rscutils.publish_cat(ss_api, project_id, cat_name, application_name, schedule_id)
    except Exception as e:
        # mark the publish as failed but continue to get as many done as possible
        return_code = 1
        logger.error("Problem publishing " + cat_name)
        logger.exception(e)
        
    return return_code
                             
exit(main())
