#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Checks if a deployment exists
###############################################################################
# Prerequisites: Assumes the RSC command from RightScale is installed and
# in the system search path.
###############################################################################
# Inputs:
# usage: rsc-wait-state.py [-h] --name NAME --project PROJECT --release RELEASE --state STATE
#                          --timeout TIMEOUT --config_root_dir CONFIG_ROOT_DIR
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR      Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment to show
###############################################################################

import argparse
import time

import rscutils

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment to show',
                        required=True)

    return parser.parse_args()


def main():
    ss_api = SelfService()

    args = parse_arguments()

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')
    
    try:
        rscutils.get_deployment(ss_api, project_id, args.name)
        return_code = 0
    except rscutils.NoDeploymentError as e:
        return_code = 1

    return return_code

exit(main())
