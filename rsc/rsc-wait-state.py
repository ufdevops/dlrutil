#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Waits with timeout for a deployment to reach the supplied state
###############################################################################
# Prerequisites: Assumes the RSC command from RightScale is installed and
# in the system search path.
###############################################################################
# Inputs:
# usage: rsc-wait-state.py [-h] --name NAME --project PROJECT --release RELEASE --state STATE
#                          --timeout TIMEOUT --config_root_dir CONFIG_ROOT_DIR
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR      Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment to show
#   --state STATE         Deployment state to check for
#   --timeout TIMEOUT     Seconds to wait
###############################################################################

import argparse
import time

import rscutils

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment to show',
                        required=True)
    parser.add_argument('--state',
                        help='Deployment state to check for',
                        required=True)
    parser.add_argument('--timeout',
                        help='Seconds to wait',
                        required=True)

    return parser.parse_args()


def main():
    ss_api = SelfService()

    # assume it will fail
    return_code = 1

    args = parse_arguments()

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')
    
    try:
        return_code = rscutils.wait_deployment(ss_api, project_id, args.name, args.state, args.timeout)
    except rscutils.NoDeploymentError as e:
        # don't fail if the deployment does not exist
        print ("WARNING: There is no deployment named " + args.name)
        return_code = 0

    return return_code

exit(main())
