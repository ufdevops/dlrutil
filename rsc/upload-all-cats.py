#!/bin/python
###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  Uploads all CAT files to the specified RightScale project.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# collections imports
from collections import OrderedDict
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import wait
from concurrent.futures import ALL_COMPLETED
# multiprocessing imports
import multiprocessing
# operator imports
from operator import itemgetter
# os imports
import os
# sys imports
import sys
# subprocess imports
import subprocess
from subprocess import CalledProcessError
# re imports
import re
# rsc.api imports
from rsc.api.ss import SelfService
from rscutils import get_all_templates

###############################################################################
# Global variables
###############################################################################
# Dictionary containing an entry for each CAT file. Key is CAT file name.
# Value is list of dependencies the CAT file has.
gDependencyDict = {}

# List of CATs we have uploaded successfully.
gUploadedCats = []

# Look up table that maps a package name to a package file.
gPackageMap = {}

# Create a thread pool.
gThreadPool = ThreadPoolExecutor(multiprocessing.cpu_count())

# Regex to parse out import lines in a CAT.
gImportRegex = re.compile(r"import\s+[\'|\"](.+)[\'|\"],")

# Regex to parse out package lines in a CAT.
gPackageRegex = re.compile(r"package\s+[\'|\"](.+)[\'\"]")

# List of Future results
gFutureResults = []

# Current CATs in Designer
gCurrentCats = {}

# Global list of CATS that failed
gExceptions = []

# CAT upload order.
gCatUploadOrder = []

# Project ID.
gProjectID = None

# Whether or not to upload only common CATs.
gUploadOnlyCommon = False

# Self-service API singleton.
ss_api = SelfService()

###############################################################################
# Helper methods
###############################################################################

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--cat-dir',
                        help='Path to preprocessed CAT file directory',
                        required=True)
    parser.add_argument('--only-common',
                        help='If set, will only upload CAT files underneath the common folder.',
                        required=False)
    args = parser.parse_args()

    return args


def get_cat_package_name(cat_file_path):
    """
    Given a path to a CAT file, this function will return the
    name of the package the CAT file should be associated with.
    :param cat_file_path: Path to CAT file.
    :return: A string containing the name of the package defined for
    the given CAT file. None if there is no package.
    """

    cat_package_name = None

    with open(cat_file_path, 'r') as cat_file_stream:
        cat_file_lines = list(cat_file_stream)
        for line in cat_file_lines:
            match = gPackageRegex.match(line)
            if match:
                cat_package_name = match.group(1)

    return cat_package_name


def get_cat_dependencies(cat_file_path):
    """
    Looks through a CAT file to see what it imports.
    :param cat_file_path: Path to CAT file.
    :return: Array of CAT names that are dependencies of the given CAT.
    """

    deps = []
    package_name = None

    # Get the name of the CAT file.
    cat_file_name = os.path.basename(cat_file_path)

    # Open up the path to the cat file. Iterate over every line and try to
    # find an import. Parse out the name of the imported CAT file. Then add
    # it to a list of dependencies which we will return.
    with open(cat_file_path, 'r') as cat_file_stream:
        cat_file_lines = list(cat_file_stream)
        for line in cat_file_lines:
            # Check to see if this is an 'import' line.
            match = gImportRegex.match(line)
            if match:
                # Look up the file name via the package name.
                package_name = match.group(1)
                package_cat_path = gPackageMap[package_name]

                # Add to list of dependencies.
                deps.append(package_cat_path)

                # Continue
                continue

    return deps


def debug_message(msg, indent_level):
    """
    Prints out a string with a specified indent level.
    :param msg: This is the message to display.
    :param indent_level: Number of spaces to precede the message.
    :return: A debug message with the proper indentation level.
    """
    spaces_string = " " * indent_level
    return "%s %s" % (spaces_string, msg)


def upload_cat(cat_file_path, indent_level = 0):
    """
    Uploads a given CAT and its dependencies (if not already uploaded).
    :param cat_file_path: Absolute or relative path to the CAT file to upload.
    :param indent_level: Number of spaces to use when printing debug output.
    :return: None
    """

    global gExceptions
    global gUploadedCats

    print(debug_message("upload_cat(%s)" % cat_file_path, indent_level))

    # Skip already uploaded CAT files.
    if cat_file_path in gUploadedCats:
        print(debug_message("cat %s is already in gUploadedCATs." % cat_file_path, indent_level))
        return

    # Get the set of dependencies associated with this CAT file.
    dependencies = set(gDependencyDict[cat_file_path])

    # Were the dependencies of this CAT file already uploaded?
    # If so, we can submit them immediately for upload.
    if dependencies.issubset(gUploadedCats):
        print(debug_message("cat %s dependencies are already uploaded. threading cat upload.." % cat_file_path, indent_level))
        thread_upload_cat(cat_file_path)
        gUploadedCats.append(cat_file_path)
        return

    # Using set logic, subtract all uploaded cats from the set
    # of current dependencies.
    remaining_deps = dependencies - set(gUploadedCats)

    # If we are here, it means we still have dependencies that we
    # need to upload. Deal with all of those first.
    for dependency_path in remaining_deps:
        upload_cat(dependency_path, indent_level + 2)

    # Finally: Upload the current CAT file.
    try:
        print(debug_message("uploading cat %s (dependencies finished)." % cat_file_path, indent_level))
        thread_upload_cat(cat_file_path)
        gUploadedCats.append(cat_file_path)
    except Exception as e:
        e.cat_name = os.path.basename(cat_file_path)
        gExceptions.append(e)


def upload_cat_old(cat_file_path, indent_level = 0):
    """
    DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED
    Recursively uploads a CAT file and its dependencies.
    :param cat_file_path: Absolute or relative path to the CAT file to upload.
    :param indent_level: Number of spaces to use when printing debug output.
    :return: None.
    DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED
    """
    global gFutureResults
    global gExceptions
    global gUploadedCats

    print(debug_message("upload_cat(%s)" % cat_file_path, indent_level))

    # Skip already uploaded CAT files.
    if cat_file_path in gUploadedCats:
        print(debug_message("cat %s is already in gUploadedCATs." % cat_file_path, indent_level))
        return

    # Get the set of dependencies associated with this CAT file.
    dependencies = set(gDependencyDict[cat_file_path])
    # all_dependencies_str = debug_message("cat %s has the following dependencies:\n" % cat_file_path, indent_level)
    # for dependency in dependencies:
    #     all_dependencies_str = all_dependencies_str + debug_message("\n%s" % dependency, indent_level + 2)
    # print(all_dependencies_str)

    # Were the dependencies of this CAT file already uploaded?
    # If so, we can submit them immediately for upload.
    if dependencies.issubset(gUploadedCats):
        print(debug_message("cat %s dependencies are already uploaded. threading cat upload.." % cat_file_path, indent_level))
        future_result = gThreadPool.submit(thread_upload_cat, cat_file_path)
        future_result.cat_name = os.path.basename(cat_file_path)
        gFutureResults.append(future_result)
        gUploadedCats.append(cat_file_path)
        return

    # Using set logic, subtract all uploaded cats from the set
    # of current dependencies.
    remaining_deps = dependencies - set(gUploadedCats)
    #dependencies_left_str = debug_message("cat %s has the following dependencies left to upload:" % cat_file_path, indent_level)
    #for dependency in remaining_deps:
    #    dependencies_left_str = dependencies_left_str + debug_message("\n%s" % dependency, indent_level + 2)

    # If we are here, it means we still have dependencies that we
    # need to upload. Deal with all of those first.
    for dependency_path in remaining_deps:
        upload_cat(dependency_path, indent_level + 2)

    # Wait for all workloads to complete.
    wait(gFutureResults, timeout=None, return_when=ALL_COMPLETED)

    # Process any exceptions that occurred.
    for future_result in gFutureResults:
        the_exception = future_result.exception()
        if the_exception:
            the_exception.cat_name = future_result.cat_name
            gExceptions.append(the_exception)

    # Clear out the future results now.
    gFutureResults = []

    # Finally: Upload the current CAT file.
    try:
        print(debug_message("uploading cat %s (dependencies finished)." % cat_file_path, indent_level))
        thread_upload_cat(cat_file_path)
        gUploadedCats.append(cat_file_path)
    except Exception as e:
        e.cat_name = os.path.basename(cat_file_path)
        gExceptions.append(e)


def pass_1():
    """
    This function uploads all CATs with zero dependencies.
    """
    global gFutureResults
    global gThreadPool
    global gUploadedCats

    print("Pass 1: Upload all CAT files that have no dependencies.")
    for cat_path in gCatUploadOrder:
        if len(gDependencyDict[cat_path]) == 0:
            gUploadedCats.append(cat_path)
            future_result = gThreadPool.submit(thread_upload_cat, cat_path)
            future_result.cat_name = os.path.basename(cat_path)
            gFutureResults.append(future_result)


def pass_2():
    """
    This function uploads all remaining CATs that haven't been uploaded.
    :return:
    """
    global gExceptions

    print("Pass 2: Upload CATs that have dependencies.")
    for cat_path in gCatUploadOrder:
        if cat_path not in gUploadedCats:
            try:
                upload_cat(cat_path)
            except Exception as e:
                e.cat_name = cat_path
                gExceptions.append(e)

###############################################################################
# Threaded functions
###############################################################################
def thread_upload_cat(cat_file_path):
    """
    Actually performs the upload of the CAT.
    :param cat_file_path: Absolute or relative path to the CAT file to upload.
    :return:
    """
    cat_name = os.path.basename(cat_file_path)
    if cat_name in gCurrentCats:
        print("Updating existing CAT file %s..." % cat_name)
        ss_api.update_template(collection_id = gProjectID, id = gCurrentCats[cat_name], source = cat_file_path)
    else:
        print("Uploading new CAT file %s..." % cat_name)
        ss_api.create_template(collection_id = gProjectID, source = cat_file_path)


###############################################################################
# Implementation
###############################################################################

def main():
    global gThreadPool
    global gDependencyDict
    global gPackageMap
    global gFutureResults
    global gUploadedCats
    global gExceptions
    global gCatUploadOrder
    global gProjectID
    global gUploadOnlyCommon

    exit_code = 0

    # Read arguments out from command line.
    args = parse_arguments()

    # Figure out if we should only upload common cats.
    gUploadOnlyCommon = (args.only_common == 'true')

    # Update the python path with path to global config utilities.
    sys.path.append(os.path.join(args.config_root_dir, 'scripts'))

    # Perform delayed utilities imports.
    from util.config.ConfigJSON import ConfigJSON

    # Read out global configuration data.
    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    gProjectID = config.get('project_id')

    # Construct path to root preprocessed CAT dir.
    preprocessed_cat_dir = os.path.join(args.cat_dir, 'preprocessed')

    # If we're only supposed to upload common CATs, append common.
    if gUploadOnlyCommon:
        preprocessed_cat_dir = os.path.join(preprocessed_cat_dir, 'common')

    cat_data = get_all_templates(ss_api, gProjectID)
    for cat in cat_data:
        gCurrentCats[cat["filename"]] = cat["id"]

    # Now begin walking through the cat directory. Generate a list of CAT
    # files.
    cat_file_paths = []
    for root, dirs, files in os.walk(preprocessed_cat_dir):
        for name in files:
            # Add the CAT file's path to our list.
            cat_path = os.path.join(root, name)
            cat_file_paths.append(cat_path)

            # Create the association between the package name and the
            # CAT file path (if there is a package defined for the CAT).
            cat_package_name = get_cat_package_name(cat_path)
            if cat_package_name:
                gPackageMap[cat_package_name] = cat_path

    # Now loop through each CAT file pat we just created. Build up a list
    # of imports for each CAT file. Then insert this data into the dictionary.
    for cat_path in cat_file_paths:
        # Get name of CAT. This will be used as the dictionary key.
        cat_file_name = os.path.basename(cat_path)
        print("Parsing dependencies for CAT file %s:" % cat_file_name)

        # Get dependencies for CAT. This will be used as the value in the
        # dictionary.
        dependencies = get_cat_dependencies(cat_path)
        print("\t%d dependencies found." % len(dependencies))

        # Create the association.
        gDependencyDict[cat_path] = dependencies

    # Now, using the length of each CAT's dependency list, sort from smallest
    # to greatest.
    gCatUploadOrder = sorted(gDependencyDict.keys(), key=lambda k: len(gDependencyDict[k]))

    # Run the first pass of the upload. This uploads all of the zero-dependency CATs first.
    # This fills in the future results array with queued CAT uploads.
    pass_1()

    # Wait for the CAT uploads to complete.
    wait(gFutureResults, timeout=None, return_when=ALL_COMPLETED)

    # Process the exceptions (if any).
    for future_result in gFutureResults:
        the_exception = future_result.exception()
        if the_exception:
            the_exception.cat_name = future_result.cat_name
            gExceptions.append(the_exception)

    # Clear out the future results array. Everything should be finished by now.
    gFutureResults = []

    # Pass 2: Deal with the rest of the CAT files we haven't uploaded.
    # This fills up the future_results array with queued jobs.
    pass_2()

    # Wait for the CAT uploads to complete.
    for future_result in gFutureResults:
        the_exception = future_result.exception()
        if the_exception:
            the_exception.cat_name = future_result.cat_name
            gExceptions.append(the_exception)

    # Clean up the thread pool.
    gThreadPool.shutdown(wait=True)

    # Iterate through each exception.
    for exception in gExceptions:
        if isinstance(exception, CalledProcessError):
            print("Failed to upload CAT file %s: Reason: %s" % (exception.cat_name, exception))
        else:
            print("An exception occurred while uploading a CAT file: %s" % exception)

    if len(gExceptions) > 0:
        exit_code = 1

    # Now iterate through each exception and print it.
    return exit_code

sys.exit(main())

