#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Sets the termination date for a deployment
###############################################################################
# Prerequisites: Assumes the RSC command from RightScale is installed and
# in the system search path.
###############################################################################
# usage: rsc-set-terminate-date.py [-h] --project PROJECT --release RELEASE --config_root_dir
#                                  CONFIG_ROOT_DIR --name NAME --terminate_date
#                                  TERMINATE_DATE --terminate_time
#                                  TERMINATE_TIME
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment to show
#   --terminate_date TERMINATE_DATE
#                         No terminate date was specified
#   --terminate_time TERMINATE_TIME
#                         No terminate time was specified
###############################################################################

import argparse
import time

import rscutils

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

def parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('--project',
                        help='The RightScale project containing the deployment',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment to show',
                        required=True)
    parser.add_argument('--terminate_date',
                        help='No terminate date was specified',
                        required=True)
    parser.add_argument('--terminate_time',
                        help='No terminate time was specified',
                        required=True)

    return parser.parse_args()


def main():
    # assume it will fail
    return_code = 1

    args = parse_arguments()

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')
    
    ss_api = SelfService()

    try:
        rscutils.set_terminate_date(ss_api, project_id, args.name, args.terminate_date, args.terminate_time)
        return_code = 0
    except rscutils.NoDeploymentError as e:
        # don't fail if the deployment does not exist
        print ("WARNING: There is no deployment named " + args.name)
        return_code = 0

    return return_code

exit(main())
