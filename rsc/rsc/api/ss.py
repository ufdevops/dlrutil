################################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This module wraps the 'Self Service' API of the rsc tool.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
from rsc.api import RscApiContextBase

###############################################################################
# Classes
###############################################################################
class SelfService(RscApiContextBase):
    """
    This class wraps around the Self-Service API for RightScale.
    """

    ############################################################################
    # Static variables
    ############################################################################

    # The name of the api on the command line.
    API_NAME = 'ss'

    ############################################################################
    # Methods
    ############################################################################

    def __init__(self):
        """
        Constructor.
        """

        # Initialize the API context.
        super(SelfService, self).__init__(SelfService.API_NAME)

