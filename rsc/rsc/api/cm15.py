################################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This module wraps the 'Cloud Management' version 1.5 API of the RSC tool.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
from rsc.api import RscApiContextBase

###############################################################################
# Classes
###############################################################################
class CM15(RscApiContextBase):
    """
    This class wraps around the Cloud Management API for RightScale.
    """

    ############################################################################
    # Static variables
    ############################################################################

    # The name of the api on the command line.
    API_NAME = 'cm15'

    ############################################################################
    # Methods
    ############################################################################

    def __init__(self):
        """
        Constructor.
        """

        # Initialize the API context.
        super(CM15, self).__init__(CM15.API_NAME)

