################################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This module wraps the 'Cloud Management' version 1.5 API of the RSC tool.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# json imports
from json import loads
# rsc.cmd imports
from rsc.cmd import RscCommand
# types imports
from types import MethodType

###############################################################################
# Exceptions
###############################################################################
class RscApiCallParamError(Exception):
    """Exception raised when an RSC API call is made with missing parameters."""
    pass

###############################################################################
# Classes
###############################################################################
class RscApiContextBase(object):
    """
    This is the base class for a rightscale api context.
    """

    def __init__(self, api_name):
        """
        Initializes the api context.
        :param api_name: Name of the API to generate a context for.
        Example: 'ss' or 'cm15'.
        """

        # Store off the api name.
        self.api_name = api_name

        # Parse out the available actions and return back a dictionary
        # containing the schemas for each action that is possible with
        # the api_name.
        self.context_dict = self._initialize_context()

        # Now go through and initialize all the api methods.
        self._initialize_api()

    def _initialize_context(self):
        """
        Creates a dictionary that sets up the context for the API calls.
        :return: Dictionary containing action, href, and resource.
        """

        # Construct a command to list out all the actions available for this
        # API.
        actions_list_cmd = RscCommand(api_name=self.api_name, args=['actions'])

        # Run the command and get the results. Since we get a bytes object back,
        # decode it into a utf-8 string and split on newline.
        actions_output = actions_list_cmd.execute().decode('utf-8').split('\n')

        # Iterate through every line. Parse out the results into a context
        # dictionary.
        context_dict = {}
        action_name = ''
        for line in actions_output:
            # Ignore lines with ===== or -----
            if '=' in line or '-' in line:
                continue

            # Split on space so we get the action, href and resource.
            action_data = line.split()

            # Ignore empty lines.
            if len(action_data) == 0:
                continue

            # If we get three results, it means we have a new context.
            data_index = 0
            if len(action_data) == 3:
                action_name = action_data[data_index].lower()
                data_index += 1

            # Read out the HREF and resource.
            resource_href = action_data[data_index].lower()
            action_resource = action_data[data_index + 1].lower()

            # Add an entry into the context dictionary.
            if action_name not in context_dict:
                context_dict[action_name] = {}

            if action_resource not in context_dict[action_name]:
                context_dict[action_name][action_resource] = {}

            context_dict[action_name][action_resource]['href'] = resource_href

            # Split the href by slash and build up a list of variable arguments.
            var_arg_list = []
            for substr in resource_href.split('/'):
                if ':' in substr:
                    # Add it to the list.
                    var_arg_list.append(substr)

            # Store the variable arguments array off into the dictionary entry
            # for this resource.
            context_dict[action_name][action_resource]['var_args'] = var_arg_list

        return context_dict

    def _initialize_api(self):
        """
        Initializes the mappings required to use this API.
        :return: None.
        """

        for context_name in self.context_dict:
            for context_resource in self.context_dict[context_name]:

                # Get the metadata associated with this method.
                context_data = self.context_dict[context_name][context_resource]

                # Generate a method object.
                method_object = self._create_api_method(context_name, context_resource, context_data)

                # Add the method object to the class.
                setattr(self, method_object.__name__, MethodType(method_object, self))


    def _create_api_method(self, action, resource, context_data):
        """
        Creates an API method object and returns it.
        :param action: Name of the action to take.
        :param resource: Type of resource we're dealing with.
        :param context_data: Metadata about the action on the resource.
        :return: Method object that can be called.
        """

        # Create the body of the temporary method.
        def _temp_method(self, **kwargs):

            # Get the keys for kwargs.
            arg_names = list(kwargs.keys())

            # Make a copy of the href string.
            href_copy = context_data['href']

            # Replace all of the required arguments in the href string
            # with their respective values from the kwargs dictionary.
            for arg_name in context_data['var_args']:
                # Note: We do 1: to skip the ':' at the beginning of the argument
                # name (example: ':id' => 'id')
                arg_key_name = arg_name[1:]

                # Raise an error if it's not there.
                # Note: We do 1: to skip the ':' at the beginning of the argument
                # name (example: ':id' => 'id')
                if arg_key_name not in arg_names:

                    error_str = 'Error: %s: Missing required parameter: %s' % (
                        _temp_method.__name__, arg_key_name)

                    raise RscApiCallParamError(error_str)

                # Remove these href string values from consideration.
                # We are going to turn the remaining names into command-line
                # parameters. See note above about 1:
                else:
                    arg_names.remove(arg_key_name)

                # Grab the value from the dictionary.
                arg_value = kwargs[arg_key_name]

                # Replace the result in the href.
                href_copy = href_copy.replace(arg_name, arg_value)

            # We now have an href string with the necessary variables replaced.
            # We also have an arg_names array that contains non-href arguments.
            # Iterate through these and add them to a list. Make sure to add the
            # action and href to the beginning.
            cmdline_arg_list = [action, href_copy]
            for remaining_arg_name in arg_names:
                # Skip the 'retries' argument and 'eat' the result:
                if remaining_arg_name == 'retries':
                    continue

                # Deal with array-type variables.
                if type(kwargs[remaining_arg_name]) is list:
                    # Set the argument format string.
                    arg_str_format = '%s[]=%s'

                    # Iterate through each element in the list and add it to the
                    # command line.
                    for element in kwargs[remaining_arg_name]:
                        cmdline_arg_list.append(arg_str_format % (remaining_arg_name, element))

                # Deal with regular variables.
                else:
                    # Set the argument format string.
                    arg_str_format = '%s=%s'

                    # Add the command-line parameter for the action to the list.
                    cmdline_arg_list.append(arg_str_format % (remaining_arg_name, kwargs[remaining_arg_name]))

            # Now we can build out the command to run.
            rsc_command = RscCommand(self.api_name, cmdline_arg_list)

            # Compute the number if retries to perform (if we have one set).
            retries = 2
            if 'retries' in kwargs:
                retries = int(kwargs['retries'])

            # Execute the command and get the output.
            raw_output = rsc_command.execute(retries=retries)

            # Attempt to load the output as json. If this fails,
            # then return the raw output.
            try:
                result = loads(raw_output)
            except Exception:
                result = raw_output

            return result

        # Fix up the method name.
        _temp_method.__name__ = str('%s_%s' % (action, resource))

        # Done.
        return _temp_method
