###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This module provides some convenient methods for dealing with date strings
#  returned back from rightscale JSON calls.
###############################################################################

###############################################################################
# Imports
###############################################################################
# dateutil imports
from dateutil import parser

###############################################################################
# Global methods
###############################################################################

def get_rs_date_as_date(the_date):
    """
    Converts an RS date to a Python datetime object.
    :param the_date: Date string from RS JSON data.
    :return: Python datetime object representing the given date.
    """

    # Convert to epoch time.
    #epoch_time = get_rs_date_epoch(the_date)

    # Convert from epoch to datetime.
    #return datetime.utcfromtimestamp(epoch_time)

    return parser.parse(the_date)