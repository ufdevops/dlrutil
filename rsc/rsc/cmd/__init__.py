###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This module wraps around the rsc executable command on the command line.
###############################################################################

###############################################################################
# Imports
###############################################################################
import time
import re

from json import loads

from subprocess import check_output
from subprocess import list2cmdline
from subprocess import STDOUT
from subprocess import CalledProcessError

import rscutils.logger as logger

###############################################################################
# Implementation
###############################################################################
class RscCommand(object):
    """
    This class will execute rightscale rsc commands and return back
    either the output or throw an exception.
    Command arguments are passed to 'rsc' as-is; they are not interpreted by the shell.
    """

    def __init__(self, api_name, args=[]):
        """
        Initializes the rsc command.
        :param api_name:  The name of the API you are trying to use.
                          Example: cm15
        :param args:      Arguments you want to pass in on the command line.
                          By specifying the name of a parameter as InviSiblE, it is ignored
                          by the command execute method.  The value must be a list.
                          If you have more than one, use InviSiblE01, InviSiblE02, etc.
                          The names must be unique and begin with InviSiblE

                          Example:
                          launch=ss_api.launch_application(
                              catalog_id=project_id,
                              id=application_id,
                              name=execution_name,
                              description=description,
                              schedule_name=config.get ('default_schedule_name'),
                              InviSiblE=execution_options
                          )
        """

        self.cmd_and_args = ['rsc', api_name]

        # Process invisible arguments:
        # When InviSible specified, strip it out
        for a in args:
            keys = re.findall("InviSiblE.*\[\]=(.*)", a)
            if len(keys) > 0:
                self.cmd_and_args.append(keys[0])
            else:
                self.cmd_and_args.append(a)

        # Store off other variables.
        self.api_name = api_name

    def execute(self, retries=2):
        """
        Executes the given command.
        :param retries: The number of times to rerun the command in case
        of failure.
        :return Textual output of the command.
        """

        result = None

        # Repeat for the number of retries.
        for i in range(0, retries+1):
            try:
                # Run the command.
                logger.debug('Execute command: ')
                logger.debug (self.cmd_and_args)
                result = check_output(self.cmd_and_args, shell=False, stderr=STDOUT)
                logger.debug(result)
                break
            except CalledProcessError as e:
                output = e.output.decode('utf-8')

                # Print out errors.
                logger.error(output + '\nCommand: ' + list2cmdline(e.cmd))

                # determine if a bad request made and don't bother to retry if so
                if output.find("400 Bad Request") != -1:
                    raise
                # determine if unprocessable and don't bother to retry if so
                if output.find("422 Unprocessable Entity") != -1:
                    raise
                # determine if there is a conflict with a CAT upload
                if output.find("409 Conflict") != -1:
                    raise

                # Try again if we're below the number of retries.
                if i < retries:
                    logger.info('Retrying: (%d/%d)' % (i + 1, retries))
                    time.sleep (2)
                    continue

                # Otherwise: raise the exception up.
                else:
                    raise e
                
        return result

