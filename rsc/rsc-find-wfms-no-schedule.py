#!/bin/python
###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  Finds all WFM deployments with no schedule defined. Dumps their information
#  to a spreadsheet.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import wait
from concurrent.futures import ALL_COMPLETED
# json imports
import json
# multiprocessing imports
import multiprocessing
# os imports
import os
# sys imports
import sys
# subprocess imports
import subprocess
from subprocess import CalledProcessError
# re imports
import re
# rsc imports
from rsc.api.ss import SelfService
# rscutils imports
import rscutils

###############################################################################
# Globals
###############################################################################

# API singletons.
ss_api = SelfService()

# List of executions.
#gExecutions = None

###############################################################################
# Helper methods
###############################################################################

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--out-csv',
                        help='Path to output csv data to.',
                        required=True)
    args = parser.parse_args()

    return args


def load_executions(proj_id):
    """
    Loads in executions from RightScale. Saves the result to a cache
    file. Loads that file in next time if it exists.
    :param proj_id: String containing project id to query against.
    :return: Array containing all known executions in RightScale.
    """

    executions = []

    print('Loading in executions...')

    # Check if cache file exists.
    if os.path.isfile('executions.cache'):
        with open('executions.cache', 'r') as executions_file_stream:
            print('Reading executions from cache file..')
            executions = json.load(executions_file_stream)
            print('Success!')

    # Query manually via SS. Then write to file.
    else:
        print('Querying RightScale for all executions.')
        executions = ss_api.index_execution(project_id=proj_id, view="expanded")
        with open('executions.cache', 'w') as executions_file_stream:
            json.dump(executions, executions_file_stream)
        print('Success!')

    return executions


def process_execution(execution):
    """
    Given an execution, will check to see if it is a WFM Team or User.
    Then will check to see if it has no next action.
    :param execution:
    :return: True if the execution has no scheduled action. False if it does.
    """

    result = False

    # Figure out what CAT file launched this guy.
    launched_from = execution["launched_from_summary"]
    cat_name = launched_from["value"]["name"]

    # Is the CAT file one of the CATs we care about?
    if cat_name in ["WFM Services - Team", "WFM Services - User"]:

        # Get the scheduling data for this execution.
        schedules = execution["schedules"]

        # If there are no schedules listed, then this is a WFM we need to
        # nuke.
        if len(schedules) == 0:
            result = True

    return result


def process_executions(thread_pool, executions):
    """
    Queues up process_execution calls for each execution. Returns the
    result.
    :param thread_pool: The thread pool to submit process_execution jobs to.
    :param executions: List of all executions in RightScale.
    :return: List of FutureResult objects.
    """

    future_results = []

    print("Queuing up executions for processing.")

    for execution in executions:
        # Submit the job to the thread pool.
        future_result = thread_pool.submit(process_execution, execution)

        # Store off the execution data for later access.
        future_result.execution_data = execution

        # Add to our future results array.
        future_results.append(future_result)

        # TEMP TEMP
        #break

    print("Queued up %d executions for processing." % len(future_results))

    return future_results

###############################################################################
# Implementation
###############################################################################
def main():
    """
    Main program implementation.
    :return: Exit status.
    """
    exit_status = 0

    # Read arguments out from command line.
    args = parse_arguments()

    # Update the python path with path to global config utilities.
    sys.path.append(os.path.join(args.config_root_dir, 'scripts'))

    # Perform delayed utilities imports.
    from util.config.ConfigJSON import ConfigJSON

    # Read out global configuration data.
    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()

    # Get project identifier.
    project_id = config.get('project_id')

    # Read in the executions.
    executions = load_executions(project_id)

    # Construct a thread pool.
    thread_pool = ThreadPoolExecutor(multiprocessing.cpu_count())

    # Thread out all of the execution processing.
    future_results = process_executions(thread_pool, executions)

    # Now wait until all executions are processed.
    wait(future_results, timeout=None, return_when=ALL_COMPLETED)

    # Open up the output file.
    with open(args.out_csv, 'w') as out_csv_stream:
        # Write out the header.
        out_csv_stream.write('Execution name,Execution HREF,Execution owner,Date launched,State\n')

        # Iterate through and check each result.
        for future_result in future_results:
            # Handle any and all exceptions that occur.
            execution_name = future_result.execution_data["name"]
            if future_result.exception():
                print("An exception happened with %s: %s" % (execution_name, future_result.exception()))
                continue

            # Only write the deployment to the CSV file if we got the correct result.
            if future_result.result():
                # Read the required data for the csv line out.
                execution = future_result.execution_data
                execution_name = execution["name"]
                execution_href = execution["href"]
                execution_owner = execution["created_by"]["email"].lower()
                execution_launch_data = execution["timestamps"]["launched_at"]
                execution_state = execution["status"]

                # Write the data out.
                out_csv_stream.write('%s,%s,%s,%s,%s\n' % (
                    execution_name,
                    execution_href,
                    execution_owner,
                    execution_launch_data,
                    execution_state
                ))


    return exit_status

# Run the program.
if __name__ == '__main__':
    sys.exit(main())