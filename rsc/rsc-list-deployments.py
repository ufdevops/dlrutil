###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  List deployments
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################
# usage: rsc-list-deployments.py [-h] --project PROJECT --release RELEASE --config_root_dir
#                                CONFIG_ROOT_DIR [--cat_filter CAT_FILTER]
#                                [--name_filter NAME_FILTER]
#                                [--deployment_status DEPLOYMENT_STATUS]
#                                [--created_by CREATED_BY]
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project to operate on.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of the config utilities
# 
# Use case #1
#   --deployment_status DEPLOYMENT_STATUS
#                         State of deployments to list.
#   --cat_filter CAT_FILTER
#                         String to filter CATs
#   --name_filter NAME_FILTER
#                         String to filter executions
# Use case #2
#   --created_by CREATED_BY
#                         List deployments by this account.
###############################################################################

import argparse
import json
import os

import rscutils
import rscutils.logger as logger
from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15

ss_api = SelfService()
cm_api = CM15()

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help="The RightScale project to operate on.",
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help="Root dir of the config utilities",
                        required=True)
    parser.add_argument('--cat_filter',
                        help="String to filter CATs",
                        required=False)
    parser.add_argument('--name_filter',
                        help="String to filter executions",
                        required=False)
    parser.add_argument('--deployment_status',
                        help="State of deployments to list.",
                        required=False)
    parser.add_argument('--created_by',
                        help="List deployments by this account.",
                        required=False)
    
    return parser.parse_args()

def byCount(t):
    # TODO get this to sort by value and numerically
    return t[1]

def local_get_deployments(data_filename, ss_api, config, loc_filter=""):
    # the only thing you can filter by is status for now

    if not os.path.isfile(data_filename):
        # Perform the query.
        if len(loc_filter) > 0:
            deployments = ss_api.index_execution(
                project_id=config.get ('project_id'),
                view='expanded',
                filter=[loc_filter]
            )
        else:
            deployments = ss_api.index_execution(
                project_id=config.get ('project_id'),
                view='expanded'
            )

        # Write the data to a file.
        with open(data_filename, 'w') as out_file:
            json.dump(deployments, out_file)
    else:
        print('Using cached ' + data_filename + ' file.')
        with open(data_filename, 'r') as in_file:
            deployments = json.load(in_file)
            
    return deployments

def main():
    args = parse_arguments()
    config_root_dir = args.config_root_dir
    deployment_status = args.deployment_status
    created_by = args.created_by
    name_filter = args.name_filter
    cat_filter = args.cat_filter
    
    config = ConfigJSON(config_root_dir, args.project, args.release)
    config.read ()
    project_id = config.get('project_id')

    if deployment_status != None:
        print('deployment_status: ' + deployment_status)
        statuses = deployment_status.strip().split(',')
        
        created = {}
        
        # get all deployable CATs, if filter set then filter
        cat_names = []
        cat_names_full = rscutils.get_all_cats(ss_api, project_id, True)
        if len(cat_filter) > 0:
            for c in cat_names_full:
                if c.lower().find(cat_filter.lower()) > -1:
                    cat_names.append(c)
        else:
            cat_names = cat_names_full
                       
        total_count = 0
        for status in sorted(statuses):
            data_filename='./'+status+'_executions.dat'
           
            print('Querying for all ' + status + ' RightScale deployments.')
            deployments=local_get_deployments(data_filename, ss_api, config, 'status=='+status)
               
            status_count = 0
            print('Status: %s' % status)
            for cat_name in sorted(cat_names):
                print('\t%s: %s' % (status, cat_name))
                cat_count = 0
                for deployment in sorted(deployments):
                    deployment_name = deployment['name']

                    # if you have a filter set and there is no match, skip this deployment
                    if len(name_filter) > 0 and deployment_name.lower().find(name_filter.lower()) == -1:
                        continue

                    # If we found the exclusion tag, skip this deployment.
                    exclusion_tag = "kronos:do_not_delete=true"
                    if rscutils.has_tag(cm_api,deployment,exclusion_tag):
                        continue

                    current_cat = deployment['launched_from_summary']['value']['name']
                    current_status = deployment['status']
                    if (current_cat == cat_name and current_status == status):
                        created_by = deployment['created_by']['name'].encode('utf-8')
                        print('\t\t%s (%s)' % (deployment['name'].encode('utf-8'), created_by))
                        if created_by in created:
                            created[created_by] = created[created_by] + 1
                        else:
                            created[created_by] = 1
                           
                        cat_count=cat_count+1
                        status_count=status_count+1
                        total_count=total_count+1
                print ('\t\tTotal ' + status + ' ' + cat_name + ' deployments: ' + str(cat_count))
            print ('\tTotal ' + status + ' deployments: ' + str(status_count))
        print ('Total deployments: ' + str(total_count))

        # print out totals
        for created_by in sorted(created, key=byCount, reverse=True):
            print "#   " + str(created[created_by]) + " : " + created_by

    elif created_by != None:
        # list by account
        data_filename='./'+created_by+'_executions.dat'
        print('Querying for all ' + created_by + ' RightScale deployments.')
        deployments=local_get_deployments(data_filename, ss_api, config)

        for deployment in sorted(deployments):
            cur_created_by = deployment['created_by']['name']
            if cur_created_by == created_by:
                status = deployment['status']
                name = deployment['name']
                cost = deployment['cost']['value']
                launched_at = deployment['timestamps']['launched_at']
                
                print status + "\t" + name + "\t" + cost + "\t" + launched_at + "\t" + cur_created_by + "\t"
    else:
        # nothing to do
        logger.error ("You have not specified enough input parameters to do anything")

    return 0
                             
exit(main())
