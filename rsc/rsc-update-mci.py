### TODO: Need to create this from the existing shell script
###
###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  A utility to create/update an mci
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################
#usage: rsc-update-mci.py [-h] --project PROJECT --release RELEASE
#                         --config_root_dir CONFIG_ROOT_DIR --name NAME
#                         --mci_name MCI_NAME --image-name IMAGE_NAME
#                         --instance-size INSTANCE_SIZE --google GOOGLE
#                         --description DESCRIPTION
#
#optional arguments:
#  -h, --help            show this help message and exit
#  --project PROJECT     The RightScale project containing the deployment.
#  --release RELEASE     The CIA release for the deployment.
#  --config_root_dir CONFIG_ROOT_DIR
#                        Root dir of global config utilities
#  --name NAME           The name of the RightScale deployment on which to
#                        operate.
#  --mci_name MCI_NAME   The name of the RightScale MCI to launch.
#  --image-name IMAGE_NAME
#                        Name of the image on the cloud provider.
#  --instance-size INSTANCE_SIZE
#                        Size of the instance (in provider terms).
#  --google GOOGLE       Name of the cloud provider the image is hosted on.
#  --description DESCRIPTION
#                        The description of the MCI.
###############################################################################

import argparse
import json
import os

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

ss_api = SelfService()

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment on which to operate.',
                        required=True)
    parser.add_argument('--mci_name',
                        help='The name of the RightScale MCI to launch.',
                        required=True)
    parser.add_argument('--image-name',
                        help='Name of the image on the cloud provider.',
                        required=True)
    parser.add_argument('--instance-size',
                        help='Size of the instance (in provider terms).',
                        required=True)
    parser.add_argument('--cloud',
                        help='Name of the cloud provider the image is hosted on.',
                        required=True)
    parser.add_argument('--description',
                        help='The description of the MCI.',
                        required=True)

    return parser.parse_args()

def main():
    # assume it will fail
    return_code = 1

    args = parse_arguments()
    config_root_dir = args.config_root_dir
    execution_name = args.name

    config = ConfigJSON(config_root_dir, args.project, args.release)
    config.read ()
    project_id = config.get ('project_id')

    return return_code
                             
exit(main())
