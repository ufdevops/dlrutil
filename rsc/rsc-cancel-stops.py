#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Cancel stops for a deployment for the next X number of days
###############################################################################
# Prerequisites: Assumes the RSC command from RightScale is installed and
# in the system search path.
###############################################################################
# Inputs:
# usage: rsc-cancel-stops.py [-h] --project PROJECT --release RELEASE --config_root_dir
#                            CONFIG_ROOT_DIR --name NAME --num_days_to_run
#                            NUM_DAYS_TO_RUN
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment on which to
#                         operate.
#   --num_days_to_run NUM_DAYS_TO_RUN
#                         The number of cancels you want to stop.
###############################################################################

import argparse
import subprocess

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment on which to operate.',
                        required=True)
    parser.add_argument('--num_days_to_run',
                        help='The number of cancels you want to stop.',
                        required=True)
    return parser.parse_args()

def main():
    # assume it will fail
    return_code = 1

    args = parse_arguments()
    num_days_to_run = args.num_days_to_run
    deployment_name = args.name

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')

    ss_api = SelfService()

    deployment_id = None
    try:
        # need both because cannot filter by name, only by id.  turn name into id
        # and then get deployment object
        deployment_id = rscutils.get_deployment_id(ss_api, project_id, deployment_name)
        deployment = rscutils.get_deployment_by_id(ss_api, project_id, deployment_id)

        next_action = None
        if "next_action" in deployment:
	    next_action=deployment["next_action"];

        action = None
        if next_action != None and "action" in next_action:
	    action=next_action["action"]

	if action == "stop":
	    next_occurrence=next_action["next_occurrence"]
	    next_id=next_action["id"]
	 
	    # skip future cancellations
	    logger.info ("Skipping next " + str(num_days_to_run) + " stops")

	    next_scheduled_occurrence=ss_api.skip_scheduledaction(
                project_id=project_id,
                id=next_id,
                count=num_days_to_run
            )

	    action=next_scheduled_occurrence["action"]
	    next_occurrence=next_scheduled_occurrence["next_occurrence"]

	    logger.info ("Deployment will " + action + " on " + next_occurrence)
	    return_code = 0
	elif action == None:
	    logger.warning ("The deployment has no next scheduled action")
	else:
	    logger.warning ("The deployment must be running to cancel stops")
        
    except subprocess.CalledProcessError as s:
        logger.error (s.output)
        
    except rscutils.NoDeploymentError as e:
        logger.error ("There is no deployment named " + deployment_name)
        return_code = 2

    return return_code

exit(main())
