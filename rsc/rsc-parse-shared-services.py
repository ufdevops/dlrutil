###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This utility will take a saved html-only version of the following page:
#   https://engconf.int.kronos.com/display/CIA/Google+-+Official+Shared+Services
#  and read out all the self-service links. These links will then be written out
#  to a csv file.
###############################################################################
# Pre-requisites:
#  None
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
from argparse import ArgumentParser
# HTMLParser imports
from HTMLParser import HTMLParser
# os imports
from os import path
# sys imports
from sys import exit

###############################################################################
# Classes
###############################################################################
class MyHTMLParser(HTMLParser):


    def __init__(self):
        """
        Constructor.
        """

        # Chain to constructor.
        HTMLParser.__init__(self)

        # Blank out array to deployments.
        self.deployment_links = []

    def handle_starttag(self, tag, attrs):
        """
        Called when the parser first encounters a tag.
        :param tag: The tag name.
        :param attrs: List of tag attributes.
        :return: None.
        """

        # Ensure we've got an 'a' link.
        if tag == "a":
            # Iterate through all of the attributes.
            for attr in attrs:
                # Ensure we only look at hrefs.
                if attr[0] == "href":
                    # Ensure we only have self-service in the value.
                    if "https://selfservice-4.rightscale.com/manager/exe" in attr[1]:
                        # Add to deployment links.
                        self.deployment_links.append(attr[1])


###############################################################################
# Functions
###############################################################################
def parse_arguments():
    """
    Parses arguments required by this program.
    :return: Values of said arguments.
    """

    parser = ArgumentParser(description='Parses official shared services from confluence.')

    parser.add_argument('-f', '--html-file', type=str,
                        help='Path to HTML file to parse.',
                        action='store', dest='html_file',
                        required=True)

    parser.add_argument('-o', '--out-file', type=str,
                        help='Path to output file.',
                        action='store', dest='out_file',
                        required=True)

    args = parser.parse_args()

    return args.html_file, args.out_file


###############################################################################
# Implementation
###############################################################################

# Parse the arguments.
arg_html_file_path, arg_out_file_path = parse_arguments()

# Ensure the file exists.
if not path.exists(arg_html_file_path):
    print('Path to HTML file %s does not exist.' % arg_html_file_path)
    exit(1)

# Open file handle to html.
with open(arg_html_file_path, 'r') as f:
    # Construct the html parser object.
    parser = MyHTMLParser()

    # Feed it the contents of the file.
    parser.feed(f.read())

    # Write out all the deployment links to a file.
    with open(arg_out_file_path, 'w') as out:
        for link in parser.deployment_links:
            # Some people have pasted shas inside the URL. Remove these.
            # This basically just grabs the root URL. TODO: Use regular
            # expressions.
            link = link.split('?')[0]

            # Write to file.
            out.write(link + '\n')

