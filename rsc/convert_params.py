###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  A utility to convert Jenkins job input parameters which end up as
#  environment variables to input parameters for deployment.  It also
#  takes the contents of the extra_inputs and convert them to deployment
#  parameters.  The contents of extra_inputs should be formated as JSON like
#  such:
#        {"tms_url":"http://ken01-tms01-app01.int.dev.mykronos.com","pwd":"jalapenos password","is_production_mode":"true"}
#
#  The key is the name of the CAT input parameter without the leading param_
#
###############################################################################
# usage: convert_params.py [-h] --project PROJECT --release RELEASE --config_root_dir CONFIG_ROOT_DIR
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
###############################################################################

import argparse
import json
import os

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

ss_api = SelfService()

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)

    return parser.parse_args()

def check_env_vars(evars):
    missing = False
    for e in evars:
        if os.getenv(e) == None:
            logger.error('You must set ' + e)
            missing = True
    if missing:
        logger.error ("Exiting because you are missing some environment variables.")
        exit (1)
    return

def format_param(param_name, param_type, param_value):
    return "'" + param_name + "'|'" + param_type + "'|'" + param_value + "'"

def check_environment(param_dict, input_list):
    for e in os.environ:
        param_name = 'param_' + e
        if param_name in param_dict:
            param_type = param_dict[param_name]["type"]
            param_value = os.environ[e]
            param_string = format_param(param_name, param_type, param_value)
            input_list.append(param_string)

    return input_list
        
def check_extra_inputs(param_dict, input_list):
    var_name = "extra_inputs"
    if var_name in os.environ:
        extra_string = os.environ[var_name]
        print extra_string
        extra_dict = json.loads(extra_string)
        for e in extra_dict:
            param_name = 'param_' + e
            if param_name in param_dict:
                param_type = param_dict[param_name]["type"]
                param_value = extra_dict[e]
                param_string = format_param(param_name, param_type, param_value)

                input_list.append(param_string)
                
    return input_list
        
def main():
    check_env_vars (["PYTHONPATH"])

    return_code = 1

    args = parse_arguments()
    config_root_dir = args.config_root_dir

    config = ConfigJSON(config_root_dir, args.project, args.release)

    input_list = []

    # check to see what input parameters are in the environment and get
    # pertinent deploy info from parameter dictionary in the config
    logger.info ("Getting input paramters from environment")
    input_list = check_environment(config.get_parameter_dict(), input_list)

    # check for an environment variable named extra_inputs.  It has json
    # for any extra inputs that aren't specified in the Jenkins job
    logger.info ("Getting input paramters from extra_inputs")
    input_list = check_extra_inputs(config.get_parameter_dict(), input_list)

    logger.info ("List of input parameters found:")
    logger.info ("---------------------")
    for o in input_list:
        print o
    logger.info ("---------------------")

    # This is the key output of this script.  This is how we expose the outputs
    # to the calling program.  It is in the format the deploy script expects.
    options = '|'.join(input_list)
    print 'OPTIONS="' + options + '"'
    return_code = 0

    return return_code
                             
exit(main())
