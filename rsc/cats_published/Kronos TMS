# -----------------------------------------------------------------------------
# Copyright 2016, Kronos Inc
# -----------------------------------------------------------------------------
# Rightscale CAT script for TMS deployment.
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# CAT Metadata
# -----------------------------------------------------------------------------
name              'Kronos TMS'
short_description 'Provides an TMS deployment.'
long_description <<-EOS
## Deployment information
[Current Services](https://engconf.int.kronos.com/display/CIA/Google+-+Official+Shared+Services)

**Image type**:
  The image to build the deployment from.

**Unique identifier**:
  A unique identifier to assign to this TMS instance.

## Network configuration

**Instance group**:
  This is the instance group to deploy to.

**Location**:
  The geographic location of the network to deploy this service in.
EOS
rs_ca_ver         20131202

# -----------------------------------------------------------------------------
# CAT user inputs
# -----------------------------------------------------------------------------
parameter 'param_pwd' do
  type           'string'
  category       'Security'
  label          'Password'
  description    'This deployment requires a password'
  allowed_pattern '(\W|^)(cronitez)(\W|$)'
end

parameter 'param_image_type' do
  type           'list'
  label          'Image type'
  category       'Deployment information'
  description    'The image to build the deployment from.'
  allowed_values 'Release - use this', 'CIA use only - unstable'
  default        'Release - use this'
end

parameter 'param_build_number' do
  type           'string'
  label          'Build Number'
  category       'Deployment configuration'
  description    'Jenkins build number to use (very old images are not availible)'
  allowed_pattern '^(Latest|[0-9]+)$'
  default        'Latest'
end

parameter 'param_cluster_id' do
  type           'string'
  category       'Deployment information'
  label          'Cluster identifier'
  description    'The unique cluster identifier for this TMS deployment. Get details of currently deployed official services at https://engconf.int.kronos.com/display/CIA/Google+-+Official+Shared+Services'
  min_length     2
  max_length     2
  allowed_pattern '/^([0-9][0-9])$/'
end

parameter 'param_stack_id' do
  category       'Deployment information'
  label          'Stack ID'
  type           'string'
  description    'Stack-Id where TMS is being deployed.'
  default        'Kronos_US'
end

parameter 'param_domain_name' do
  category       'Deployment information'
  label          'Domain Name'
  type           'string'
  description    'Domain Name for this TMS deployment.'
  default        'mykronos.internal'
end

parameter 'param_location' do
  type           'list'
  label          'Location'
  category       'Network configuration'
  description    'The geographic location of the network to deploy this service in.'
  allowed_values 'North America Development', 'Asia Development', 'North America SQA', 'North America Performance', 'North America (CIA use only)', 'Asia (CIA use only)'
  default        'North America Development'
end


parameter 'param_dbname' do
  category       'Database Configuration'
  label          'Database Name'
  type           'string'
  description <<-EOS
    The name of the database being used.
    For more info see https://engconf.int.kronos.com/display/CIA/Google+Cloud+Deployment+FAQ
  EOS
  default        'ppas_example'
end

parameter 'param_dbserver' do
  category       'Database Configuration'
  label          'Database Server FQDN'
  type           'string'
  description <<-EOS
    The fully qualified domain name of the database server to connect to.
    For more info see https://engconf.int.kronos.com/display/CIA/Google+Cloud+Deployment+FAQ
  EOS
  allowed_pattern /^keng01-(dev|ops|prf|sqa|xfn)\d{2}-(wfm|tms)\d{2}-dbs\d{2}(-\d{10})?.(dev|ops|prf|sqa|xfn).mykronos.internal$/
  default        'keng01-dev01-wfm01-dbs00.dev.mykronos.internal'
end

parameter 'param_db_username' do
  category       'Database Configuration'
  label          'Database Username'
  type           'string'
  description    'Username to connect to the database with.'
  default        'tms'
end

parameter 'param_db_password' do
  category       'Database Configuration'
  label          'Database password'
  type           'string'
  description    'Password to authenticate user with.'
  default        'kronites'
end

parameter 'param_redis_host' do
  category      'Caching Service Configuration'
  label         'Redis FQDN'
  type          'string'
  description <<-EOS
    Use Redis server wrt to deployment in North America or Asia
  Details about deployed redis servers can be found here
  https://engconf.int.kronos.com/display/CIA/Google+-+Official+Shared+Services
    sample pattern: keng01-dev01-dmc01.dev.mykronos.internal
  EOS
  default        'keng01-dev01-dmc02.dev.mykronos.internal'
end

parameter 'param_auth_url' do
  category      'Authentication Configuration'
  label         'Authentication Server URL'
  type          'string'
  default        'http://keng01-dev01-ath05-oam05.dev.mykronos.internal'
  description <<-EOS
    Use Auth server wrt to deployment in North America or Asia
  Details about deployed Auth servers can be found here
  https://engconf.int.kronos.com/display/CIA/Google+-+Official+Shared+Services
    sample pattern: http://keng01-dev01-ath01-oam01.dev.mykronos.internal
  EOS
end

parameter 'param_deployment_purpose' do
  type           'list'
  label          'Deployment purpose'
  category       'Deployment information'
  description    'How this deployment will be used.'
  allowed_values 'Development (Platform team only)', 'Production (Ops team only)'
  default        'Development (Platform team only)'
end

# -----------------------------------------------------------------------------
# Information mappings.
# -----------------------------------------------------------------------------

mapping 'map_locations' do {
    'North America Development' => {
        'mapping_datacenter'          => 'us-east1-b',
        'mapping_location_identifier' => 'use1b',
        'mapping_subnetwork'          => 'dev-net (us-east1)',
        'mapping_subnet_identifier'   => 'dev',
        'mapping_sectag_kvpn'         => 'devnet-kvpn--keng01-net',
        'mapping_sectag_internal'     => 'devnet-internal--keng01-net',
        'mapping_sectag_local'        => 'devnet-local--keng01-net',
        'mapping_geographic_location' => 'North America' },
    'North America SQA' => {
        'mapping_datacenter'          => 'us-east1-b',
        'mapping_location_identifier' => 'use1b',
        'mapping_subnetwork'          => 'sqa-net (us-east1)',
        'mapping_subnet_identifier'   => 'sqa',
        'mapping_sectag_kvpn'         => 'sqanet-kvpn--keng01-net',
        'mapping_sectag_local'        => 'sqanet-local--keng01-net',
        'mapping_geographic_location' => 'North America' },
    'North America Performance' => {
        'mapping_datacenter'          => 'us-east1-b',
        'mapping_location_identifier' => 'use1b',
        'mapping_subnetwork'          => 'prf-net (us-east1)',
        'mapping_subnet_identifier'   => 'prf',
        'mapping_sectag_kvpn'         => 'prfnet-kvpn--keng01-net',
        'mapping_sectag_local'        => 'prfnet-local--keng01-net',
        'mapping_geographic_location' => 'North America' },
    'North America (CIA use only)' => {
        'mapping_datacenter'          => 'us-east1-b',
        'mapping_location_identifier' => 'use1b',
        'mapping_subnetwork'          => 'ops-net (us-east1)',
        'mapping_subnet_identifier'   => 'ops',
        'mapping_sectag_kvpn'         => 'opsnet-kvpn--keng01-net',
        'mapping_sectag_internal'     => 'opsnet-internal--keng01-net',
        'mapping_sectag_local'        => 'opsnet-local--keng01-net',
        'mapping_geographic_location' => 'North America' },
    'Asia Development' => {
        'mapping_datacenter'          => 'asia-east1-a',
        'mapping_location_identifier' => 'use1a',
        'mapping_subnetwork'          => 'dev-net-ase1 (asia-east1)',
        'mapping_subnet_identifier'   => 'dev',
        'mapping_sectag_kvpn'         => 'devnet-kvpn--keng01-net',
        'mapping_sectag_internal'     => 'devnet-internal--keng01-net',
        'mapping_sectag_local'        => 'devnet-local--keng01-net',
        'mapping_geographic_location' => 'Asia' },
    'Asia SQA' => {
        'mapping_datacenter'          => 'us-east1-a',
        'mapping_location_identifier' => 'use1a',
        'mapping_subnetwork'          => 'sqa-net (us-east1)',
        'mapping_subnet_identifier'   => 'sqa',
        'mapping_sectag_kvpn'         => 'sqanet-kvpn--keng01-net',
        'mapping_sectag_local'        => 'sqanet-local--keng01-net',
        'mapping_geographic_location' => 'Asia' },
    'Asia Performance' => {
        'mapping_datacenter'          => 'asia-east1-a',
        'mapping_location_identifier' => 'use1a',
        'mapping_subnetwork'          => 'prf-net-ase1 (asia-east1)',
        'mapping_subnet_identifier'   => 'prf',
        'mapping_sectag_kvpn'         => 'prfnet-kvpn--keng01-net',
        'mapping_sectag_local'        => 'prfnet-local--keng01-net',
        'mapping_geographic_location' => 'Asia' },
    'Asia (CIA use only)' => {
        'mapping_datacenter'          => 'asia-east1-a',
        'mapping_location_identifier' => 'use1a',
        'mapping_subnetwork'          => 'ops-net-ase1 (asia-east1)',
        'mapping_subnet_identifier'   => 'ops',
        'mapping_sectag_kvpn'         => 'opsnet-kvpn--keng01-net',
        'mapping_sectag_internal'     => 'opsnet-internal--keng01-net',
        'mapping_sectag_local'        => 'opsnet-local--keng01-net',
        'mapping_geographic_location' => 'Asia' }
}
end

mapping 'map_builds' do {
  'Release - use this' => {
    'mapping_branch' => 'master' },
  'CIA use only - unstable' => {
    'mapping_branch' => 'develop' }
}
end


# -----------------------------------------------------------------------------
# Outputs resources
# -----------------------------------------------------------------------------
output 'output_private_ip' do
  label         'Private IP'
  category      'Connection Info'
  default_value @server_tms.private_ip_address
  description   'Internal IP Address'
end

output 'output_datacenter' do
  label         'Datacenter'
  category      'Instance Information'
  default_value map($map_locations, $param_location, "mapping_datacenter")
  description   'Instance deployed in this datacenter.'
end

output 'output_subnetwork' do
  label         'Subnetwork'
  category      'Instance Information'
  default_value map($map_locations, $param_location, "mapping_subnetwork")
  description   'Subnetwork used by this instance.'
end

output 'output_fqdn' do
  label         'FQDN'
  category      'Instance Information'
  description   'This is the FQDN for this Redis deployment.'
  default_value join([@server_tms.name, ".",
                      map($map_locations, $param_location, "mapping_subnet_identifier"),
                      ".", $param_domain_name])
end

output 'output_url' do
  label         'TMS URL'
  category      'Instance Information'
  default_value join(["http://", @server_tms.name, ".",
                      map($map_locations, $param_location, "mapping_subnet_identifier"),
                      ".", $param_domain_name, "/metaui"])
  description   'This is the URL of TMS front end.'
end

output 'output_build_number' do
  label         'Build Number'
  category      'Instance Information'
  description   'Build number of image used'
  default_value $param_build_number
end

output 'output_build_level' do
  label         'Build Level Requested'
  category      'Instance Information'
  description   'Build number of image used'
  default_value $param_image_type
end

###############################################################################
# Get Google Image
###############################################################################
define get_google_image($cookbook_name, $param_build_number, $build_level) return $google_image_href do
  if ($param_build_number == 'Latest')
    @all_images = rs.images.get(filter: join(['name==', $cookbook_name]))
    if (size(@all_images) > 0)
      @google_image = select(sort(@all_images, 'name', 'desc'), {"name": join(["/\-", $build_level, "/"])})
      if (size(@google_image) > 0)
        $google_image_href = @google_image.href
      else
        raise 'No Google image exists for this cookbook build level.'
      end
    else
      raise 'No Google image exists for this cookbook.'
    end
  else
    @all_images = rs.images.get(filter: join(['name==', $cookbook_name]))
    if (size(@all_images) > 0)
      @google_image = select(@all_images, {"name": join(["/\-b", $param_build_number, "\-", $build_level, "/"])})
      if (size(@google_image) > 0)
        $google_image_href = @google_image.href
      else
        raise 'No Google image exists for this build number at this build level.'
      end
    else
      raise 'No Google image exists for this cookbook.'
    end
  end
end

# -----------------------------------------------------------------------------
# Operations
# -----------------------------------------------------------------------------

operation "stop" do
  definition "stop_servers"
end

operation "start" do
  definition "start_servers"
end

define stop_servers() do

  @@deployment.servers().current_instance().stop()

  # Wait for them all to stop since the stop call is asynchronous.  Don't want user trying to
  # start before they are all stopped.
  $wake_condition = "/^(stranded|stranded in booting|stopped|terminated|inactive|error|provisioned)$/"
  sleep_until all?(@@deployment.servers().state[], $wake_condition)
  $all_stopped = all?(@@deployment.servers().state[], "provisioned")

  if !$all_stopped
    raise "Failed to stop one or more servers"
  end
end

define start_servers() do
  @@deployment.servers().current_instance().start()

  # Wait for them all to start/fail since the start call is asynchronous.  Don't want user trying to
  # stop before they are all started.
  $wake_condition = "/^(stranded|stranded in booting|stopped|terminated|inactive|error|operational)$/"
  sleep_until all?(@@deployment.servers().state[], $wake_condition)
  $all_started = all?(@@deployment.servers().state[], "operational")

  if !$all_started
    raise "Failed to start one or more servers"
  end
end

operation "launch" do
  definition "launch"
end

#Launch Configuration

define launch($param_build_number, $map_builds, $param_image_type, $map_locations, $param_location, $param_cluster_id, $param_deployment_purpose, $param_domain_name, @server_tms) return @server_tms do

  # Construct a filter to search for existing.
  $filter = join(["keng01-", map($map_locations, $param_location, 'mapping_subnet_identifier'),
                                    '01-tms', $param_cluster_id])

  # For development, append a unique timestamp. This will prevent clashing
  # when users attempt to deploy something to the same cluster number.
  if $param_deployment_purpose == 'Development (Platform team only)'
    $filter = join([ $filter, '-', to_n(now()) ])
  end

  # Search for any existing TMS deployments.
  @results = rs.servers.get(filter: join(['name==', $filter]))

  # If they have one, fail.
  if size(@results) > 0
    raise "A TMS deployment with a server named " + $filter + " already exists. For existing deployments check here: https://engconf.int.kronos.com/display/CIA/Google+-+Official+Shared+Services"
  end

  $cookbook_name = 'kronos-tms-'
  $build_level   = map($map_builds, $param_image_type, "mapping_branch")
  call get_google_image($cookbook_name, $param_build_number, $build_level) retrieve $google_image_href

  #############################################################################
  # Setup server properties.
  #############################################################################

  # Set the hostname and server name manually using JSON.
  $tms_json = to_object(@server_tms)
  $tms_json['fields']['name'] = $filter
  $tms_json['fields']['inputs']['HOST'] = join(['text:', $filter])
  $tms_json['fields']['image_href'] = $google_image_href
  $tms_json['fields']['inputs']['HOST_URL'] = join(['text:', $filter,
      '.', map($map_locations,$param_location,'mapping_subnet_identifier'),
      '.', $param_domain_name])

  # Cast JSON data back to TMS reference.
  @server_tms = $tms_json

  #############################################################################
  # Apply Kronos specific tags.
  ###########################################=#################################
  $tag_deployment_geography = join(['kronos:geographic_location=',
                                    map($map_locations, $param_location, 'mapping_geographic_location')])
  $tag_subnet = join(['kronos:subnet=', map($map_locations, $param_location, 'mapping_subnet_identifier')])

  # Apply the tags
  rs.tags.multi_add(
      resource_hrefs: [@@deployment.href],
      tags: [ $tag_deployment_geography,
              $tag_subnet ]
  )

  # Provision TMS.
  provision(@server_tms)
end


# -----------------------------------------------------------------------------
# CAT resources
# -----------------------------------------------------------------------------
resource 'server_tms', type: 'server' do
  name                        '' # Note: Intentionally left blank: See tms_json variable above.
  cloud                       'google'
  network                     'keng01-net'
  instance_type               'n1-standard-2'
  server_template             'st-kronos-tms-production'
  datacenter                  map($map_locations, $param_location, "mapping_datacenter")
  subnets                     map($map_locations, $param_location, "mapping_subnetwork")
  security_groups             map($map_locations, $param_location, 'mapping_sectag_kvpn'),
                              map($map_locations, $param_location, 'mapping_sectag_internal'),
                              map($map_locations, $param_location, 'mapping_sectag_local')
  associate_public_ip_address false
  inputs do {
    'HOST' => 'text:',                          # Note: This is intentionally left blank. See tms_json variable above.
  'LOCATION' => join(['text:', $param_location]),
    'SERVER_HOSTNAME' => 'env:RS_SERVER_NAME',  # Fix rightscale hostname problem.
    'SUBNET' => join(['text:', map($map_locations, $param_location, 'mapping_subnet_identifier')]),
    'HOST_URL' => 'text:',
    'DB_SERVER_NAME' => join(['text:', $param_dbserver]),
    'DB_NAME'        => join(['text:', $param_dbname]),
    'DB_USER'       => join(['text:', $param_db_username]),
    'DB_PASS'        => join(['text:', $param_db_password]),
    'REDIS_URL'      => join(['text:', $param_redis_host]),
    'AUTH_URL'       => join(['text:', $param_auth_url]),
    'STACK_ID'       => join(['text:', $param_stack_id]),
    'DOMAIN_NAME'    => join(['text:', $param_domain_name]),
    #this is here so the param input shows up when the CAT is launched. Ignore but don't remove.
    'BLANK' => join(['text:', $param_pwd])
  } end
end
