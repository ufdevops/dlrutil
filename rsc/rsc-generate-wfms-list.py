###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  Finds all ppas servers in rightscale in dev both in US and asia.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
# csv imports
import csv
# datetime imports
import datetime
# dateutil imports
from dateutil import parser as dateutil_parser
from dateutil.tz import tzlocal
from dateutil.relativedelta import relativedelta
# itertools imports
import itertools
# json imports
import json
# multiprocessing imports
import multiprocessing
# os imports
import os
# re imports
import re
# rsc.api imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
# sys imports
import sys

###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()

# Create a thread pool.
thread_pool = ThreadPoolExecutor(multiprocessing.cpu_count())

# Stores all of the futures.
future_results = []

# Sanctioned shared services.
sanctioned_services = []

# Whitelisted deployments.
whitelisted_deployments = []

# Deployments we're going to nuke.
deployments_to_nuke = []

# Subnets to look at.
subnets_to_consider = [
    "dev-net",
    "ops-net"
]

# Shared services to look at.
shared_services_to_consider = [
    "application-foundation",
    "generic-foundation",
    "apigee",
    "apigeepublish",
    "birt-ihub",
    "birt-singlenode",
    "birt-sysconsole",
    "kronos-dse",
    "kronos-ihub",
    "kronos-ihub-cluster",
    "kronos-openam",
    "kronos-opendj",
    "kronos-opscenter",
    "post-openam-config",
    "kronos-rabbitmq",
    "kronos-redis",
    "kronos-redis-sentinel",
    "kronos-sdm",
    "kronos-tms",
    "kronos-udm",
    "kronos-ums",
    "kronos-workflow-admin",
    "kronos-workflow-app"
]

wfm_templates_to_consider = [
    "wfm-backend",
    "wfm-frontend"
]

# Get the current date/time with the timezone.
current_time = datetime.datetime.now(tzlocal())

# Make timedelta object for one month.
one_month_ago = current_time - relativedelta(months=1)

# Make timedelta object for two weeks ago.
two_weeks_ago = current_time - relativedelta(days=14)

###############################################################################
# Functions
###############################################################################
def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()

    parser.add_argument('--project-id',
                        help="ID of the RightScale project to operate on.",
                        required=True)

    parser.add_argument('--sanctioned-services',
                        help="Text file containing shared services.",
                        required=True)

    parser.add_argument('--deployment-whitelist',
                        help="Text file containing whitelisted deployments.",
                        required=True)

    #parser.add_argument('--instances-file',
    #                    help="JSON file containing instance data from rightscale.",
    #                    required=True)

    parser.add_argument('--deployment-file',
                        help="JSON file containing deployment data for each deployment.",
                        required=True)

    args = parser.parse_args()

    return args.project_id, args.sanctioned_services, args.deployment_whitelist, args.deployment_file


def is_instance_in_correct_subnet(instance):
    instance_subnets = instance["subnets"]
    if len(instance_subnets) > 1:
        #print("Instance %s is in multiple subnets. Skipping." % instance["name"])
        return False

    elif len(instance_subnets) < 1:
        #print("Instance %s is not in any subnets. Skipping." % instance["name"])
        return False

    result = False
    subnet_name = instance_subnets[0]["name"]
    for subnet in subnets_to_consider:
        if subnet in subnet_name:
            result = True
            break

    #print("Instance %s is in subnet %s." % (instance["name"], subnet_name))
    return result


def is_instance_ppas(instance):
    server_template_name = instance["server_template"]["name"]
    return "ppas" in server_template_name


def is_instance_phase7(instance):
    server_template_name = instance["server_template"]["name"]
    return "_phase" in server_template_name


def is_shared_service(instance):
    server_template_name = instance["server_template"]["name"]
    for service_name in shared_services_to_consider:
        if service_name in server_template_name:
            return True
    return False


def is_phase_deployment(deployment):
    return "phase" in deployment["name"].lower()


def is_svcci_deployment(deployment):
    for tag in deployment["tags"]:
        if "launched_by" in tag:
            dep_owner = tag.split("=")[1]
            if "svcci" in dep_owner.lower():
                return True
    return False

def is_wfm(instance):
    server_template_name = instance["server_template"]["name"]
    for wfm_name in wfm_templates_to_consider:
        if wfm_name in server_template_name:
            return True
    return False


def has_do_not_delete(deployment):
    for tag in deployment["tags"]:
        if tag == "kronos:do_not_delete=true":
            return True
    return False

def process_deployment(deployment):
    try:
        # Check to see if it's excluded.
        if deployment["href"] in sanctioned_services:
            #print("Deployment %s is in sanctioned services." % deployment["name"])
            return (deployment,[],[])

        if deployment["href"] in whitelisted_deployments:
            #print("Deployment %s is in whitelist." % deployment["name"])
            return (deployment,[],[])

        if has_do_not_delete(deployment):
            return (deployment,[],[])

        if is_phase_deployment(deployment):
            return (deployment,[],[])

        if is_svcci_deployment(deployment):
            return (deployment,[],[])

        # Get the instances in the deployment.
        instances = deployment["instances"]
        if len(instances) < 1:
            #print(json.dumps(deployment, sort_keys=True, indent=4))
            #print("No instances in deployment %s." % deployment["name"])
            return (deployment,[],[])

        # Iterate through each instance. Generate a list of bad instances.
        bad_instances = []
        good_instances = []
        for instance in instances:
            # Ensure we're in the right subnet.
            if not is_instance_in_correct_subnet(instance):
                #print("Instance %s is not in the correct subnet." % instance["name"])
                good_instances.append((instance, "Is in wrong subnet"))
                continue

            # Ensure we have an actual server template.
            if "server_template" in instance:
                # Don't even bother looking at PPAS.
                if is_instance_ppas(instance):
                    print("Instance %s is a PPAS server." % instance["name"])
                    good_instances.append((instance, "Is PPAS server"))
                    continue

                # Don't bother with shared services or non-wfm deployments.
                if not is_shared_service(instance) and not is_wfm(instance):
                    print("Instance %s is not a shared service or wfm." % instance["name"])
                    good_instances.append((instance, "Is not a shared service or wfm"))
                    continue

                # Don't bother with phase7.
                if is_instance_phase7(instance):
                    print("Instance %s is phase7 server." % instance["name"])
                    good_instances.append((instance, "Is Phase 7 deployment"))
                    continue
            else:
                # We automatically consider this instance to be crap if there is no server
                # template. This should never happen if people used our deployment process.
                print("Instance %s does not have server template." % instance["name"])
                bad_instances.append(instance)
                continue

            # If there's a booted at timestamp, we need to look at that first.
            if "booted_at" in instance["timestamps"]:
                booted_at = dateutil_parser.parse(instance["timestamps"]["booted_at"])
                if is_wfm(instance):
                    if booted_at >= two_weeks_ago:
                        print("Skipping WFM %s as it was last booted more than two weeks ago." % instance["name"])
                        good_instances.append((instance, "Is too new to terminate (WFM)"))
                        continue
                elif is_shared_service(instance):
                    if booted_at >= one_month_ago:
                        print("Shared service %s booted more than a month ago. Too new to terminate." % instance["name"])
                        good_instances.append((instance, "Is too new to terminate (Shared Service)"))
                        continue

                # If we are here, it means we didn't pass the checks above.
                bad_instances.append(instance)

            # If no booted_at timestamp, we have no choice but to look at the creation time.
            if "created_at" in instance["timestamps"]:
                created_at = dateutil_parser.parse(instance["timestamps"]["created_at"])
                if is_wfm(instance):
                    if created_at >= two_weeks_ago:
                        print("WFM %s created on %s. Too new to terminate." % (instance["name"], created_at))
                        good_instances.append((instance, "Is too new to terminate (WFM)"))
                        continue

                elif is_shared_service(instance):
                    if created_at >= one_month_ago:
                        print("Shared service %s created on %s. Too new to terminate." % (instance["name"], created_at))
                        good_instances.append((instance, "Is too new to terminate (Shared Service)"))
                        continue

                # If we are here, it means we didn't pass the checks above.
                bad_instances.append(instance)

            # If we are here, it means we didn't fit any of the termination criteria.
            good_instances.append((instance, "Did not fit any termination criteria"))

        return (deployment, bad_instances, good_instances)
    except Exception as e:
        print(json.dumps(deployment, sort_keys=True, indent=4))
        sys.exit(1)


def write_deployment_data_to_file(csv_writer_good, csv_writer_bad, deployment_result):
    # Now create stubs for all the csv rows.
    dep_owner = "(missing)"
    dep_name  = "(missing)"
    dep_href  = "(missing)"

    # Retrieve the deployment information.
    deployment = deployment_result[0]
    bad_instances = deployment_result[1]
    good_instances = deployment_result[2]

    # Get generic deployment information.
    for tag in deployment["tags"]:
        if "launched_by" in tag:
            dep_owner = tag.split("=")[1]

    if "name" in deployment:
        dep_name = deployment["name"]

    if "href" in deployment:
        dep_href = deployment["href"]

    # Write out data for bad instances.
    for instance in bad_instances:
        ins_type  = "(missing)"
        ins_st    = "(missing)"
        ins_name  = "(missing)"
        ins_sub   = "(missing)"
        ins_state = "(missing)"
        ins_cd    = "(missing)"
        ins_lnch  = "(missing)"
        ins_href  = "(missing)"

        # Get instance properties.
        if "server_template" in instance:
            ins_st = instance["server_template"]["name"]

        if "name" in instance:
            ins_name = instance["name"]

        if "subnets" in instance:
            ins_sub = ""
            for subnet in instance["subnets"]:
                ins_sub = ins_sub + subnet["name"] + ","
            ins_sub = ins_sub.rstrip(",")

        if "state" in instance:
            ins_state = instance["state"]

        if "timestamps" in instance:
            if "created_at" in instance["timestamps"]:
                ins_cd = instance["timestamps"]["created_at"]
            if "booted_at" in instance["timestamps"]:
                ins_lnch = instance["timestamps"]["booted_at"]

        if "instance_type" in instance["links"]:
            ins_type = instance["links"]["instance_type"]["name"]

        if "href" in instance:
            ins_href = instance["href"]

        # Write the csv data.
        result = [dep_owner, dep_name, dep_href, ins_st,
         ins_type, ins_name, ins_sub, ins_state,
         ins_cd, ins_lnch, ins_href]

        if len(deployment["tags"]) > 0:
            result = result + sorted(deployment["tags"])

        csv_writer_bad.writerow(result)

    # Write out data for good instances.
    for instance_tup in good_instances:
        ins_type  = "(missing)"
        ins_st    = "(missing)"
        ins_name  = "(missing)"
        ins_sub   = "(missing)"
        ins_state = "(missing)"
        ins_cd    = "(missing)"
        ins_lnch  = "(missing)"
        ins_href  = "(missing)"

        instance = instance_tup[0]
        reason = instance_tup[1]

        # Get instance properties.
        if "server_template" in instance:
            ins_st = instance["server_template"]["name"]

        if "name" in instance:
            ins_name = instance["name"]

        if "subnets" in instance:
            ins_sub = ""
            for subnet in instance["subnets"]:
                ins_sub = ins_sub + subnet["name"] + ","
            ins_sub = ins_sub.rstrip(",")

        if "state" in instance:
            ins_state = instance["state"]

        if "timestamps" in instance:
            if "created_at" in instance["timestamps"]:
                ins_cd = instance["timestamps"]["created_at"]
            if "booted_at" in instance["timestamps"]:
                ins_lnch = instance["timestamps"]["booted_at"]

        if "instance_type" in instance["links"]:
            ins_type = instance["links"]["instance_type"]["name"]

        if "href" in instance:
            ins_href = instance["href"]

        # Write the csv data.
        result = [dep_owner, dep_name, dep_href, ins_st,
         ins_type, ins_name, ins_sub, ins_state,
         ins_cd, ins_lnch, ins_href]

        if len(deployment["tags"]) > 0:
            result = result + sorted(deployment["tags"])

        csv_writer_good.writerow(result)

###############################################################################
# Implementation
###############################################################################

#with open(instances_file, "r") as instances_file_stream:
#    instances_to_consider = json.load(instances_file_stream)
#    print("There are %d instances to consider." % len(instances_to_consider))

# Parse arguments.
project_id, sanctioned_services_file, deployment_whitelist_file, deployment_file = parse_arguments()

# Read out the sanctioned services data.
with open(sanctioned_services_file, "r") as sanctioned_services_stream:
    sanctioned_services = sanctioned_services_stream.read().split()
    print("There are %d sanctioned shared services." % len(sanctioned_services))

# Read out the deployment whitelist.
with open(deployment_whitelist_file, "r") as deployment_whitelist_stream:
    whitelisted_deployments = deployment_whitelist_stream.read().split()
    print("There are %d whitelisted deployments." % len(whitelisted_deployments))

# Read out the instance data.
with open(deployment_file, "r") as deployment_file_stream:
    deployments_to_consider = json.load(deployment_file_stream)
    print("There are %d deployments to consider." % len(deployments_to_consider))

# Perform the processing.
counter = 0

future_results = []
for deployment in deployments_to_consider:
    future_result = thread_pool.submit(process_deployment, deployment)
    future_results.append(future_result)

# Wait for the threadpool to finish.
thread_pool.shutdown(wait=True)

# Write results out to a csv file.
with open("bad_instances.csv", "w") as bad_deployments_stream:
    with open("good_instances.csv", "w") as good_deployments_stream:
        bad_deployments_writer = csv.writer(bad_deployments_stream)
        good_deployments_writer = csv.writer(good_deployments_stream)

        # Write the headers
        bad_deployments_writer.writerow([
            "Owner",
            "Deployment name",
            "Deployment href",
            "Server template",
            "Instance type",
            "Instance name",
            "Subnet",
            "State",
            "Create date",
            "Launch date",
            "Instance href"
        ])

        good_deployments_writer.writerow([
            "Owner",
            "Deployment name",
            "Deployment href",
            "Server template",
            "Instance type",
            "Instance name",
            "Subnet",
            "State",
            "Create date",
            "Launch date",
            "Reason",
            "Instance href"
        ])

        for future_result in future_results:
            if future_result.exception():
                print("An exception occurred: %s" % future_result.exception())
                continue

            result = future_result.result()
            write_deployment_data_to_file(good_deployments_writer, bad_deployments_writer, result)







