#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Utility to get the IPs for all servers in a deployment
###############################################################################
# Prerequisites: Assumes the RSC command from RightScale is installed and
# in the system search path.
###############################################################################
# usage: rsc-get-ips.py [-h] --project PROJECT --release RELEASE --config_root_dir CONFIG_ROOT_DIR
#                       --name NAME
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment on which to
#                         operate.
###############################################################################

import argparse
import subprocess

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment on which to operate.',
                        required=True)
    return parser.parse_args()

def main():
    # assume it will fail
    return_code = 1

    args = parse_arguments()
    deployment_name = args.name

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')

    ss_api = SelfService()
    cm_api = CM15()

    deployment_id = None
    try:
        # Retrieve overall data for execution.
        deployment_id = rscutils.get_deployment_id(ss_api, project_id, args.name)
        deployment = rscutils.get_deployment_by_id(ss_api, project_id, deployment_id)

        # Iterate through each server in the deployment.
        deployment_servers=deployment["api_resources"]
        
        backend_ip = "unknown"
        frontend_ip = "unknown"
        for server in deployment_servers:
            name = server["name"]
            private_ips = server["value"]["details"]["current_instance"]["private_ip_addresses"]

            if name == "server_wfm_backend":
                backend_ip = private_ips[0]
                print "WFM_BACKEND_IP=" + backend_ip
            elif name == "server_wfm_frontend":
                frontend_ip = private_ips[0]
                print "WFM_FRONTEND_IP=" + frontend_ip
            else:
                ip = private_ips[0]
                print "Server: " + name
                print "IP=" + ip

        return_code = 0
    except subprocess.CalledProcessError as s:
        logger.error (s.output)
        
    except rscutils.NoDeploymentError as e:
        logger.error ("There is no deployment named " + deployment_name)
        return_code = 2

    return return_code

exit(main())
