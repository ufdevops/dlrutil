###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This RightScale utility finds WFMs who have used build numbers between
#  445 and 473 for frontend and/or backend.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
#  This script also assumes you have Python 2's "futures" package installed.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
# itertools imports
import itertools
# json imports
import json
# os imports
import os
# rsc.api imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()

###############################################################################
# Helper methods
###############################################################################

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()

    parser.add_argument('--project-id',
                        help="ID of the RightScale project to operate on.",
                        required=True)

    args = parser.parse_args()

    return args.project_id

###############################################################################
# Implementation
###############################################################################

# Process the command line arguments.
arg_rs_project_id = parse_arguments()

# First off, we need to make sure we've queried for all of the deployments.
if not os.path.isfile('./executions.dat'):
    print('Querying for all RightScale deployments..')

    # Perform the query.
    deployments = ss_api.index_execution(
        project_id=arg_rs_project_id,
        view='expanded')

    # Write the data to a file.
    with open('./executions.dat', 'w') as out_file:
        json.dump(deployments, out_file)

with open('./executions.dat', 'r') as in_file:
    deployments = json.load(in_file)

# Create ten lists we're going to consolidate after running
# the threads.
thread_statics = []
for i in range(0, 9):
    thread_statics.append([])

# Image look up table.
image_dictionary = {}

# Used to cycle through array indices to append data to.
current_thread_id = 0

# Progress counters.
deployment_count = len(deployments)
deployments_processed = 0

def perform_work(deployment_data, array_index):
    """
    Checks to see if a deployment has a bad image.
    :param deployment:
    :param array_index: Index in thread_statics array to add to.
    :return: A dictionary containing data about the deployment.
    """

    print("Processing deployment %d out of %d." % (deployments_processed, deployment_count))

    # Shim variable.
    deployment = deployment_data

    # Ensure it is a WFM Services deployment.
    deployment_type = deployment['launched_from_summary']['value']['name']
    if deployment_type not in ['WFM Services - Team', 'WFM Services - User']:
        return

    # Ensure that the deployment is not terminated.
    if deployment['status'] not in ['running', 'stopped']:
        return

    # Get the build numbers.
    backend_build_number = None
    frontend_build_number = None

    for resource in deployment['api_resources']:
        if resource['type'] != 'servers':
            continue

        # Ensure the links are all valid.
        if not (resource and resource['value'] and resource['value']['details'] and \
                        resource['value']['details']['current_instance'] and \
                        resource['value']['details']['current_instance']['links']):
            print('Skipping resource due to invalid resource data.')
            continue

        # Get resource links.
        resource_details = resource['value']['details']
        resource_links = resource_details['current_instance']['links']

        # Get the resource instance link.
        index = next(index for (index, d) in enumerate(resource_links) if d['rel'] == 'self')
        resource_instance = resource_links[index]['href']

        # Skip invalid instances.
        if not resource_instance:
            print('Skipping deployment %s. Invalid instance.' % deployment['name'])
            continue

        # Get the instance identifier.
        resource_instance_id = resource_instance.split('/')[5]

        try:
            # Get the image link that the instance is using.
            instance_data = cm_api.show_instance(cloud_id='2175', id=resource_instance_id, view="extended")
            instance_data_links = instance_data['links']

            index = next(index for (index, d) in enumerate(instance_data_links) if d['rel'] == 'image')
            image_link = instance_data_links[index]['href']

            # Get the image identifier.
            image_link_id = image_link.split('/')[5]

            # Is the image already in the dictionary?
            if image_link not in image_dictionary:
                # Query for the image data, then the name.
                image_data = cm_api.show_image(cloud_id='2175', id=image_link_id)
                image_name = image_data['name']

                # Split the image name by hypens.
                image_name_fields = image_name.split('-')

                # Get the build number. Note: skip the b in the beginning.
                image_build_number = int(image_name_fields[3][1:])

                # Insert image build number into dictionary.
                image_dictionary[image_link] = image_build_number
            else:
                # Pull out the build number.
                image_build_number = image_dictionary[image_link]

            # If the image build number doesn't fall into the range, get out.
            if image_build_number not in range(445, 473 + 1):
                continue

            # Now handle frontend or backend.
            if 'backend' in resource['name']:
                backend_build_number = image_build_number
                continue

            if 'frontend' in resource['name']:
                frontend_build_number = image_build_number
                continue

            # Stop querying if we're done.
            if backend_build_number and frontend_build_number:
                break

        except:
            print('\tException encountered. Skipping resource..')
            break

    # Ensure we actually got a build number back.
    if not backend_build_number or not frontend_build_number:
        print('Skipping deployment %s.' % deployment['name'])
        return

    # Add to the bad deployments list.
    thread_statics[array_index].append({
        'deployment' : deployment,
        'backend' : backend_build_number,
        'frontend' : frontend_build_number})

    # Increment the deployment count.
    deployments_processed = deployments_processed + 1

# Start the thread pool.
with ThreadPoolExecutor(max_workers=8) as thread_pool:
    for deployment in deployments:
        # Submit the data.
        thread_pool.submit(perform_work, deployment_data=deployment, array_index=current_thread_id)

        # Increment and limit.
        current_thread_id = (current_thread_id + 1) % 8

# Flatten the deployment lists.
bad_deployments = list(itertools.chain.from_iterable(thread_statics))

with open('bad_deployments.csv', 'wb') as out_file:
    out_file.write('Name,Creator,Creator Email,Front end build number,Back end build number\n')
    for deployment in bad_deployments:
        # Gather the data.
        deployment_data = deployment['deployment']
        deployment_name = deployment_data['name']
        deployment_creator = deployment_data['created_by']['name']
        deployment_creator_email = deployment_data['created_by']['email']
        deployment_fe_number = deployment['frontend']
        deployment_be_number = deployment['backend']

        # Sanitize deployment name.
        deployment_name = unicode(deployment_name)
        deployment_name = deployment_name.encode("UTF-8")

        # Write it out. deployment_name written separate as it's encoded.
        out_file.write(deployment_name)
        out_file.write(',%s,%s,%d,%d\n' % (deployment_creator,deployment_creator_email,deployment_fe_number,deployment_be_number))