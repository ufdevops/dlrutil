#!/usr/bin/env python
###############################################################################
# (C) Kronos 2016
###############################################################################
# Perform an action on a RightScale deployment
###############################################################################
# Prerequisites: Assumes the RSC command from RightScale is installed and
# in the system search path.
###############################################################################
# usage: rsc-deployment.py [-h] --project PROJECT --release RELEASE --config_root_dir
#                          CONFIG_ROOT_DIR --name NAME --action
#                          {delete,launch,show,start,stop,terminate}
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   --project PROJECT     The RightScale project containing the deployment.
#   --release RELEASE     The CIA release for the deployment
#   --config_root_dir CONFIG_ROOT_DIR
#                         Root dir of global config utilities
#   --name NAME           The name of the RightScale deployment on which to
#                         operate.
#   --action {delete,launch,show,start,stop,terminate}
#                         The action to perform on the deployment.
#
# Exit status
#    0 if action succeeded
#    1 if action did not succeed
#    2 if deployment did not exist
###############################################################################

import argparse
import subprocess

import rscutils
import rscutils.logger as logger

from util.config.ConfigJSON import ConfigJSON
from rsc.api.ss import SelfService

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)
    parser.add_argument('--name',
                        help='The name of the RightScale deployment on which to operate.',
                        required=True)
    parser.add_argument('--action',
                        choices=['delete', 'launch', 'show', 'get', 'start', 'stop', 'terminate'],
                        help='The action to perform on the deployment.',
                        required=True)
    return parser.parse_args()

def main():
    # assume it will fail
    return_code = 1

    args = parse_arguments()
    action = args.action

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()
    project_id = config.get('project_id')

    ss_api = SelfService()

    deployment_id = None
    try:
        deployment_id = rscutils.get_deployment_id(ss_api, project_id, args.name)
        method_name = action + "_deployment"
        action_method = getattr(rscutils, method_name)
        action_method(ss_api, project_id, deployment_id)
        return_code = 0
    except subprocess.CalledProcessError as s:
        logger.error (s.output)
    except rscutils.NoDeploymentError as e:
        # if we asked to terminate or delete and it's not there anymore, then we're good
        if action == "delete" or action == "terminate":
            logger.info ("Nothing to delete or terminate.  There is no deployment named " + args.name)
            return_code = 0
        else:
            logger.error ("There is no deployment named " + args.name)
            return_code = 2

    return return_code

exit(main())
