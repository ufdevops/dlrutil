#!/bin/python
###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  Stops all WFM Services - User and WFM Services - Team deployments under
#  the 'dev' subnet.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# collections imports
from collections import OrderedDict
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import wait
from concurrent.futures import ALL_COMPLETED
# multiprocessing imports
import multiprocessing
# operator imports
from operator import itemgetter
# os imports
import os
# sys imports
import sys
# subprocess imports
import subprocess
from subprocess import CalledProcessError
# re imports
import re
# rscutils imports
import rscutils
import rscutils.logger as logger
# rsc.api imports
from rsc.api.cm15 import CM15
from rsc.api.ss import SelfService
# json imports
import json

###############################################################################
# Globals
###############################################################################

# API singletons.
ss_api = SelfService()
cm_api = CM15()

# Project identifier.
gProjectID = None

gExclusions = [];
gExclusions.append ("seahawks-b LaborCategory")
gExclusions.append ("Dig-Phase7")
gExclusions.append ("Atlas-A-Phase7-KPI")
gExclusions.append ("Anitha Transfer Slider")

###############################################################################
# Helper methods
###############################################################################

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()

    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)

    args = parser.parse_args()

    return args


def load_executions(proj_id):
    """
    Loads in executions from RightScale. Saves the result to a cache
    file. Loads that file in next time if it exists.
    :param proj_id: String containing project id to query against.
    :return: Array containing all known executions in RightScale.
    """

    executions = []

    print('Loading in executions...')

    # Check if cache file exists.
    if os.path.isfile('executions.cache'):
        with open('executions.cache', 'r') as executions_file_stream:
            print('Reading executions from cache file..')
            executions = json.load(executions_file_stream)
            print('Success!')

    # Query manually via SS. Then write to file.
    else:
        print('Querying RightScale for all executions.')
        executions = ss_api.index_execution(project_id=proj_id, view="expanded")
        with open('executions.cache', 'w') as executions_file_stream:
            json.dump(executions, executions_file_stream)
        print('Success!')

    return executions


def process_execution(execution):
    """
    Given an execution, will check to see if it is a WFM Team or User.
    Then will check to see if it is deployed in dev. If it is, will stop the
    deployment.
    :param execution: The execution to consider.
    :param project_id: The project identifier.
    :return: True if the execution has no scheduled action. False if it does.
    """

    result = False

    # Figure out what CAT file launched this guy.
    launched_from = execution["launched_from_summary"]
    cat_name = launched_from["value"]["name"]

    # Is the CAT file one of the CATs we care about?
    if cat_name in ["WFM Services - Team", "WFM Services - User"]:
        if execution["status"] == "running":
            logger.info ("running: %s" % execution["name"])
        else:
            logger.info ("not running (%s): %s" % (execution["status"], execution["name"]))
            return False

        # Get the deployment ID.
        deployment_href = execution["deployment"]
        # deployment_id = deployment_href.split("/")[3]

        # Get the tags associated with this deployment.
        tags = cm_api.by_resource_tag(resource_hrefs=[deployment_href])[0]["tags"]

        # Collect each tag into an array.
        tag_data = []
        for tag in tags:
            tag_data.append(tag["name"].lower())

        # Only consider deployments in North America in Development that aren't
        # launched by svcCI@kronos.com.
        if "kronos:geographic_location=north america" in tag_data:
            if "kronos:subnet=dev" in tag_data:
                if "kronos:do_not_delete=true" not in tag_data:
                    if "selfservice:launched_by=svcci@kronos.com" not in tag_data:
                        exe_name = execution["name"]
                        if is_excluded(exe_name):
                            logger.info ("exclude '%s'" % execution["name"])
                        else:
                            logger.info ("stop (%s) '%s'" % (execution["id"], execution["name"]))
                            try:
                                result = ss_api.stop_execution(project_id=gProjectID, id=execution["id"])
                            except Exception as e:
                                logger.error ("BAD STOP " + str(e))
                    else:
                        logger.info ("launched by svvci: " + execution["name"])
                else:
                    logger.info ("do_not delete: " + execution["name"])
            else:
                logger.info ("not in dev: " + execution["name"])
        else:
            logger.info ("not in NA: " + execution["name"])

    return result

def is_excluded(name):
    excluded = False
    for n in gExclusions:
        if n.strip() == name.strip():
            excluded = True
            break

    return excluded 

def process_executions(thread_pool, executions):
    """
    Queues up process_execution calls for each execution. Returns the
    result.
    :param thread_pool: The thread pool to submit process_execution jobs to.
    :param executions: List of all executions in RightScale.
    :return: List of FutureResult objects.
    """

    future_results = []

    print("Queuing up executions for processing.")

    for execution in executions:
        # Submit the job to the thread pool.
        future_result = thread_pool.submit(process_execution, execution)

        # Store off the execution data for later access.
        future_result.execution_data = execution

        # Add to our future results array.
        future_results.append(future_result)

        # TEMP TEMP
        # break

    print("Queued up %d executions for processing." % len(future_results))

    return future_results

###############################################################################
# Implementation
###############################################################################
def main():
    """
    Main program implementation.
    :return: Exit status.
    """
    global gProjectID
    exit_status = 0

    # Read arguments out from command line.
    args = parse_arguments()

    # Update the python path with path to global config utilities.
    sys.path.append(os.path.join(args.config_root_dir, 'scripts'))

    # Perform delayed utilities imports.
    from util.config.ConfigJSON import ConfigJSON

    # Read out global configuration data.
    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read()

    # Get project identifier.
    gProjectID = config.get('project_id')

    # Read in the executions.
    executions = load_executions(gProjectID)

    # Construct a thread pool.
    thread_pool = ThreadPoolExecutor(multiprocessing.cpu_count())

    # Thread out all of the execution processing.
    future_results = process_executions(thread_pool, executions)

    # Now wait until all executions are processed.
    wait(future_results, timeout=None, return_when=ALL_COMPLETED)

    for future_result in future_results:
        the_exception = future_result.exception()
        if the_exception:
            execution_name = future_result.execution_data["name"]
            print("Exception occurred while stopping %s: %s" % (execution_name, the_exception))

    return exit_status

# Run the program.
if __name__ == '__main__':
    sys.exit(main())
