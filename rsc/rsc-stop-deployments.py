###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This RightScale utility will stop a list of deployments. It takes in a
#  a single txt file that contains the hrefs for the deployments you want
#  to stop.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
from argparse import ArgumentParser
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
# multiprocessing imports
import multiprocessing
# json imports
from json import dumps
# os imports
from os import path
# rsc imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
from rsc.api.cm16 import CM16
# sys imports
from sys import exit

###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()
cm16_api = CM16()

# Create a thread pool.
thread_pool = ThreadPoolExecutor(multiprocessing.cpu_count())

# Stores all of the futures.
future_results = []

###############################################################################
# Functions
###############################################################################

def parse_arguments():
    """
    Parses arguments required by this program.
    :return: Values of said arguments.
    """

    parser = ArgumentParser(description='Shuts down deployments.')
    parser.add_argument('--deployments-file',
                        help="Text file containing deployments to stop.",
                        required=True)
    args = parser.parse_args()

    return args.deployments_file

###############################################################################
# Helper functions
###############################################################################
def get_execution_id(deployment_tags):
    for tag in deployment_tags:
        if "/api/manager/projects/84502/executions" in tag["name"]:
            return tag["name"].split("=")[1]
    return None

def attempt_to_stop_execution(execution_id):
    try:
        ss_api.stop_execution(project_id='84502', id=execution_id, retries=1)
        return True
    except:
        return False

def process_deployment(deployment_href):
    # Get the tags for the deployment along with the deployment data.
    deployment_id = deployment_href.split("/")[3]
    deployment_tags = cm_api.by_resource_tag(resource_hrefs=[deployment_href])
    deployment = cm16_api.show_deployment(id=deployment_id, view="extended")

    # Iterate through each instance the deployment has.
    for instance in deployment["instances"]:
        # Skip instances that are "next" instances.
        if instance["is_next"]:
            print("Instance %s is next instance. Skipping.." % instance["name"])
            continue

        # Get the reference to the instance.
        instance_href = instance["href"].split('/')[5]

        try:
            print("Instance %s: state: %s" % (instance["name"], instance["state"]))
            if instance["state"] == "inactive":
                # Lock the instance.
                cm_api.lock_instance(cloud_id='2175', id=instance_href)
                print("\tSuccessfully locked instance %s." % instance["name"])

                # Skip it if it's inactive.
                print("Skipping instance %s -- inactive." % instance["name"])
                continue

            if instance["state"] == "provisioned":
                # Lock the instance.
                cm_api.lock_instance(cloud_id='2175', id=instance_href)
                print("\tSuccessfully locked instance %s." % instance["name"])

                # Skip it.
                print("Skipping instance %s -- already stopped." % instance["name"])
                continue

        except Exception as e:
            print("Exception occurred: %s" % repr(e))
            continue

        if instance["locked"]:
            print("Unlocking instance %s in state %s." % (instance["name"], instance["state"]))
            cm_api.unlock_instance(cloud_id='2175', id=instance_href)

        # Stop the instance.
        cm_api.stop_instance(cloud_id='2175', id=instance_href)
        print("\tSuccessfully stopped instance %s." % instance["name"])

        # Lock the instance up.
        cm_api.lock_instance(cloud_id='2175', id=instance_href)
        print("\tSuccessfully locked instance %s." % instance["name"])

    # Lock the deployment.
    cm_api.lock_deployment(id=deployment_id)

    # Now be nice and try to stop the execution too.
    execution_id = get_execution_id(deployment_tags[0]["tags"])
    execution_id = execution_id.split("/")[6]
    print("\tStopping execution %s.." % execution_id)
    if not attempt_to_stop_execution(execution_id):
        print("\tFailed to stop execution.")
    else:
        print("\tStopped execution successfully.")


###############################################################################
# Implementation
###############################################################################

# Parse arguments
deployments_file_path = parse_arguments()

# Read in the deployments to stop.
deployments_to_stop = []
with open(deployments_file_path, "r") as deployments_file_stream:
    deployments_to_stop = deployments_file_stream.read().split()

# Debug message.
print("There are %d deployments to stop." % len(deployments_to_stop))

# Queue up the future results.
for deployment_href in deployments_to_stop:
    future_result = thread_pool.submit(process_deployment, deployment_href)
    future_results.append(future_result)

# Run the processing.
thread_pool.shutdown(wait=True)

# Open up file handles.
error_deployments_file_stream = open("error_deployments.txt", "w")

# Iterate through future results.
for stop_result in future_results:
    if stop_result.exception():
        result_idx = future_results.index(stop_result)
        deployment_href = deployments_to_stop[stop_result]
        error_deployments_file_stream.write("%s\n" % deployment_href)

    #result = stop_result.result()

if False:
    # Begin iterating through each deployment.
    deployments_we_stopped_via_ss_executions = []

    counter = 0
    for deployment_href in deployments_to_stop:
        # Did we already stop this deployment via self-service?
        # If yes, then we need to just skip it.
        if deployment_href in deployments_we_stopped_via_ss_executions:
            print("Skipping %s as it's already been stopped by SS." % deployment_href)
            continue

        # Get the tags for the deployment along with the deployment data.
        deployment_id = deployment_href.split("/")[3]
        deployment_tags = cm_api.by_resource_tag(resource_hrefs=[deployment_href])
        deployment = cm16_api.show_deployment(id=deployment_id, view="extended")

        # Print debug information.
        print("Attempting to stop deployment %s (%d/%d).." % (deployment["name"], counter, len(deployments_to_stop)))

        # Attempt to stop at the execution level first.
        execution_stop_failed = False
        deployment_stop_failed = False

        try:
            # Attempt to stop the execution first.
            execution_id = get_execution_id(deployment_tags[0]["tags"])
            execution_id = execution_id.split("/")[6]
            if execution_id:
                # Stop the execution.
                print("\tStopping execution %s.." % execution_id)
                attempt_to_stop_execution(execution_id)
                print("\tStopped execution successfully.")

                # Lock the deployment.
                print("\tLocking deployment..")
                cm_api.lock_deployment(id=deployment_id)
                print("\tLocked deployment successfully.")
                deployments_we_stopped_via_ss_executions.append(deployment_href)
            else:
                execution_stop_failed = True
        except Exception as e:
            print("\tFailed to stop execution: %s" % repr(e))
            execution_stop_failed = True

        try:
            # If we failed to stop the execution, then we need to stop the
            # instances within the deployment manually.
            for instance in deployment["instances"]:
                if instance["state"] != "stopped":
                    print("\tAttempting to stop instance %s." % instance["name"])
                    instance_href = instance["href"].split('/')[5]
                    cm_api.stop_instance(cloud_id='2175', id=instance_href)
                    print("\tSuccessfully stopped instance.")
                    cm_api.lock_instance(cloud_id='2175', id=instance_href)
                    print("\tSuccessfully locked instance.")

            # Once all instances are stopped, lock the deployment down.
            cm_api.lock_deployment(id=deployment_id)

        except Exception as e:
            print("\tFailed to stop deployment manually. %s" % repr(e))
            deployment_stop_failed = True

        if deployment_stop_failed and execution_stop_failed:
            error_deployments_file_stream.write("%s\n" % deployment_href)

        counter = counter + 1
