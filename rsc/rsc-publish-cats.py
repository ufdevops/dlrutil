###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This script will publish the cloud application templates specified by the
#  'config/publish_whitelist.conf' file.
###############################################################################
# Pre-requisites:
#  This script assumes you are authenticated with the 'rsc' command locally
#  on the machine you are running the script on.
#  This script also assumes you have Python 2's "futures" package installed.
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# concurrent imports
from concurrent.futures import ThreadPoolExecutor
# itertools imports
import itertools
# json imports
import json
# os imports
import os
# rsc.api imports
from rsc.api.ss import SelfService
from rsc.api.cm15 import CM15
# sys imports
from sys import exit

###############################################################################
# Globals
###############################################################################
ss_api = SelfService()
cm_api = CM15()

###############################################################################
# Helper methods
###############################################################################

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()

    parser.add_argument('-pi',
                        help="ID of the RightScale project to operate on.",
                        required=True)

    args = parser.parse_args()

    return args.pi

###############################################################################
# Implementation
###############################################################################

# Read in arguments
arg_project_id = parse_arguments()

# Read in configuration file.
current_directory = os.getcwd()
whitelist_file = os.path.join(current_directory, "config", "publish_whitelist.conf")
if not os.path.isfile(whitelist_file):
    print("Could not find whitelist file %s." % whitelist_file)
    exit(1)

cats_to_publish = []
with open(whitelist_file, "r") as f:
    cats_to_publish = f.read().splitlines()

print(cats_to_publish)

# Loop through the CAT files and publish them
for cat_file_name in cats_to_publish:
    # Print debug info.
    print("Publishing CAT file %s." % cat_file_name)

    # Read in the CAT data.
    cat_data = ss_api.index_template(
        collection_id=arg_project_id, filter=["name==%s" % cat_file_name])

    # Skip CATs that don't exist yet.
    if len(cat_data) == 0:
        print("No CAT file found for %s." % cat_file_name)
        continue

    # Get the CAT file ID.
    cat_file_id = cat_data[0]["id"]

    # Now publish the CAT file.
    # For some reason, this call attempts to publish multiple times even though
    # it is only called once. Force retries here to 1.
    ss_api.publish_template(collection_id=arg_project_id, id=cat_file_id, retries=1)