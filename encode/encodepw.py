#!/usr/bin/python

from Crypto.Cipher import AES
import base64
# msg_text must be multiple of 16
#                    1         2         3         4
#           123456789012345678901234567890123456789012345678
msg_text = 'domenic.larosa@kronos.com:Trainbound99!         '.rjust(32)

# secret_key must be 24 characters
#                    1         2      
#             123456789012345678901234
secret_key = 'domenicwasheredomenicwas'
cipher = AES.new(secret_key,AES.MODE_ECB)
encoded = base64.b64encode(cipher.encrypt(msg_text))
print encoded

decoded = cipher.decrypt(base64.b64decode(encoded))
print decoded.strip()
