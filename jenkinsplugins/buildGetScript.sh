#!/bin/bash -x

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
json_file=$script_dir/cifalconplugins.json
script=curlplugins.sh

grep shortName $json_file | awk '{print $2}' | awk -F "," '{print $1}' | awk -F '"' '{printf "curl -O http://ftp-chi.osuosl.org/pub/jenkins/plugins/%s/latest/%s.hpi\n", $2, $2}' > $script

chmod +x $script
./$script
