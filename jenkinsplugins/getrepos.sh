#!/bin/bash -x

for obj in repositories; do
		rm $obj*
		wget -O $obj.json https://api.bitbucket.org/2.0/teams/ufdevops/$obj
		jq . $obj.json > $obj.pp
done
