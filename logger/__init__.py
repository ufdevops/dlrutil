#!/usr/bin/python3

import json
import threading

LEVELS = {
    'DEBUG': 0,
    'INFO': 1,
    'WARNING': 2,
    'ERROR': 3
}

current_level = 'INFO'

def set_log_level(level):
    global current_level
    current_level = level
    debug('set log level to ' + level)

def logger(level, msg):
    if LEVELS[level] >= LEVELS[current_level]:
        ct = threading.currentThread()
        print('##### {0}|{1}: {2}'.format(level, ct.ident, msg))

def info(msg):
    logger('INFO', msg)

def error(msg):
    logger('ERROR', msg)

def debug(msg):
    logger('DEBUG', msg)

def warning(msg):
    logger('WARNING', msg)

def exception(e):
    error(str(e))

def pp(data):
    # data must be a dictionary
    if isinstance(data,dict):
        logger(json.dumps(data, sort_keys=True, indent=2, separators=(',', ': ')))
    else:
        error("Cannot pretty print because it is not a dictionary")
        print(data)
