import os
import string
import sys

def get_script_path():
    return os.path.abspath(os.path.dirname(sys.argv[0]))

def does_dir_exist(dirname):
    exists = os.path.exists(dirname)
    return exists

def create_dir(dirname):
    if not does_dir_exist(dirname):
        os.makedirs(dirname)
    return

def printPythonPath():
    print ('\n'.join(sys.path))
    #exit (printPythonPath())
    #print (dlutil.__file__)
