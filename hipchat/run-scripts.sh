#!/bin/bash -x

 # This sleep is a workaround because the jobs Retrieve_Statuses and Send_Notifications are running concurrently
 # This pause allows for results.json to be complete in Artifactory 
 sleep 180s

 wget -O "${WORKSPACE}/results.json" "${ARTIFACTORY_SERVER}/artifactory/falcon-deploy/build_status/${parent_job_name}/${parent_build_no}/results.json"
 if [[ $? -ne 0 ]]; then
     echo "ERROR: Could not retrieve file from artifactory."
 exit 1
 fi
 cat "${WORKSPACE}/results.json"
 if ! grep -q "timestamp" "${WORKSPACE}/results.json"; then
 echo "The file results.json is either empty or malformed"
 exit 1
 fi
 
 # We are not going to check if these two files are gotten. It is possible that they
 # are not ready, in a case when a build phase fails quickly.
 # We decided to still allow for jira creation and hipchat notification in this case.
 wget -O "${WORKSPACE}/gitLogEmailsList.txt" "${ARTIFACTORY_SERVER}/artifactory/falcon-deploy/build_status/${parent_job_name}/${parent_build_no}/gitLogEmailsList.txt"
 wget -O "${WORKSPACE}/gitLogResults.txt" "${ARTIFACTORY_SERVER}/artifactory/falcon-deploy/build_status/${parent_job_name}/${parent_build_no}/gitLogResults.txt"
 
 #Check per component, because the final status can be FAILURE when components are not
 # TODO: in the future, may add name 'release' for release job names
 int_name='Integration'
if [ "$parent_job_name" == "$int_name" ];then
    comp_name_sufix='develop'
else
    comp_name_sufix="${parent_job_name}Fixes"
fi

 if grep -q -E "\"wtk_${comp_name_sufix}\": {\"status\": \"FAILURE\"|\
\"install_${comp_name_sufix}\": {\"status\": \"FAILURE\"|\
\"eou_${comp_name_sufix}\": {\"status\": \"FAILURE\"|\
\"wfp_${comp_name_sufix}\": {\"status\": \"FAILURE\"|\
\"wfl_${comp_name_sufix}\": {\"status\": \"FAILURE\"|\
\"dbm_${comp_name_sufix}\": {\"status\": \"FAILURE\"|\
\"wat_${comp_name_sufix}\": {\"status\": \"FAILURE\""  "${WORKSPACE}/results.json";then
     echo "FAILURE case"
     sudo pip install objdict
     echo "Running:"
     echo "python create_jira.py --proj-key "SUP" --jira-user buildUser --jira-pass ${PASSWORD}"
     python create_jira.py --proj-key "SUP" --jira-user "buildUser" --jira-pass ${PASSWORD} > jira_out.txt
     export JIRA_URL=`cat jira_out.txt`
     echo ${JIRA_URL}
     export BUILD_STATUS="FAILURE"
 elif grep -q "ABORTED" "${WORKSPACE}/results.json"; then
     echo "ABORTED case"
     exit 1
 else
     echo "SUCCESS case"
     export BUILD_STATUS="SUCCESS"
 fi
 pushd ${WORKSPACE}/hipchat
 python post-message.py --build-status ${BUILD_STATUS} --results-file ${WORKSPACE}/results.json --emails-file-path ${WORKSPACE}/gitLogEmailsList.txt --build-number ${parent_build_no}

 # This step will prepare the email.properties file to be used by EmailNotifications
 fixed_list="niranjan.bansal@kronos.com,jeffrey.candiello@kronos.com,alain.cousineau@kronos.com,al.lawler@kronos.com,bhrigu.malhotra@kronos.com,terri.mason@kronos.com,sushil.purbey@kronos.com,prashant.rawat@kronos.com,jeffrey.pratt@kronos.com,dirce.richards@kronos.com,timothy.robertson@kronos.com,charanpreet.singh@kronos.com,peter.spooner@kronos.com"
recipients_list=${fixed_list}
if [[ $BUILD_STATUS = "FAILURE" && -s ${WORKSPACE}/gitLogEmailsList.txt ]]
then
    echo "Case failure, compose recipients_list"
    sed 's/;/,/g' <${WORKSPACE}/gitLogEmailsList.txt >${WORKSPACE}/newEmailsList.txt
    echo $fixed_list >> ${WORKSPACE}/newEmailsList.txt
    recipients_list=`cat ${WORKSPACE}/newEmailsList.txt | xargs`
 fi

echo "parent_job_name=${parent_job_name}" > ${WORKSPACE}/email.properties
echo "parent_build_no=${parent_build_no}" >> ${WORKSPACE}/email.properties
echo "jira_url=${JIRA_URL}" >> ${WORKSPACE}/email.properties
echo "recipients_list=${recipients_list}" >> ${WORKSPACE}/email.properties
echo "status=${BUILD_STATUS}"  >> ${WORKSPACE}/email.properties

cat ${WORKSPACE}/email.properties