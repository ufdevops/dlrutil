#!/usr/bin/python

###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This script gets the results for a list of jobs
###############################################################################
# usage: retrieve-results.py [-h] --build_number bn --job_names jobs --output_fn fn
#                        --parent_job job_name
# optional arguments:
#   -h, --help            show this help message and exit
#   --parent_job jn       Name of the parent job that calls job_names
#   --build_number bn     The build number of the parent job
#   --job_names jobs      Jobs for which to retrieve results
#   --output_fn fn        Output filename
###############################################################################

from util.config.ConfigJSON import ConfigJSON

import os
import argparse

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--project',
                        help='The RightScale project containing the deployment.',
                        required=True)
    parser.add_argument('--release',
                        help='The CIA release for the deployment.',
                        required=True)
    parser.add_argument('--config_root_dir',
                        help='Root dir of global config utilities',
                        required=True)

    return parser.parse_args()

def check_env_vars(evars):
    missing = False
    for e in evars:
        if os.getenv(e) == None:
            logger.error('You must set ' + e)
            missing = True
    if missing:
        logger.error ("Exiting because you are missing some environment variables.")
        exit (1)
    return

def execute_test(execution_name, job_name, jenkins_server):
    print ("%s: %s on %s" % (execution_name, job_name, jenkins_server))
    os.system('./trigger_job.sh "%s" "%s" "%s"' % (job_name, jenkins_server, execution_name))

def main():
    check_env_vars (["PYTHONPATH"])

    return_code = 1

    args = parse_arguments()

    config = ConfigJSON(args.config_root_dir, args.project, args.release)
    config.read ()

    p0tests = config.get ("p0tests")
    for t in p0tests:
        execute_test(p0tests[t]["execution_name"]["value"],
                     p0tests[t]["remote_job_name"]["value"],
                     p0tests[t]["remote_jenkins_server"]["value"])

    return return_code

exit(main())
