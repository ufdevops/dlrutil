#!/bin/bash -x

# This script is called from Prepare_Notifications_Collate.dsl, to collate the temporary logs that were previously created
# by Prepare_Notifications_Multi

# Parameter: parent_job_name, parent_build_no, number_of_instances

echo "Will collate $3 files from $1/$2"

for (( i=1; i <= $3; i++ ))
do
    echo "Collating $i"
    wget  -O "${WORKSPACE}/gitLogResults.txt$i" "https://artifactory.falcon.kronos.com/artifactory/falcon-deploy/build_status/$1/$2/gitLogResults.txt$i"
    if [ -s ${WORKSPACE}/gitLogResults.txt$i ]
    then
        cat ${WORKSPACE}/gitLogResults.txt$i >> ${WORKSPACE}/gitLogResults.txt
    fi

   wget  -O "${WORKSPACE}/gitLogEmailsList.txt$i" "https://artifactory.falcon.kronos.com/artifactory/falcon-deploy/build_status/$1/$2/gitLogEmailsList.txt$i"
   if [ -s ${WORKSPACE}/gitLogEmailsList.txt$i ]
   then
       cat ${WORKSPACE}/gitLogEmailsList.txt$i >> ${WORKSPACE}/gitLogEmailsList.txt
       printf "; " >> ${WORKSPACE}/gitLogEmailsList.txt
    fi
done

# Publish results
curl -v --user ${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD} -X PUT -T "${WORKSPACE}/gitLogResults.txt" "${ARTIFACTORY_SERVER}/artifactory/falcon-deploy/build_status/$1/$2/gitLogResults.txt"
curl -v --user ${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD} -X PUT -T "${WORKSPACE}/gitLogEmailsList.txt" "${ARTIFACTORY_SERVER}/artifactory/falcon-deploy/build_status/$1/$2/gitLogEmailsList.txt"