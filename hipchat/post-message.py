################################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  Build notifications!
###############################################################################

###############################################################################
# Imports
###############################################################################
# argparse imports
import argparse
# hipchat imports
from hipchat import HipChat
# json imports
import json
# os imports
import os

###############################################################################
# Globals
###############################################################################
hipchat_server = HipChat()

###############################################################################
# Functions
###############################################################################
def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()

    parser.add_argument("--emails-file-path",
                        help="Absolute path to list of emails to notify.",
                        required=True)
    parser.add_argument("--build-status",
                        help="Status of the build.",
                        required=True)
    parser.add_argument("--build-number",
                        help="Number of the build that passed/failed.",
                        required=True)
    parser.add_argument("--results-file",
                    help="Path to results.json file.",
                    required=True)

    args = parser.parse_args()

    return args.emails_file_path, args.build_status, args.build_number, args.results_file


def get_status_color(build_status):
    """
    Gets the color to use when sending the notification regarding the status
    of a build.
    :param build_status: Can by any one of "ABORTED", "SUCCESS", "FAILED" or "UNSTABLE".
    :return: Color to use for the notification. Default to gray if a corresponding color
    is not found.
    """

    color_dict = { "SUCCESS":"green", "UNSTABLE":"yellow",
                   "ABORTED":"gray", "FAILURE":"red" }

    if build_status in color_dict:
        return color_dict[build_status]

    return "gray"


def post_status_notification(room, build_component, build_number, build_status, jira_url=None):
    """
    Posts a build status notification regarding the given component to
    the given room.
    :param room: The room to post the notification in.
    :param build_component: The component the status belongs to.
    :param build_number: The number associated with this build component.
    :param build_status: The actual build status string.
    :param jira_url: A URL to the JIRA ticket (if there is one).
    :return: The build status string.
    """

    # Compose the notification according to the component (Integration, Phase8, wfp_develop, etc)
    if (build_component == "Integration") or ("phase" in build_component) or ("Phase" in build_component):
        build_status_string = "%s #%s: Compile status is %s!" % \
                              (build_component, build_number, build_status)
    else:
        # Construct the build status string:
        build_status_string = "Component %s #%s: Compile status is %s!" % \
                              (build_component, build_number, build_status)

    # Create string for JIRA ticket (if there is one).
    if jira_url:
        jira_string = "JIRA ticket: %s" % jira_url
        build_status_string = build_status_string + " %s" % jira_string

    # Construct the message dict.
    message_dict = { "from": "Service Account", "message_format": "text",
                     "color": get_status_color(build_status),
                     "notify": "true",
                     "message": build_status_string}


    # Post a message to the hipchat room.
    room.send_notification(message_dict)

    return build_status_string


###############################################################################
# Implementation
###############################################################################

# Get arguments.
emails_path, build_status, build_number, results_file_path = parse_arguments()

# Read in the JSON file that contains the results.
json_build_statuses = None
try:
    with open(results_file_path) as json_data:
        json_build_statuses = json.load(json_data)
except:
    print("Could not read out results.json file.")

# API ID of the HipChat room.
#TODO: automate this for any new phase room

room_name_id_map = { "Integration": "206", "phase8": "307"}
build_comp = json_build_statuses["parent_job"]["name"]
hipchat_room = hipchat_server.get_room(room_name_id_map.get(build_comp))

# Get JIRA_URL in case of failure.
jira_url = None
if build_status == "FAILURE":
    jira_url = os.environ["JIRA_URL"]

# Handle notifications for the main build.
build_status_string = post_status_notification(
    hipchat_room,
    build_comp,
    build_number,
    build_status,
    jira_url)

# Set the topic on the room too.
print("BUILD STATUS STRING: %s" % build_status_string)
hipchat_room.set_topic(build_status_string)

# If build status is FAILED, print failed components and email
# suspects.
if build_status == "FAILURE":
    # Now print notifications for each component in json_build_statuses
    if "jobs" in json_build_statuses:
        for job_name in json_build_statuses["jobs"]:
            # Get the status of the component build.
            job_status = json_build_statuses["jobs"][job_name]["status"]

            # If the component failed, print a status.
            if job_status == "FAILURE":
                job_build_number = json_build_statuses["jobs"][job_name]["build"]
                post_status_notification(
                    hipchat_room, job_name,
                    job_build_number, job_status)

    # Read in suspect emails list.
    emails_file = open(emails_path, "r")
    suspect_list = emails_file.readline().split(";")

    # Email and invite each one.
    for suspect in suspect_list:
        # Remove all trailing and leading whitespace in the suspect email name.
        suspect = suspect.strip()

        # Ensure that we have an actual email.
        if len(suspect) > 0:
            # Invite the suspect.
            print("Attempting to invite suspect %s to hipchat room." % suspect)
            try:
                hipchat_room.invite_person(suspect, "You have checked in since the build broke. Please help troubleshoot.")
            except Exception as e:
                print("Failed to invite suspect %s. Error: %s" % (suspect, str(e)))
                continue
