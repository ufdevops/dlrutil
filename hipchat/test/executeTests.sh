#!/bin/bash -x

PROJECT=gce_engineering
RELEASE=phasedemo
GLOBAL_CONFIG_DIR=../../global-config
export PYTHONPATH=$GLOBAL_CONFIG_DIR/scripts:.

python execute-tests.py --project "$PROJECT" --release "$RELEASE" --config_root_dir "$GLOBAL_CONFIG_DIR" 
