#!/bin/bash -x

job_name="domenic p0 test"
jenkins_server="http://kvh-ca-kate01.ca.kronos.com:8080"

job_name="HawksP0Wrapper"
jenkins_server="http://kvs-us-kate02.int.kronos.com:8080"
execution_name="IntegrationTesting"

echo "Triggering remote job $job_name on $jenkins_server"

#rm -f *.json

index_fn=index.txt
index=$(<$index_fn)
# update right away so new jobs don't get same
i=$index
i=$((i+1))
echo $i > $index_fn

param_base_fn=set_deployment_parameters_$index
param_fn="$param_base_fn.json"
(cd ../rsc && ./getattr.sh $param_base_fn && cp $param_fn ../hipchat)
sed -i \
		-e 's/output_url_front/frontend_server_fqdn/g' \
		-e 's/output_url_back/backend_server_fqdn/g' \
		-e "s,http://,,g" -e "s,\"\(.*\)/wfc/logon\",\"\1\",g" \
		$param_fn
more $param_fn
python trigger_job.py --execution_name "$execution_name" --job_name "$job_name" \
			 --jenkins_server "$jenkins_server" --timeout 600 --param_fn "$param_fn"
