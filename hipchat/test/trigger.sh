#!/bin/bash -x

job_name="$1"
jenkins_server="$2"
execution_name="$3"

# where to run the remote execute job
re_job_name="Execute Remote Test"
ci_jenkins_server=https://ci.falcon.kronos.com

echo "Triggering remote job $job_name on $jenkins_server"

param_fn="$job_name.json"
cat > "$param_fn"<<EOF
{
  "REMOTE_JENKINS_SERVER": "$jenkins_server",
  "REMOTE_JOB_NAME": "$job_name",
  "EXECUTION_NAME": "$execution_name"
}
EOF

cat "$param_fn"

python trigger_job.py --job_name "$re_job_name" --jenkins_server "$ci_jenkins_server" \
			 --timeout 600 --param_fn "$param_fn"

exit $?

