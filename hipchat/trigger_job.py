#!/usr/bin/python

###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This script triggers a Jenkins job
###############################################################################
###############################################################################

from Crypto.Cipher import AES
import base64
import argparse
import json
import os
import urllib
import time
import random

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--job_name',
                        help='Job name to trigger',
                        required=True)

    parser.add_argument('--timeout',
                        help='Timeout to wait for job to finish',
                        required=True)

    parser.add_argument('--jenkins_server',
                        help='URL of Jenkins server',
                        required=True)

    parser.add_argument('--param_fn',
                        help='Parameter file for inputs',
                        required=True)

    return parser.parse_args()

def trigger_jenkins_job(command_base, job_name, random_num, param_fn):

    job_info_fn = job_name + "_job_info.json"
    job_info_fn = job_info_fn.replace(" ", "")
    build_info_fn = job_name + "_build_info.json"
    build_info_fn = build_info_fn.replace(" ", "")

    parameters = None
    parameter_str = ""
    if os.path.exists(param_fn):
        with open(param_fn) as param_file:
            try:
                parameters = json.load(param_file)
                for p in parameters:
                    parameter_str = parameter_str + "&" + p + "=" + urllib.quote_plus(parameters[p])
            except Exception as e:
                print ("Error loading the json file: " + param_fn)
                raise

    # the random number prevents caching issues
    command_tail = "buildWithParameters?token=CIA&delay=0sec"
    command = "%s/%s%s&random_num=%s' -O '%s'" % \
              (command_base, command_tail, parameter_str, random_num, job_info_fn)

    print "Starting job: " + job_name
    print command
    os.system(command)

    return parameters

def get_current_build(command_base, job_name, last_build_number, job_parameters, timeout):
    stripped_job_name = job_name.replace(" ", "")

    build_number = last_build_number + 1
    command_tail = "api/json?pretty=true"
    ret_job_info = None
    build_info = None

    rc = 1
    elapsed = 0
    delay = 5
    while rc != 0 and elapsed < timeout:
        # sleep here in case we need to reloop
        time.sleep(delay)
        elapsed = elapsed + delay

        # wait for the job to start
        print "Polling to find the current build (" + str(elapsed) + "/" + str(timeout) + ")"

        build_info_fn = stripped_job_name + "_build_info_" + str(build_number) + ".json"
        command = "%s/%s/%s' -O '%s'" % (command_base, build_number, command_tail, build_info_fn)
        print "Getting build info for job: " + job_name
        print command
        rc = os.system(command)

        if rc == 0:
            # check that parameters match if we get the build
            match = False
            if os.path.exists(build_info_fn):
                with open(build_info_fn) as build_info_file:
                    try:
                        build_info = json.load(build_info_file)
                        parameters = build_info['actions'][0]["parameters"]
                        match = True
                        for p in parameters:
                            name = p['name']
                            value = p['value']
                            if name not in job_parameters:
                                print name + " not in job_parameters"
                                match = False
                            elif job_parameters[name] != value:
                                print "No match on " + name + " " + job_parameters[name] + "!=" + value
                                match = False
                    except Exception as e:
                        print ("Error loading the json file: " + build_info_fn)
                        raise

            if not match:
                rc = 1
                build_number = build_number + 1
            else:
                print "Found match at build #" + str(build_number)

    if match:
        ret_job_info = get_job_info (job_name, build_number, build_info['result'])
        
    return ret_job_info

def get_jenkins_job_info(command_base, job_name, last_build_number=-1):

    job_info_fn = job_name + "_running_job_info.json"
    job_info_fn = job_info_fn.replace(" ", "")
    build_info_fn = job_name + "_build_info.json"
    build_info_fn = build_info_fn.replace(" ", "")

    command_tail = "api/json?pretty=true"
    command = "%s/%s' -O '%s'" % (command_base, command_tail, job_info_fn)

    if last_build_number == -1:
        print "Getting build info for job: " + job_name
        print command
        os.system(command)
        with open(job_info_fn) as job_info_file:
            try:
                job_info = json.load(job_info_file)
                last_build_number = job_info['lastCompletedBuild']['number']
            except Exception as e:
                print ("Error loading the json file: " + job_info_fn)
                last_build_number = 0

    # get results of the build #last_build_number
    command = "%s/%s/%s' -O '%s'" % (command_base, last_build_number, command_tail, build_info_fn)
    print "Get results for job: " + job_name
    print command
    os.system(command)
    result = ""
    with open(build_info_fn) as build_info_file:
        try:
            build_info = json.load(build_info_file)
            result = build_info['result']
        except Exception as e:
            print ("Error loading the json file: " + build_info_fn)
            result = None

    ret_job_info = get_job_info (job_name, last_build_number, result)

    return ret_job_info

def get_job_info (job_name, build_number, status):
    ret_job_info = {}
    
    ret_job_info['name'] = job_name
    ret_job_info['build'] = build_number
    ret_job_info['status'] = status
    
    return ret_job_info

def main():
    return_code = 1

    args = parse_arguments()
    param_fn = args.param_fn
    timeout = args.timeout

    secret_key = 'domenicwasheredomenicwas'
    block = '1234567890123456'
    decryption_suite = AES.new(secret_key, AES.MODE_CBC, block)
    username = "domenic.larosa@kronos.com"

    encoded_password = 'jrpbci5M0aVdMNw87yHc1w=='
    password = decryption_suite.decrypt(base64.b64decode(encoded_password)).strip()
    random_num = random.randint(0,100000)

    jenkins_server = args.jenkins_server
    job_name = args.job_name
    job_name_encoded = job_name.replace(" ", "%20")

    command_base = "wget --quiet --no-verbose --auth-no-challenge --http-user=%s --http-password=%s '%s/job/%s" % (
        username, password, jenkins_server, job_name_encoded
    )

    last_build_number = -1
    ret_job_info = get_jenkins_job_info(command_base, job_name, last_build_number)
    last_build_number = ret_job_info['build']

    print ("Next job has to be greater than build %s" % (last_build_number))

    job_parameters = trigger_jenkins_job(command_base, job_name, random_num, param_fn)
    if job_parameters is not None:
        # find the build on the remote server
        ret_job_info = get_current_build(command_base, job_name, last_build_number,
                                         job_parameters, timeout)
        
        if ret_job_info != None:
            sleep_time=20
            elapsed=0
            build_number = ret_job_info['build']
            status = ret_job_info['status']
            
            # loop until completed
            while status is None and elapsed < int(timeout):
                print "Polling build #" + str(build_number) + " status = " + str(status) + \
                    " (" + str(elapsed) + "/" + str(timeout) + ")"

                ret_job_info = get_jenkins_job_info(command_base, job_name, build_number)
                status = ret_job_info['status']
                
                if status is None:
                    time.sleep(sleep_time)
                    elapsed = elapsed + sleep_time

            status = ret_job_info['status']
            if status is None:
                print "Build #" + str(build_number) + " timed out after " + str(timeout) + " seconds"
            else:
                print "Completed status for build #" + str(build_number) + " = " + status
                if status == "SUCCESS":
                    return_code = 0

    return return_code

exit(main())
