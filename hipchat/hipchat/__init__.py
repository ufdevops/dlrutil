################################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  HipChat interaction module.
###############################################################################

###############################################################################
# Imports
###############################################################################
import json
import urllib
from urllib2 import Request, urlopen

###############################################################################
# Functions
###############################################################################


def do_hipchat_query(api_url, query_type="GET", payload=None):
    """
    Performs a GET query against the hipchat server.
    :param api_url: The API URL you want to run the query against.
    :param query_type: The type of query to perform (either "GET" or "PUT").
    :param payload: Dictionary containing data you want to place in the
    payload section of the message. Can be none if there is no payload.
    :return: JSON response (if any).
    """

    # Construct the message header.
    msg_header = {  "content-type": "application/json",
                    "authorization": "Bearer %s" % HipChat.AUTH_TOKEN }

    # Convert the message data to a string.
    if payload is None:
        payload_str = None
    else:
        payload_str = json.dumps(payload)

    # Fire off the message.
    request = Request(api_url, headers=msg_header, data=payload_str)

    # Patch the request type.
    if query_type is "PUT":
        request.get_method = lambda: 'PUT'

    # Open the URL.
    uo = urlopen(request)

    # Read out the raw response.
    response = ''.join(uo)

    # Close the connection.
    uo.close()

    # Attempt to load the response as JSON.
    try:
        response = json.loads(response)
    except:
        pass

    # Return the response.
    return response

###############################################################################
# Classes
###############################################################################


class HipChatRoom(object):
    """
    This class represents a HipChat room.
    """

    def __init__(self, room_url, raw_data):
        """
        Initializes the room.
        :param room_url This is the API url of the room. This will be used for
        any and all room related queries.
        :param raw_data: Raw JSON data returned by the hipchat server. This
        data should contain all the backing data for the room.
        """

        self.room_url = room_url
        self.backing_data = raw_data

    def __str__(self):
        """
        Converts this object to a string.
        :return: Returns a string representation of this object.
        """

        if self.backing_data:
            return str(json.dumps(self.backing_data, indent=4, sort_keys=True))

        return "HipChatRoom"

    def add_person(self, person_email):
        """
        Adds a person to the room. Only works on a private room.
        :param person_email: The email of the person to add to the room.
        :return: None
        """

        # Add a person to the hipchat room.
        do_hipchat_query(self.room_url + "/member/%s" % person_email, query_type="PUT")

    def invite_person(self, person_email, reason):
        """
        This method will invite a person to a room. This will work with public
        and private rooms.
        :param person_email: Email address of the person to invite.
        :param reason: Reason why this person is being invited.
        :return: None
        """

        do_hipchat_query(self.room_url + "/invite/%s" % person_email, query_type="POST",
                         payload={"reason": reason})

    def send_message(self, message_data):
        """
        This method will post a message to this room.
        :param message_data: JSON data for the message.
        :return: None
        """

        do_hipchat_query(self.room_url + "/message", query_type="POST",
                         payload=message_data)

    def send_notification(self, notification_data):
        """
        This method will post a notification to this room.
        :param notification_data: Payload for the notification.
        :return: None
        """

        do_hipchat_query(self.room_url + "/notification", query_type="POST",
                 payload=notification_data)

    def set_topic(self, new_topic):
        """
        Sets the topic of the room.
        :param new_topic: The topic to apply to the room.
        :return: None
        """

        do_hipchat_query(self.room_url + "/topic", query_type="PUT",
                  payload={"topic": new_topic})


class HipChat(object):
    """
    This class represents an interface to the HipChat server.
    """

    ###########################################################################
    # Static variables
    ###########################################################################
    AUTH_TOKEN  = "udWrcGQfsaVzyewkujKcFRP9No1PI2hJN4TrdlGZ"
    HIPCHAT_URL = "https://kvs-us-hipchat.int.kronos.com/v2/"

    ###########################################################################
    # Methods
    ###########################################################################
    def __init__(self):
        pass

    def get_room(self, room_id_or_name):
        """
        Gets data about the given room.
        :param room_id_or_name: The room name or HipChat ID.
        :return: A room object
        """

        # Convert room identifier to string.
        room_id = str(room_id_or_name)
        room_id = urllib.quote(room_id)

        # Construct the base URL for the room API.
        ROOM_API_URL = HipChat.HIPCHAT_URL + ("room/%s" % room_id)

        # Perform the query.
        return HipChatRoom(ROOM_API_URL, do_hipchat_query(ROOM_API_URL))

    def create_room(self, room_properties):
        """
        Creates a room with the given name and properties.
        :param room_properties: Properties to apply to the room.
        :return: A HipChatRoom object.
        """

        # Construct the base URL for the room API.
        ROOM_API_URL = HipChat.HIPCHAT_URL + "room"

        # Perform the query.
        raw_room_data = do_hipchat_query(ROOM_API_URL, query_type="POST", payload=room_properties)
        print(raw_room_data)

        # Return the result.
        return self.get_room(room_properties["name"])