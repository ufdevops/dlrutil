#!/usr/bin/python

###############################################################################
# (C) Kronos Inc, 2016
###############################################################################
# Description:
#  This script gets the results for a list of jobs
###############################################################################
# usage: retrieve-results.py [-h] --build_number bn --job_names jobs --output_fn fn
#                        --parent_job job_name
# optional arguments:
#   -h, --help            show this help message and exit
#   --parent_job jn       Name of the parent job that calls job_names
#   --build_number bn     The build number of the parent job
#   --job_names jobs      Jobs for which to retrieve results
#   --output_fn fn        Output filename
###############################################################################

from Crypto.Cipher import AES
import base64
import argparse
import json
import os
import urllib
import time

def parse_arguments():
    """
    Parses and returns arguments.
    :return: Values of said arguments.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--build_number',
                        help='Build number of parent job',
                        required=True)

    parser.add_argument('--parent_job',
                        help='Name of the parent job that calls job_names',
                        required=True)

    parser.add_argument('--job_names',
                        help='Jobs for which to retrieve results',
                        required=True)

    parser.add_argument('--output_fn',
                        help='Output filename',
                        required=True)

    # Special parsing for job names. We have to add quotes around these in the DSL
    # because Jenkins doesn't like java properties with commas that aren't surrounded
    # by double quotes.
    args = parser.parse_args()
    args.job_names = args.job_names[1:-1]

    # Return the args structure.
    return args

def get_jenkins_job_info(credentials, jenkins_server, job_name, last_build_number=-1):

    job_info_fn = job_name + "_job_info.json"
    job_info_fn = job_info_fn.replace(" ", "")
    build_info_fn = job_name + "_build_info.json"
    build_info_fn = build_info_fn.replace(" ", "")
    job_name_encoded = job_name.replace(" ", "%20")

    # OLD: Curl way of doing it.
    # curl_base = "curl -s -u %s %s/job/%s" % (credentials, jenkins_server, job_name_encoded)
    # curl_tail = "/api/json?pretty=true"

    # NEW: Use wget.
    command_base = "wget --auth-no-challenge --http-user=svcci@kronos.com --http-password=%s %s/job/%s" % (
        credentials, jenkins_server, job_name_encoded
    )
    command_tail = "/api/json?pretty=true"

    # get build number of latest build for the job if build number not supplied
    # command = "%s%s -o '%s'" % (curl_base, curl_tail, job_info_fn)
    command = "%s%s -O '%s'" % (command_base, command_tail, job_info_fn)

    if last_build_number == -1:
        os.system(command)
        with open(job_info_fn) as job_info_file:
            try:
                job_info = json.load(job_info_file)
            except Exception as e:
                print ("Error loading the json file: " + job_info_fn)
                raise 
            
            last_build_number = job_info['lastCompletedBuild']['number']

    # get results of the build #last_build_number
    # command = "%s/%s%s -o '%s'" % (curl_base, last_build_number, curl_tail, build_info_fn)
    command = "%s/%s%s -O '%s'" % (command_base, last_build_number, command_tail, build_info_fn)
    os.system(command)
    with open(build_info_fn) as build_info_file:
        try:
            build_info = json.load(build_info_file)
        except Exception as e:
            print ("Error loading the json file: " + build_info_fn)
            raise 

    status = build_info['result']

    ret_job_info = {}
    ret_job_info['name'] = job_name
    ret_job_info['build'] = last_build_number
    ret_job_info['status'] = status

    return ret_job_info

def main():
    return_code = 1

    args = parse_arguments()

    # Old credentials:
    # encoded_credentials = 'MSG/iQgR7hymtG8UMebirJu48sEFjohgM2uG+TMP0EHRuYxTL1ogxXkn3qtkXOqK'
    # New credentials:
    encoded_credentials = 'Nkqf9k0u+QEPiyAEGAv210VcKYUTBgfVUuy0UMXu5Xg='
    secret_key = 'domenicwasheredomenicwas'
    cipher = AES.new(secret_key,AES.MODE_ECB)
    credentials = cipher.decrypt(base64.b64decode(encoded_credentials)).strip()

    jenkins_server = "https://ci.falcon.kronos.com"

    all_jobs_results = {}
    all_jobs_results['jobs'] = {}
    all_jobs_results['parent_job'] = get_jenkins_job_info(credentials, jenkins_server, args.parent_job, args.build_number)
    all_jobs_results['timestamp'] = time.ctime(time.time())

    # process all the child jobs from the command line and get results for each
    for job_name in args.job_names.split(','):
        job_name = job_name.strip(" ")
        print "Getting results: " + job_name
        all_jobs_results['jobs'][job_name] = get_jenkins_job_info(credentials, jenkins_server, job_name, -1)

    with open(args.output_fn, "w") as results_file:
        json.dump(all_jobs_results, results_file)
        results_file.close ()

exit(main())
